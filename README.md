# Calf - A Laravel Framework CMS

## About

Calf is a CMS built specifically for use with Laravel - http://laravel.com. It is designed to be highly configurable and to give developers the control over features in the CMS and to be maintainable in version control.

**Not currently compatible with Laravel 5**

## Version

version 1.10.1

## Requirements

- Composer
- Laravel >= 4.1 && < 5.0
- Grunt
- Bower
- Node >= 0.10.28
- NPM

## Installation

1. **Add the package to your dependencies in composer**

    _Currently Calf is not published to Packagist so use BitBucket repo_

    ```json
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:cozyt/calf.git"
        }
    ],
    "require": {
        "cozyt/calf": "1.x"
    }
    ```

    Run `composer install`

2. **Register the Service Provider**

    Register the `Cozyt\Calf\CalfServiceProvider` by adding it to the providers array in the `app/config/app.php` file.

3. **Set up authentication**

    Setup your application to use the authentication methods provided by Calf.

    To make this really simple, you could add this line to your `app/config/auth.php` file
    `return include base_path( 'vendor/cozyt/calf/src/config/auth.php' );`

    _See `src/config/auth.php` for more information on the values that require updating or read about [Laravel Security Configuration](http://laravel.com/docs/4.2/security#configuration)_

4. **Create database tables**

    Run migrations

    ```bash
    php artisan migrate --package="cozyt/calf"
    ```

5. **Uploads** - Create the uploads directory as defined in `src/config/config.php[upload_path]` you can do this by running

    ```bash
    mkdir -m 755 -p {PATH_TO_UPLOADS_DIRECTORY}
    ```

    You may also be required to register the `UploadHandler` in the application's autoloader.

    ```json
    "autoload": {
        "classmap": [
            "vendor/blueimp/jquery-file-upload/server/php/UploadHandler.php"
        ]
    }
    ```

6. **Assets** - Compile and publish assets to the public directory by running

    ```bash
    cd vendor/cozyt/calf && php fed up && cd ../../../ && php artisan asset:publish cozyt/calf
    ```

    This may take some time. Node and Bower will download Calf's dependencies, Grunt then compiles them and runs its front end tasks. Finally, Artisan will publish the assets to public directory for use in the application by Calf.

## Configuration

Calf was designed to be highly configurable and to allow the changes in configuration to be stored in version control. Documented below are some of the common settings you may wish to change.

In order to configure Calf you will need to [Cascade Configuration Files](http://laravel.com/docs/4.2/packages#package-configuration) from the package into your application. You can do this by running:

```bash
php artisan config:publish cozyt/calf
```

When this command is executed, the configuration files for Calf will be copied to `app/config/packages/cozyt/calf` where they can be safely modified by the developer!

*Note:* You may also create environment specific configuration files for Calf by placing them in `app/config/packages/cozyt/calf/environment`.

_For further information or to see what other settings can be configured, please see the documentation in `src/config/config.php`_

### Allowed file extensions

Array of allowed extensions by generic file type that may be uploaded in Calf.

```php
'allowed_extensions' => array(
    'image' => array('png','jpg','jpeg','gif')
),
```

### Image versions

Versions of images to create during upload.

Set as a named key value that acts as the name of the image version, each image version may contain the following settings:

- `max_width` _(string)_ Maximum width of the image version _(required)_
- `max_height` _(string)_ Maximum height of the image version _(required)_
- `auto_orient` _(bool)_ Automatically orientate the image base don its meta data
- `crop` _(bool)_ Force the max dimensions and e.g. create square thumbnails
- `upload_dir` _(string)_ Full path to the directory where the image version is stored
- `upload_url` _(string)_ Full URL for the image version

```php
'image_versions' => array(
    'thumbnail' => array('max_width' => 100, 'max_height' => 100)
),
```

### Layouts

A layout is typically used for defining the template a record should use. It can also be used to define categories for use in blog posts.

Set as a named key value that acts as an ID for the layout, there are three settings each layout must contain:

- `name` _(string)_ Human readable version of the layout for users to understand
- `type` _(string)_ Permission the layout typically belongs to
- `view` _(string)_ The view file the layout uses

```php
'layouts' => array(
    'default' => array(
        'name' => 'Default',
        'type' => 'page',
        'view' => 'layouts.default'
    )
),
```

### Permission types

These are the main permissions categories that permissions are grouped under in Calf

Set as a named key value that acts as the name of the permission type, each type must contain the following settings:

- `title` _(string)_ Human readable title for the permission type
- `icon` _(string)_ Bootstrap glyphicon
- `view` _(string)_ Path to view for permission type index
- `auth_type` _(string)_ General authorization level required to access permission

```php
'permission_types' => array(
    'content' => array(
        'title' => 'Content',
        'icon' => 'pencil',
        'view' => 'calf.pages.content.index',
        'auth_type' => 'contributor',
    )
),
```

### Permissions

Permissions are the user administrable sections of Calf. Within a model they are refered to as a `contentType`

Set as a named key value that acts as the name of the permission, depending on the permission type, each permission may contain the following settings:

- `advanced_filter`     _(bool_)          Flag to allow advanced filtering on listing page
- `alt_title`           _(string_)        Smaller alt. title to the typically longer title option
- `allow_sorting`       _(bool_)          Flag to allow sorting options on record listing page
- `auth_type`           _(string_)        General authorization level required to access permission
- `allow_uploads`       _(bool|int)_      Flag to allow and manage file uploads. Parse int to define upload limit
- `actions`             _(array_)         List of available batch editing features for this permission (publish,unpublish,queue,delete,arrange,export)
- `buttons`             _(string_)        List of text to display in different button types
- `filter_fields`       _(array_)         List of fields to search in for record keyword filters
- `icon`                _(string_)        Bootstrap glyphicon
- `meta_fields`         _(array_)         Fields that should be considered meta content for the record
- `model`               _(string_)        Permission model that should be used to manage data (defaults to CalfContent)
- `paginate`            _(int|bool)_      Paginate records on listing page (int for number of records or bool to default)
- `revisions_allowed`   _(bool_)          Defines if the permission should store revisions against each record created
- `show_record_count`   _(bool_)          Flag to display record count data
- `sort_order`          _(array_)         Default sorting order of permission
- `table_headers`       _(array_)         List of table headers on listing page
- `table_options`       _(array_)         List of options for each record on listing page
- `title`               _(string_)        Human readable title for the permission, usually capitalized & pluralized
- `type`                _(string_)        Type of permission as defined in ['permission_types']
- `validation_rules`    _(array_)         Rules to validate form data defined as array for individual actions (create, edit)
- `views`               _(array_)         Specify override views for actions

### User interface and assets

Calf ships with it's own styles and scripts based on the popular bootstrap UI kit. It also makes use of JQuery. So if you just want to get the CMS up and running, you will find that the shipped styles are sufficient for your needs.

However, if you need to customise it's look and feel, perhaps you need to apply branding for your client, you can manage the assets that are included by removing the defaults or adding to them with your own set of styles. The choice is yours!

Simply update the `assets` config with paths to the files you wish to include/overwrite.

```php
'assets' => array(
    'css' => array(
        '/styles/styles.min.css',
    ),
    'js' => array(
        'https://www.google.com/jsapi',
        "/scripts/scripts.min.js",
    ),
),
```

## Troubleshooting

### File uploads

If you are having issues with file uploads in Calf. You should

1. Confirm that an upload path for the application is set in the `app/config/calf.php` file
2. Confirm that the set upload path exists in your application
3. Check the upload path has the correct permission to save and edit files
4. Make sure the correct name for the file field is parsed through in the `files_upload` method. Defaults to `files`.
5. Check the max file upload size on your server is not too small (by default this is set to 2Mb).

### Setting up email

If you are having issues with sending email, please check the settings in the general `app/config/mail.php` configuration file. Make sure that;

1. A from address is set in the mail config
2. The correct username/password is entered for SMTP connections
3. Confirm that the server is setup to send email

For further information please see http://laravel.com/docs/mail

### Run composer

You will need to run

```bash
composer dump-autoload
```

from your command line terminal or possibly

```bash
composer install
```

in order to get your project up and running.

### UI and assets

If Calf is missing it's UI:

1. Make sure you have downloaded and compiled the front end dependencies. _see installation 6. Assets_
2. Check that `public/packages/cozyt/calf/assets` directory exists.
3. Make sure the paths to the files in the `config/assets.php` file are correct

## Contributing

Follow the steps on [Package Development](http://laravel.com/docs/4.2/packages) on Laravel's official website for information on best practice with setting up your Laravel environment to develop for Calf.

You may also find some of the following commands and adjustments to installation steps useful;

- **Setting up auth**

    Amend the include line in your `app/config/auth.php` file

    ```php
    return include base_path( 'workbench/cozyt/calf/src/config/auth.php' );
    ```

- **Registering the 'UploadHandler' in the application's autoloader**

    Amend the UploadHandler autoload registration (where required)

    ```json
    "autoload": {
        "classmap": [
            "workbench/cozyt/calf/vendor/blueimp/jquery-file-upload/server/php/UploadHandler.php"
        ]
    }
    ```

- **Publish config**

    ```bash
    php artisan config:publish --path="workbench/cozyt/calf/src/config" cozyt/calf
    ```

- **Compile and publish assets**

    ```bash
    cd workbench/cozyt/calf && php fed up && cd ../../../ && php artisan asset:publish --bench="cozyt/calf"
    ```

- **Migrate database tables**

    ```bash
    php artisan --env={DEVELOPMENT_ENVIRONMENT} migrate --bench="cozyt/calf"
    ```

- **Seed test users**

    ```bash
    php artisan --env={DEVELOPMENT_ENVIRONMENT} db:seed --class=CalfUserTableSeeder
    ```


## Roadmap

- Include auto-save functionality
- Auto register dependant UploadHandler class
- Support for Laravel 5


## Known bugs

None at this time. If you find any, please email mail@cozyt.net or file a bug report
