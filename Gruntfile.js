/**
 * Encapsulate Grunt configuration
 */
module.exports = function(grunt) {

	/**************************************************************************
	 * CONFIGURATION
	 * Define configuration for Grunt tasks used for this package
	 *************************************************************************/

	var packageConfig = grunt.file.readJSON('package.json'),
		appConfig = grunt.file.readJSON('config/app.json'),
		copyFiles = grunt.file.readJSON('config/copy.json'),
		sourceFiles = grunt.file.readJSON('config/sources.json'),
		customTasks = grunt.file.readJSON('config/tasks.json'),
		plugins = Object.keys(packageConfig.devDependencies),
		customTaskNames = Object.keys(customTasks),
		fileSeparator = "\n\n\n\n\n/****************************************/\n";





	/**************************************************************************
	 * VALIDATION
	 * Handle/validate configuration values
	 *************************************************************************/

	// Build the correct paths for the application scripts
	for(var i=0;i<sourceFiles.scripts.app.length;i++) {
		sourceFiles.scripts.app[i] = appConfig.directories.source + '/' +
			appConfig.directories.scripts + '/' + sourceFiles.scripts.app[i];
	}

	// Build the correct paths for the application styles
	for(var i=0;i<sourceFiles.styles.app.length;i++) {
		sourceFiles.styles.app[i] = appConfig.directories.source + '/' +
			appConfig.directories.styles + '/' + sourceFiles.styles.app[i];
	}





	/**************************************************************************
	 * DEFINE TASKS
	 * Define individual configuration for each of the tasks we have.
	 * The configuration object for a task lives as a property on the
	 * configuration object, that's named the same as the task. So the "concat"
	 * task goes in our config object under the "concat" key.
	 *************************************************************************/

	var defaultTasks = {

		/**********************************************************************
		 * Notifications
		 * Configure 'grunt-notify' plugin tasks
		 *********************************************************************/

		notify_hooks: {
			options: {
				enabled: true,
				max_jshint_notifications: 5,
				title: packageConfig.title
			}
		},

		notify: {
			all: {
				options: {
					title: packageConfig.title,
					message: 'All tasks completed'
				}
			},
			parse: {
				options: {
					title: packageConfig.title,
					message: 'Gruntfile parsed without errors'
				}
			},
			scripts: {
				options: {
					title: packageConfig.title,
					message: 'Script compiled and copied'
				}
			},
			styles: {
				options: {
					title: packageConfig.title,
					message: 'Styles compiled and copied'
				}
			},
			images: {
				options: {
					title: packageConfig.title,
					message: 'Images optimised and copied'
				}
			}
		},


		/**********************************************************************
		 * Watch directories
		 * Configure 'grunt-contrib-watch' plugin tasks
		 *********************************************************************/

		watch: {
			all: {
				options: {
					reload: true
				},
				files: appConfig.directories.source + '/**',
				tasks: ['default']
			},
			scripts: {
				options: {
					reload: true
				},
				files: appConfig.directories.source + '/' + appConfig.directories.scripts + '/**',
				tasks: ['default']
			},
			styles: {
				options: {
					reload: true
				},
				files: appConfig.directories.source + '/' + appConfig.directories.styles + '/**',
				tasks: ['default']
			},
			images: {
				options: {
					reload: true
				},
				files: appConfig.directories.source + '/' + appConfig.directories.images + '/**',
				tasks: ['default']
			}
		},


		/**********************************************************************
		 * Copy files
		 * Configure 'grunt-contrib-copy' plugin tasks
		 *********************************************************************/

		copy: {
			main: {
				"files": copyFiles
			},

			// Copy ALL scripts into distribution directory
			scripts: {
				expand: true,
				cwd: appConfig.directories.build + '/'  + appConfig.directories.scripts + '/',
				src: ['**'],
				dest: appConfig.directories.dist + '/'  + appConfig.directories.scripts
			},

			// Copy ALL styles into distribution directory
			styles: {
				expand: true,
				cwd: appConfig.directories.build + '/'  + appConfig.directories.styles + '/',
				src: ['**/*.css'],
				dest: appConfig.directories.dist + '/'  + appConfig.directories.styles
			},

			// Copy ALL images into distribution directory
			images: {
				expand: true,
				cwd: appConfig.directories.build + '/'  + appConfig.directories.images + '/',
				src: ['**/*.{png,jpg,gif,svg}'],
				dest: appConfig.directories.dist + '/'  + appConfig.directories.images
			}
		},


		/**********************************************************************
		 * Concatenate
		 * Configure 'grunt-contrib-concat' plugin tasks
		 *********************************************************************/

		concat: {
			// Concatenates all app and vendor scripts into one large file
			scripts: {
				options: { separator: fileSeparator },
				src: [
					'<%= concat.scripts_vendor.dest %>',
					'<%= concat.scripts_app.dest %>'
					// appConfig.directories.build + '/' + appConfig.directories.scripts + '/vendor.js',
					// appConfig.directories.build + '/' + appConfig.directories.scripts + '/app.js'
				],
				dest: '<%=  copy.scripts.cwd %>' + appConfig.filenames.scripts
				// dest: appConfig.directories.build + '/' + appConfig.directories.scripts + '/' + appConfig.filenames.scripts
			},

			// Concatenates all app scripts into one large file
			scripts_app: {
				options: { separator: fileSeparator },
				src: sourceFiles.scripts.app,
				dest: '<%= copy.scripts.cwd %>app.js'
				// dest: appConfig.directories.build + '/' + appConfig.directories.scripts + '/app.js'
			},

			// Concatenates all vendor scripts into one large file
			scripts_vendor: {
				options: { separator: fileSeparator },
				src: sourceFiles.scripts.vendor,
				dest: '<%= copy.scripts.cwd %>vendor.js'
				// dest: appConfig.directories.build + '/' + appConfig.directories.scripts + '/vendor.js'
			},

			// Concatenates all app and vendor styles into one large file
			styles: {
				options: { separator: fileSeparator },
				src: [
					'<%= concat.styles_vendor.dest %>',
					'<%= concat.styles_app.dest %>'
					// appConfig.directories.build + '/' + appConfig.directories.styles + '/vendor.scss',
					// appConfig.directories.build + '/' + appConfig.directories.styles + '/app.scss'
				],
				dest: '<%= copy.styles.cwd %>' + appConfig.filenames.styles.replace('.css','.scss')
				// dest: appConfig.directories.build + '/' + appConfig.directories.styles + '/' + appConfig.filenames.styles.replace('.css','.scss')
			},

			// Concatenates all app styles into one large file
			styles_app: {
				options: { separator: fileSeparator },
				src: sourceFiles.styles.app,
				dest: '<%= copy.styles.cwd %>app.scss'
				// dest: appConfig.directories.build + '/' + appConfig.directories.styles + '/app.scss'
			},

			// Concatenates all vendor styles into one large file
			styles_vendor: {
				options: { separator: fileSeparator },
				src: sourceFiles.styles.vendor,
				dest: '<%= copy.styles.cwd %>vendor.scss'
				// dest: appConfig.directories.build + '/' + appConfig.directories.styles + '/vendor.scss'
			}
		},


		/**********************************************************************
		 * Minification
		 * Configure 'grunt-contrib-uglify' plugin tasks
		 *********************************************************************/

		uglify: {
			options: {
				banner: '/*! ' + packageConfig.title + 'v' + packageConfig.version + ' - <%= grunt.template.today("dd-mm-yyyy") %> */',
				// mangle: true,
				// compress: true,
				sourceMap: false,
				sourceMapName: appConfig.filenames.scripts + '.map'
			},

			// Minifies app and vendor scripts into one minified js file
			dist: {
				files: {
					'<%= concat.scripts.dest.replace(".js",".min.js") %>': ['<%= concat.scripts.dest %>'],
				}
			},

			// Minifies app scripts into one minified js file
			dist_app: {
				files: {
					'<%= concat.scripts_app.dest.replace(".js",".min.js") %>': ['<%= concat.scripts_app.dest %>']
				}
			},

			// Minifies vendor scripts into one minified js file
			dist_vendor: {
				files: {
					'<%= concat.scripts_vendor.dest.replace(".js",".min.js") %>': ['<%= concat.scripts_vendor.dest %>']
				}
			}

		},


		/**********************************************************************
		 * JS Hint
		 * Configure 'grunt-contrib-jshint' plugin tasks
		 *********************************************************************/

		jshint: {
			files: sourceFiles.scripts.app
		},


		/**********************************************************************
		 * SASS
		 * Configure 'grunt-sass' plugin tasks
		 *********************************************************************/

		sass: {
			// Concatenate app and vendor SASS files into one large css file
			concat: {
				options: {
					style: 'expanded',
					precision: 5,
					trace: true,
					quiet: false,
					debugInfo: false,
					lineNumbers: true
				},
				files: {
					// c
					'<%= concat.styles.dest.replace(".scss",".css") %>': '<%= concat.styles.dest %>'
				}
			},

			// Concatenate app SASS files into one large css file
			concat_app: {
				options: {
					style: 'expanded',
					precision: 5,
					trace: true,
					quiet: false,
					debugInfo: false,
					lineNumbers: true
				},
				files: {
					'<%= concat.styles_app.dest.replace(".scss",".css") %>': '<%= concat.styles_app.dest %>'
				}
			},

			// Concatenate vendor SASS files into one large css file
			concat_vendor: {
				options: {
					style: 'expanded',
					precision: 5,
					trace: true,
					quiet: false,
					debugInfo: false,
					lineNumbers: true
				},
				files: {
					'<%= concat.styles_vendor.dest.replace(".scss",".css") %>': '<%= concat.styles_vendor.dest %>'
				}
			},

			// Concatenate app and vendor sass file into one large css file and minify
			minify: {
				options: {
					style: 'compressed',
					precision: 5,
					trace: false,
					quiet: false,
					debugInfo: false,
					lineNumbers: false
				},
				files: {
					'<%= concat.styles.dest.replace(".scss",".min.css") %>': '<%= concat.styles.dest %>'
				}
			},

			// Concatenate app sass file into one large css file and minify
			minify_app: {
				options: {
					style: 'compressed',
					precision: 5,
					trace: false,
					quiet: false,
					debugInfo: false,
					lineNumbers: false
				},
				files: {
					'<%= concat.styles_app.dest.replace(".scss",".min.css") %>': '<%= concat.styles_app.dest %>'
				}
			},

			// Concatenate vendor sass file into one large css file and minify
			minify_vendor: {
				options: {
					style: 'compressed',
					precision: 5,
					trace: false,
					quiet: false,
					debugInfo: false,
					lineNumbers: false
				},
				files: {
					'<%= concat.styles_vendor.dest.replace(".scss",".min.css") %>': '<%= concat.styles_vendor.dest %>'
				}
			}
		},


		/**********************************************************************
		 * Image minification
		 * Configure 'grunt-contrib-imagemin' plugin tasks
		 *********************************************************************/

		imagemin: {
			all: {
				files: [{
					expand: true,
					cwd: '<%= watch.images.files.replace("/**","") %>',
					// cwd: appConfig.directories.source + '/' + appConfig.directories.images,
					src: '<%= copy.images.src %>', // ['**/*.{png,jpg,gif,svg}'],
					dest: '<%= copy.images.cwd %>'
					// dest: appConfig.directories.build + '/'  + appConfig.directories.images + '/'
				}]
			}
		}

	};





	/**********************************************************************
	 * INIT CONFIG
	 * Initialize configuration object
	 *********************************************************************/

	grunt.initConfig(defaultTasks);





	/**************************************************************************
	 * LOAD PLUGINS
	 * Load in the Grunt plugins we need.
	 * These should have all been installed through npm.
	 *************************************************************************/

	for(var i=1;i<plugins.length;i++) {
	 	grunt.loadNpmTasks(plugins[i]);
	}





	/**************************************************************************
	 * CREATE COMMANDS
	 * Create new Grunt commands with pre-defined task lists
	 *************************************************************************/

	 for(var i=0;i<customTaskNames.length;i++) {
	 	grunt.registerTask(customTaskNames[i], customTasks[customTaskNames[i]]);
	 }





	/**************************************************************************
	 * START AUTO RUN TASKS
	 * Run tasks that should be run automatically
	 *************************************************************************/

	for(var i=0;i<packageConfig.autorun.length;i++) {
		grunt.task.run(packageConfig.autorun[i]);
	}

};