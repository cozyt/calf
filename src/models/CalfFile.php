<?php

class CalfFile extends CalfModel
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'calf_file';

    /**
     * Append dynamic accessor
     */
    protected $appends = array('url');

    /**
     * Visible properties
     */
    protected $visible = array('id','filename','type','width','height','extension','mime_type','size','title','alt','caption','url',);

	/**
	 * Name of the form index that stores file data
	 *
	 * @var string
	 */
	public $filesKey = 'files';

	/**
	 * Create new file
	 *
	 * @param  array $data
	 * @return object
	 */
	public function createRecord($data, $prepareOnly=false)
	{
		if(is_array($data[$this->filesKey]))
		{
			$return = $this;

			// Should fix the issue with returning empty fieldset
			// on some browsers
			if(count($data[$this->filesKey]))
			{
				foreach($data[$this->filesKey] as $file)
				{
					if($file != '')
					{
						$data[$this->filesKey] = $file;
						$return = $this->createRecord($data, false);
					}
				}
			}

			return $return;
		}
		else
		{
			$filename = $data[$this->filesKey];
			$fileData = self::rawFileData($filename);

	        $this->filename = $filename;
	        $this->type = $fileData['type'];
	        $this->extension = $fileData['extension'];
	        $this->mime_type = $fileData['mime'];
	        $this->size = $fileData['size'];
	        $this->width = isset($fileData['width']) ? $fileData['width'] : '';
	        $this->height = isset($fileData['height']) ? $fileData['height'] : '';
	        $this->bits = isset($fileData['bits']) ? $fileData['bits'] : '';
	        $this->channels = isset($fileData['channels']) ? $fileData['channels'] : '';
	        $this->title = isset($data['title']) ? $data['title'] : $filename;
	        $this->alt = isset($data['alt']) ? $data['alt'] : '';
	        $this->caption = isset($data['caption']) ? $data['caption'] : '';
	        $this->tags = isset($data['tags']) ? $data['tags'] : '';
	        $this->meta = isset($data['meta']) ? $data['meta'] : '';

			parent::createRecord($data, false);

			// Media need to be copied into the public domain
			// so that they play back as quick as possible.

			if(
				($this->type === 'video' || $this->type === 'audio') &&
				Config::get('calf::upload_media_to_public') &&
				! self::exists($this->filename, true)
			)
			{
				File::copy(
					self::path($this->filename, false),
					self::path($this->filename, true)
				);
			}

			return $this;
		}
	}

	/**
	 * Edit existing file
	 *
	 * @param  array $data
	 * @return object
	 */
	public function editRecord($data, $prepareOnly=false)
	{
		if(isset($data[$this->filesKey]))
		{
			if(is_array($data[$this->filesKey]))
			{
				$return = $this;
				$files = $data[$this->filesKey];

				unset($data[$this->filesKey]);

				if(count($files))
				{
					foreach($files as $file)
					{
						if($file != '')
						{
							$data['filename'] = $file;
							$return = $this->editRecord($data, $prepareOnly);
						}
					}
				}

				return $return;
			}
			else
			{
				$data['filename'] = $data[$this->filesKey];
				unset($data[$this->filesKey]);
				return $this->editRecord($data, $prepareOnly);
			}
		}
		else
		{
			$filename = $data['filename'];
			$fileData = self::rawFileData($filename);

			$this->filename = $filename;
			$this->type = $fileData['type'];
			$this->extension = $fileData['extension'];
			$this->mime_type = $fileData['mime'];
			$this->size = $fileData['size'];
			$this->width = isset($fileData['width']) ? $fileData['width'] : '';
			$this->height = isset($fileData['height']) ? $fileData['height'] : '';
			$this->bits = isset($fileData['bits']) ? $fileData['bits'] : '';
			$this->channels = isset($fileData['channels']) ? $fileData['channels'] : '';
			$this->title = isset($data['title']) ? $data['title'] : '';
			$this->alt = isset($data['alt']) ? $data['alt'] : '';
			$this->caption = isset($data['caption']) ? $data['caption'] : '';
			$this->tags = isset($data['tags']) ? $data['tags'] : '';
			$this->meta = isset($data['meta']) ? $data['meta'] : '';

			parent::editRecord($data, $prepareOnly);

			// Media files need to be copied into the public domain
			// so that they play back as quick as possible.

			if(
				($this->type === 'video' || $this->type === 'audio') &&
				Config::get('calf::upload_media_to_public') &&
				! self::exists($this->filename, true)
			)
			{
				File::copy(
					self::path($this->filename, false),
					self::path($this->filename, true)
				);
			}

			// Handle updated image versions
			foreach(self::imageVersions() as $version)
			{
				$versionFieldname = $this->filesKey . '_' . $version;

				if(
					isset($data[$versionFieldname]) &&
					count($data[$versionFieldname])
				)
				{
					$originalPath = $version . DIRECTORY_SEPARATOR . $this->filename;
					$replacePath = $version . DIRECTORY_SEPARATOR . $data[$versionFieldname][1];

					if(
						$originalPath !== $replacePath &&
						self::exists($originalPath) && // original
						self::exists($replacePath) // replacement
					)
					{
						$originalPath = self::path($originalPath);
						$replacePath = self::path($replacePath);

						File::copy($originalPath, $originalPath . '.original');
						// File::copy($replacePath, $replacePath . '.original');
						File::move($replacePath, $originalPath);
					}
				}
			}

			return $this;
		}
	}

    /**
     * Return dynamic accessor
     */

    public function getUrlAttribute()
    {
        return self::url($this->filename, true);
    }

	/**
	 * Determines if the parsed file exists in the file system.
	 *
	 * @param  string $filename [description]
	 * @return boolean
	 */
	static public function exists($filename='', $publicPath=true)
	{
		return File::exists(self::path($filename, $publicPath));
	}

	/**
	 * Return the absolute upload path for a given file ($filename)
	 *
	 * @param  string $filename
	 * @param  bool $publicPath
	 * @return string
	 */
	static public function path($filename='', $publicPath=true)
	{
		// Media files such as audio and video need to be stored in the public domain
		// Therefore, the path may be different for media files

		if($publicPath === true)
		{
	        $type = self::rawType($filename);

			if(
				$type === 'video' ||
				$type === 'audio'
			)
			{
				return Config::get('calf::upload_public_path') . $filename;
			}
		}

		return Config::get('calf::upload_path') . $filename;
	}

	/**
	 * Return the publicly accessible URL for a given file ($filename)
	 *
	 * @param  string  $filename
	 * @param  boolean $withDomain
	 * @return string
	 */
	static public function url($filename='', $withDomain=true)
	{
		return URL::route('calf.uploads', array($filename), $withDomain);
	}

	/**
	 * Returns image versions from config
	 *
	 * @return array
	 */
	static public function imageVersions()
	{
		return array_keys(self::imageVersionSettings());
	}

	/**
	 * Returns image version settings from config
	 *
	 * @param  string $version
	 * @return array
	 */
	static public function imageVersionSettings($version=NULL)
	{
		if($version !== NULL)
		{
			return Config::get('calf::image_versions.'.$version);
		}
		else
		{
			return Config::get('calf::image_versions');
		}
	}

	/**
	 * Determines if the parsed version is a valid version listed in config
	 */
	static public function isValidImageVersion($version)
	{
		return in_array($version, CalfFile::imageVersions());
	}

	/**
	 * [rawFileData description]
	 *
	 * @param  string $filename
	 * @return array
	 */
	static public function rawFileData($filename)
	{
		$info = array(
			'path' => self::path($filename, true),
			'url' => url(self::url($filename)),
			'type' => self::rawType($filename),
			'extension' => self::rawExtension($filename),
			'mime' => self::rawMimeType($filename),
			'size' => self::rawSize($filename).' bytes',
		);

		switch($info['type'])
		{
			case 'image' :
				$i = self::rawImageSize($filename);

				$info['width'] = $i[0];
				$info['height'] = $i[1];
				$info['bits'] = @$i['bits'];
				$info['channels'] = @$i['channels'];
				break;

			case 'audio' :
			case 'video' :
				$info['storage_path'] = self::path($filename, false);
				break;
		}

		return $info;
	}

	/**
	 * [rawExtension description]
	 *
	 * @param  string $filename
	 * @return string
	 */
	static public function rawExtension($filename)
	{
		return pathinfo($filename, PATHINFO_EXTENSION);
	}

	/**
	 * [rawMimeType description]
	 *
	 * @param  string $filename
	 * @return string
	 */
	static public function rawMimeType($filename)
	{
		if(self::exists($filename, false))
		{
			$path = self::path($filename, false);
	        $finfo = new finfo;

	        return $finfo->file($path, FILEINFO_MIME_TYPE);
		}
		else
		{
			return 'application/octet-stream';
		}
	}

	/**
	 * [rawSize description]
	 *
	 * @param  string $filename
	 * @return string
	 */
	static public function rawSize($filename)
	{
		return filesize(self::path($filename, false));
	}

	/**
	 * [rawImageSize description]
	 *
	 * @param  string $filename
	 * @return string
	 */
	static public function rawImageSize($filename)
	{
		return getimagesize(self::path($filename, false));
	}

	/**
	 * [rawType description]
	 *
	 * @param  string $filename
	 * @return string
	 */
	static public function rawType($filename)
	{
		return self::rawTypeFromMime(self::rawMimeType($filename));
	}

	/**
	 * [rawTypeFromMime description]
	 *
	 * @param  string $mime
	 * @return string
	 */
	static public function rawTypeFromMime($mime)
	{
		switch ($mime)
		{
            case 'image/png' :
			case 'image/jpg' :
            case 'image/jpeg' :
            case 'image/gif' :
            case 'image/bmp' :
            case 'image/vnd.microsoft.icon' :
            case 'image/tiff' :
            case 'image/svg+xml' :
				return 'image';

			case 'video/mp4' :
			case 'video/quicktime' :
			case 'video/mov' :
			case 'video/swf' :
			case 'application/x-shockwave-flash' :
			case 'video/flv' :
			case 'video/x-flv' :
			case 'video/msvideo' :
			case 'video/x-ms-wmv' :
				return 'video';

			case 'audio/mpeg' :
			case 'audio/mp3' :
			case 'audio/wav' :
			case 'audio/aiff' :
				return 'audio';

			case 'application/zip' :
            case 'application/x-rar-compressed' :
            case 'application/x-msdownload' :
            case 'application/x-msdownload' :
            case 'application/vnd.ms-cab-compressed' :
            	return 'archive';

            case 'application/octet-stream' :
			default :
				return 'file';
        }
	}

	/**
	 * Returns a string of the file type icon.
	 */

	static public function typeIcon($type)
	{
		switch($type)
		{
			case 'image' :
				return glyphicon('image');

			case 'video' :
				return glyphicon('video');

			case 'audio' :
				return glyphicon('audio');

			default :
				return glyphicon('file');
		}
	}


	/**
	 * Renders the file as an asset with the appropriate HTML tags for the
	 * asset type.
	 */

	static public function asset($filename, $attr='')
	{
		$type = self::rawType($filename);

		switch($type)
		{
			case 'image' :
				return '<img src="' . self::url($filename) . '" ' . $attr . '/>';

			case 'video' :
				return '
				<video controls ' . $attr . '>
					<source src="' . self::url($filename) . '" type="' . self::rawMimeType($filename) . '">
				</video>';

			case 'audio' :
				return '
				<audio controls ' . $attr . '>
					<source src="' . self::url($filename) . '" type="' . self::rawMimeType($filename) . '">
				</audio>';

			default :
				return self::typeIcon('file');
		}
	}


    /**************************************************
     * Filter scopes
    **************************************************/
	/**
	 * Scope filter by filename
	 *
	 * @param  object $query
	 * @param  string $type
	 * @return object
	 */
	public function scopeFilterByFilename($query, $filename)
	{
		return $query->where('filename', '=', $filename);
	}

	/**
	 * Scope filter by extension
	 *
	 * @param  object $query
	 * @param  string $extension
	 * @return object
	 */
	public function scopeFilterByExtension($query, $extension)
	{
		return $query->where('extension', '=', $extension);
	}

	/**
	 * Scope filter by mime type
	 *
	 * @param  object $query
	 * @param  string $mimeType
	 * @return object
	 */
	public function scopeFilterByMimeType($query, $mimeType)
	{
		return $query->where('mime_type', '=', $mimeType);
	}

	/**
	 * Scope filter by size
	 *
	 * @param  object $query
	 * @param  int $size
	 * @param  string $scope
	 * @return object
	 */
	public function scopeFilterBySize($query, $size, $scope='=')
	{
		return $query->where('size', $scope, $size);
	}

	/**
	 * Scope filter by width
	 *
	 * @param  object $query
	 * @param  int $width
	 * @param  string $scope
	 * @return object
	 */
	public function scopeFilterByWidth($query, $width, $scope='=')
	{
		return $query->where('width', $scope, $width);
	}

	/**
	 * Scope filter by height
	 *
	 * @param  object $query
	 * @param  int $height
	 * @param  string $scope
	 * @return object
	 */
	public function scopeFilterByHeight($query, $height, $scope='=')
	{
		return $query->where('height', $scope, $height);
	}

	/**
	 * Scope filter by bits
	 *
	 * @param  object $query
	 * @param  int $bits
	 * @param  string $scope
	 * @return object
	 */
	public function scopeFilterByBits($query, $bits, $scope='=')
	{
		return $query->where('', $scope, $bits);
	}

	/**
	 * Scope filter by channels
	 *
	 * @param  object $query
	 * @param  int $channels
	 * @param  string $scope
	 * @return object
	 */
	public function scopeFilterByChannels($query, $channels, $scope='=')
	{
		return $query->where('', $scope, $value);
	}

    /**************************************************
     * Relationships
     **************************************************/
	/**
	 * File belongs to many content records
	 *
	 * @return  object
	 */
	public function contents()
	{
		return $this->belongsToMany('CalfContent', CalfContentFile::tableName(), 'file_id', 'content_id');
	}

	/**
	 * A File has many contentFiles on file id
	 *
	 * @return  object
	 */
	public function contentFiles()
	{
		return $this->hasMany('CalfContentFile', 'file_id');
	}





    /**************************************************************************
     * Utilities
     *************************************************************************/

    /**
     * Determines if the current model allows revisions to be stored
     */

    static public function revisionsAllowed($permission=NULL)
    {
        return parent::revisionsAllowed('file');
    }

}
