<?php

/**
 * CalfModel
 */
abstract class CalfModel extends Eloquent
{
    /**
     * The database table used by the model.
     * @var string
     */

    protected $table = '';


    /**
     * Define relationship properties for this model
     * @var array
     */

    protected $relationshipProperties = array();


    /**
     * Allow revisioning
     * @var boolean
     */

    // protected $allowRevisions = true;


    /**
     * Name of the column to store revision ids in
     * @var string
     */

    protected $revisionField = 'revision_id';


    /**
     * Create record
     *
     * @param array $data
     * @param  bool $prepareOnly
     * @return object
     */

    public function createRecord($data, $prepareOnly=false)
    {
        $this->setCreatedMeta()
             ->setPublishedMeta(isset($data['is_published']), isset($data['published_at']) ? $data['published_at'] : '')
             ->setPriority(isset($data['priority']) ? $data['priority'] : 0);

        if($prepareOnly === false)
        {
            $this->save();
        }

        return $this;
    }


    /**
     * Edit record
     *
     * @param array $data
     * @param  bool $prepareOnly
     * @return object
     */

    public function editRecord($data, $prepareOnly=false)
    {
        if(isset($data['priority']))
        {
            $this->setPriority($data['priority']);
        }

        $this->setUpdatedMeta()
             ->setPublishedMeta(isset($data['is_published']), isset($data['published_at']) ? $data['published_at'] : '')
             ->setDeletedMeta(isset($data['is_deleted']));

        if($prepareOnly === false)
        {
            $this->save();
        }

        return $this;
    }


    /**
     * Creates a new revision for the record
     *
     * @param  string $permission Name of permission that revision relates to
     * @param  string $notes
     * @return object
     */

    public function createNewRevision($permission, $notes='')
    {
        $revisionField = $this->revisionField;
        $relationshipProperties = $this->relationshipProperties;

        $data = $this->toArray();

        // So that users like contributors can only submit draft revisions,
        // we must make sure that the publishing fields are reset for this revision
        if(CalfHelper::authUserTypeIsAuthorized('editor', '>=') === false)
        {
            $data['published_by'] = CalfHelper::authUserId();
            $data['published_at'] = '0000-00-00 00:00:00';
            $data['is_published'] = 0;
        }

        // Handle relationships on the current model
        if(count($relationshipProperties))
        {
            foreach($relationshipProperties as $prop)
            {
                $data[$prop] = $this->$prop->toArray();
            }
        }

        // Store the new revision
        $revision = new CalfRevision;

        $revision->parent_id = isset($data[$revisionField]) ? $data[$revisionField] : 0;
        $revision->record_id = $data['id'];
        $revision->permission = $permission;
        $revision->data = $data;
        $revision->notes = $notes;

        $revision->save();

        // So that users like contributors can only submit draft revisions,
        // the record should only be updated with the revision id by users
        // that are authorised to do so
        if(CalfHelper::authUserTypeIsAuthorized('editor', '>=') === true)
        {
            $this->setUpdatedMeta()
                 ->setRevisionId($revision->id)
                 ->save();
        }

        return $this->load('currentRevision');
    }






    /**************************************************
     * General scopes
    **************************************************/

    /**
     * Scope order by priority
     *
     * @param string $direction ASC|DESC
     * @return object
     */
    public function scopeOrderByPriority($query, $direction='ASC')
    {
        return $query->orderBy($this->table.'.priority', $direction);
    }





    /**************************************************
     * Filter scopes
    **************************************************/
    /**
     * Scope filter
     *
     * @param array $data
     * @param array $keywordFields
     * @return object
     */
    public function scopeFilter($query, $data, $keywordFields=array())
    {
        if(isset($data['id']) && $data['id'] != '')
        {
            $query->filterByPrimaryKey($data['id']);
        }

        if(isset($data['tags']) && $data['tags'] != '')
        {
            $query->filterByKeyword($keywordFields, $data['tags'], isset($data['tags_scope']) ? $data['tags_scope'] : '');
        }

        if(isset($data['created_by']) && $data['created_by'] != '')
        {
            $query->filterByCreatedBy($data['created_by']);
        }

        if(isset($data['created_at']) && $data['created_at'] != '')
        {
            $query->filterByCreatedAt($data['created_at'], isset($data['created_at_scope']) ? $data['created_at_scope'] : '');
        }

        if(isset($data['updated_by']) && $data['updated_by'] != '')
        {
            $query->filterByUpdatedBy($data['updated_by']);
        }

        if(isset($data['updated_at']) && $data['updated_at'] != '')
        {
            $query->filterByUpdatedAt($data['updated_at'], isset($data['updated_at_scope']) ? $data['updated_at_scope'] : '');
        }

        if(isset($data['published_by']) && $data['published_by'] != '')
        {
            $query->filterByPublishedBy($data['published_by']);
        }

        if(isset($data['published_at']) && $data['published_at'] != '')
        {
            $query->filterByPublishedAt($data['published_at'], isset($data['published_at_scope']) ? $data['published_at_scope'] : '');
        }

        if(isset($data['is_published']) && $data['is_published'] != '')
        {
            $query->filterByIsPublished($data['is_published']);
        }

        return $query;
    }

    /**
     * Scope filter by primary key
     *
     * @param int $value
     * @return object
     */
    public function scopeFilterByPrimaryKey($query, $value)
    {
        return $query->where($this->primaryKey, '=', $value);
    }

    /**
     * Scope filter by type
     *
     * @param string $type
     * @return object
     */
    public function scopeFilterByType($query, $type)
    {
        return $query->where('type', '=', $type);
    }

    /**
     * Scope filter by published
     *
     * @param int|bool $value 1|0
     * @return object
     */
    public function scopeFilterByIsPublished($query, $value=1)
    {
		return $query->where('is_published', '=', $value);
    }

    /**
     * Scope filter by deleted
     *
     * @param int|bool $value 1|0
     * @return object
     */
    public function scopeFilterByIsDeleted($query, $value=1)
    {
    	return $query->where('is_deleted', '=', $value);
    }

    /**
     * Scope filter by keyword
     *
     * @param array $columns
     * @param string $value
     * @param string $scope
     * @return object
     * @see CalfFilter::keyword()
     */
    public function scopeFilterByKeyword($query, $columns, $value, $scope='phrase')
    {
        return CalfFilter::keyword($query, $columns, $value, $scope);
    }

    /**
     * Scope filter by date
     *
     * @param array $columns
     * @param string $value
     * @param string $scope
     * @return object
     * @see CalfFilter::date()
     */
    public function scopeFilterByDate($query, $column, $value, $scope='on')
    {
        return CalfFilter::date($query, $column, $value, $scope);
    }

    /**
     * Scope filter by created at
     *
     * @param string $value
     * @param string $scope
     * @return object
     * @see self::scopeFilterByDate()
     */
    public function scopeFilterByCreatedAt($query, $value, $scope='on')
    {
        return $query->filterByDate('created_at', $value, $scope);
    }

    /**
     * Scope filter by updated at
     *
     * @param string $value
     * @param string $scope
     * @return object
     * @see self::scopeFilterByDate()
     */
    public function scopeFilterByUpdatedAt($query, $value, $scope='on')
    {
        return $query->filterByDate('updated_at', $value, $scope);
    }

    /**
     * Scope filter by published at
     *
     * @param string $value
     * @param string $scope
     * @return object
     * @see self::scopeFilterByDate()
     */
    public function scopeFilterByPublishedAt($query, $value, $scope='on')
    {
        return $query->filterByDate('published_at', $value, $scope);
    }

    /**
     * Scope filter by created by
     *
     * @param int $value
     * @return object
     */
    public function scopeFilterByCreatedBy($query, $value)
    {
        return $query->where('created_by', '=', $value);
    }

    /**
     * Scope filter by updated by
     *
     * @param int $value
     * @return object
     */
    public function scopeFilterByUpdatedBy($query, $value)
    {
        return $query->where('updated_by', '=', $value);
    }

    /**
     * Scope filter by published by
     *
     * @param int $value
     * @return object
     */
    public function scopeFilterByPublishedBy($query, $value)
    {
        return $query->where('published_by', '=', $value);
    }

    /**
     * Published
     * An item is considered published when:
     * - The item has been published (The is_published field is set to 1)
     * - The record was published before NOW
     *
     * @param  $query $query
     * @return object
     */
    public function scopeFilterByPublished($query)
    {
        return $query
            ->filterByIsPublished(1)
            ->filterByPublishedAt(date('Y-m-d H:i:s'), '<=');
    }

    /**
     * Queued
     * An item is considered queued when:
     * - The item has been published (The is_published field is set to 1)
     * - The record is to be published any time in the future
     *
     * @param  $query $query
     * @return object
     */
    public function scopeFilterByQueued($query)
    {
        return $query
            ->filterByIsPublished(1)
            ->filterByPublishedAt(date('Y-m-d H:i:s'), '>');
    }

    /**
     * Unpublished
     * An item is considered unpublished when:
     * - The item is not published (The is_published field is set to 0)
     * - The record was published at some point in time
     *
     * @param  $query $query
     * @return object
     */
    public function scopeFilterByUnpublished($query)
    {
        return $query
            ->filterByIsPublished(0)
            ->filterByPublishedAt('0000-00-00 00:00:00', '>');
    }

    /**
     * Drafts
     * An item is considered a draft when:
     * - The item is not published (The is_published field is set to 0)
     * - The record has never been published
     *
     * @param  $query $query
     * @return object
     */
    public function scopeFilterByDraft($query)
    {
        return $query
            ->filterByIsPublished(0)
            ->filterByPublishedAt('0000-00-00 00:00:00', '=');
    }





    /**************************************************
     * Create/Edit accessors
    **************************************************/

    /**
     * Scope set created meta
     *
     * @return object
     */
    public function scopeSetCreatedMeta($query)
    {
        $this->created_by = CalfHelper::authUserId();
        $this->created_at = date('Y-m-d H:i:s');

        return $this;
    }

    /**
     * Scope set updated meta
     *
     * @return object
     */
    public function scopeSetUpdatedMeta($query)
    {
        $this->updated_by = CalfHelper::authUserId();
        $this->updated_at = date('Y-m-d H:i:s');

        return $this;
    }

    /**
     * Scope set published meta
     *
     * @param  object  $query
     * @param  boolean $publish
     * @param  string  $publishDate
     * @return object
     */
    public function scopeSetPublishedMeta($query, $publish=false, $publishDate=NULL)
    {
        if(CalfHelper::authUserTypeIsAuthorized('editor', '>='))
        {
            if($publish)
            {
                if(! $publishDate || $publishDate == '0000-00-00 00:00:00')
                {
                    $publishDate = date('Y-m-d H:i:s');
                }

                $this->published_by = CalfHelper::authUserId();
                $this->published_at = $publishDate;
                $this->is_published = 1;
            }
            else
            {
                $this->is_published = 0;
            }
        }
        else
        {
            if(! isset($this->is_published))
            {
                $this->is_published = 0;
            }
        }

        return $this;
    }

    /**
     * Scope set deleted meta
     *
     * @param bool $delete
     * @return object
     */
    public function scopeSetDeletedMeta($query, $delete=false)
    {
        if(CalfHelper::authUserTypeIsAuthorized('editor', '>='))
        {
            if($delete)
            {
                $this->is_deleted = 1;
            }
            else
            {
                $this->is_deleted = 0;
            }
        }

        return $this;
    }

    /**
     * Scope set priority
     *
     * @param int $priority
     * @return object
     */
    public function scopeSetPriority($query, $priority=0)
    {
        $this->priority = $priority;

        return $this;
    }


    /**
     * Scope set revision id
     *
     * @param int $priority
     * @return object
     */

    public function scopeSetRevisionId($query, $revisionId)
    {
        $revisionField = $this->revisionField;

        $this->$revisionField = $revisionId;

        return $this;
    }





    /**************************************************
     * Relationships
     **************************************************/

    /**
     * Forces loading of relationships defined in relationshipProperties property
     */

    public function touchRelations()
    {
        if($this->relationshipProperties)
        {
            foreach($this->relationshipProperties as $relationship)
            {
                $this->$relationship()->get();
            }
        }

        return $this;
    }

    /**
     * Model belongs to User on created by
     */
    public function createdBy()
    {
        return $this->belongsTo('CalfUser', 'created_by');
    }

    /**
     * Model belongs to User on updated by
     */
    public function updatedBy()
    {
        return $this->belongsTo('CalfUser', 'updated_by');
    }

    /**
     * Model belongs to User on published by
     */
    public function publishedBy()
    {
        return $this->belongsTo('CalfUser', 'published_by');
    }


    /**
     * Returns all revisions for a record
     */

    public function revisions()
    {
        return $this->hasMany('CalfRevision', 'record_id');
    }


    /**
     * Return the current revision for the record as an int
     */

    public function currentRevision()
    {
        return $this->hasOne('CalfRevision', 'id', 'revision_id');
    }




    /**************************************************
     * Utilities
    ***************************************************/
    /**
     * Primary key
     */
    static public function primaryKey()
    {
        return with(new static)->primaryKey;
    }

    /**
     * Table name
     */
    static public function tableName()
    {
        return with(new static)->getTable();
    }

    /**
     * Revision field
     */

    static public function getRevisionField()
    {
        return with(new static)->revisionField;
    }

    /**
     * Lists currently defined relationships in the relationshipProperties 
     * property. This is NOT the equivalent of the 
     * Eloquent::getRelations() method
     */

    static public function getRelationshipProperties()
    {
        return with(new static)->relationshipProperties;
    }

    /**
     * Last query
     */
    static public function lastQuery()
    {
        $queries = DB::getQueryLog();
        return end($queries);
    }


    /**
     * Determines if the current model's table contains a column named $revisionField
     */

    static public function hasRevisionField()
    {
        return Schema::hasColumn(self::tableName(), self::getRevisionField());
    }


    /**
     * Determines if the current model allows revisions to be stored
     */

    static public function revisionsAllowed($permission=NULL)
    {
        if($permission !== NULL)
        {
            $permission = CalfPermission::find($permission);

            return  self::hasRevisionField() &&
                    isset($permission->revisions_allowed) &&
                    $permission->revisions_allowed;
        }
        else
        {
            return self::hasRevisionField(); // && with(new static)->allowRevisions;
        }
    }


}
