<?php

abstract class CalfPermissionType
{
	static private $instance;
	static private $configName = 'calf::permission_types';

	/**
	 * Create instance of permission type and loads permission type data
	 *
	 * @return object
	 */
	static private function init()
	{
		$data = Config::get(self::$configName);

		foreach($data as $name => $settings)
		{
			$data[$name]['id'] = $name;

			$data[$name] = (object) $data[$name];
		}

		self::$instance = (object) $data;

		return self::$instance;
	}

	/**
	 * Return permission types
	 *
	 * @return object
	 */
	static public function get()
	{
		if(! self::$instance)
		{
			self::init();
		}

		return self::$instance;
	}

	/**
	 * Find given permission type
	 *
	 * @param  string $id
	 * @return object
	 */
	static public function find($id)
	{
		if(! self::$instance)
		{
			self::init();
		}

		return isset(self::$instance->$id) ? self::$instance->$id : new stdClass;
	}

	/**
	 * Return count in current result set
	 *
	 * @return int
	 */
	static public function count()
	{
		if(! self::$instance)
		{
			self::init();
		}

		return count(self::$instance);
	}
}