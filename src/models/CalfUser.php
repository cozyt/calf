<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class CalfUser extends CalfModel implements UserInterface, RemindableInterface
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'calf_user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * The database table used for password reminders
	 *
	 * @var string
	 */
	static public $reminder_table = 'calf_password_reminders';

	/**
	 * Create record
	 * @param  array $data
	 * @return object
	 */
	public function createRecord($data, $prepareOnly=false)
	{
        $this->type = isset($data['type']) ? $data['type'] : '';
        $this->username = isset($data['username']) ? $data['username'] : '';
        $this->firstname = isset($data['firstname']) ? $data['firstname'] : '';
        $this->surname = isset($data['surname']) ? $data['surname'] : '';
        $this->tags = isset($data['tags']) ? $data['tags'] : '';
        $this->meta = isset($data['meta']) ? $data['meta'] : '';

        $this->password = Hash::make($data['new_password']);

		$record = parent::createRecord($data, false);

		if($record->id)
		{
	        CalfNotification::userCreated($data);
		}

		return $record;
	}

	/**
	 * Edit record
	 * @param  array $data
	 * @return object
	 */
	public function editRecord($data, $prepareOnly=false)
	{
        $this->type = isset($data['type']) ? $data['type'] : '';
        $this->username = isset($data['username']) ? $data['username'] : '';
        $this->firstname = isset($data['firstname']) ? $data['firstname'] : '';
        $this->surname = isset($data['surname']) ? $data['surname'] : '';
        $this->tags = isset($data['tags']) ? $data['tags'] : '';
        $this->meta = isset($data['meta']) ? $data['meta'] : '';

        if(isset($data['new_password']) && $data['new_password'])
        {
            $this->password = Hash::make($data['new_password']);
        }

		return parent::editRecord($data, $prepareOnly);
	}


	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Fullname accessor
	 *
	 * @param  boolean $abbr
	 * @return string
	 */
	public function fullname($abbr=false)
	{
		if($abbr === true)
		{
			return substr($this->firstname, 0, 1) . '. ' . $this->surname;
		}
		else
		{
			return $this->firstname . ' ' . $this->surname;
		}
	}





    /**************************************************************************
     * Utilities
     *************************************************************************/

    /**
     * Determines if the current model allows revisions to be stored
     */

    static public function revisionsAllowed($permission=NULL)
    {
        return parent::revisionsAllowed('user');
    }

}
