<?php

/**
 * Calf Notification
 */
class CalfNotification
{
	/**
	 * Send email notifications to recipient
	 *
	 * @param  string $to
	 * @return bool
	 */
	static public function send($view, $to, $subject='', $data=array())
	{
		if(Config::get('calf::send_notifications') === true)
		{
			return Mail::send($view, $data, function($message) use ($to, $subject)
			{
			    $message->to($to);

			    if($subject)
			    {
			    	$message->subject($subject);
			    }
			});
		}
		else
		{
			return false;
		}
	}

	/**
	 * Sends a notification email to editors when record is created by a
	 * contributor. Created record notifications are only sent to editors
	 * and only sent when created by a contributor.
	 *
	 * This can be disabled in config by setting the send_notifications
	 * setting to false.
	 *
	 * @param  array $data
	 * @return void
	 */
	static public function recordCreated($data)
	{
		if(Config::get('calf::send_notifications') === true && CalfHelper::authUserIsType('contributor'))
		{
		    $calf = Config::get('calf::call_me');
		    $view = Config::get('calf::email_views.record_created');
		    $editors = CalfUser::filterByType('editor')->lists('username');

		    if(count($editors) && $view)
		    {
			    foreach($editors as $editor)
			    {
					self::send($view, $editor, $calf.' record #'.' has been created', $data);
			    }
		    }
		}
	}


	/**
	 * Sends the new user a notification that an account has been created
	 *
	 * @param  array $data
	 * @return void
	 */
	static public function userCreated($data)
	{
		if(Config::get('calf::send_notifications') === true)
		{
			$calf = Config::get('calf::call_me');
			$view = Config::get('calf::email_views.user_created');

			self::send($view, $data['username'], 'User account created', $data);
		}
	}
}