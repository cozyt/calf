<?php

class CalfPermission
{
	static private $instance;
	static private $configName = 'calf::permissions';

	/**
	 * Create instance of permission and loads permission data
	 *
	 * @return object
	 */
	static private function init()
	{
		$data = Config::get(self::$configName);

		foreach($data as $name => $settings)
		{
			$data[$name]['id'] = $name;

			$data[$name] = (object) $data[$name];
		}

		self::$instance = (object) $data;

		return self::$instance;
	}

	/**
	 * Return permissions
	 *
	 * @return object
	 */
	static public function get()
	{
		if(! self::$instance)
		{
			self::init();
		}

		return self::$instance;
	}

	/**
	 * Find given permission
	 *
	 * @param  string $id
	 * @return object
	 */
	static public function find($id)
	{
		if(! self::$instance)
		{
			self::init();
		}

		return isset(self::$instance->$id) ? self::$instance->$id : array();
	}

	/**
	 * Return count in current result set
	 *
	 * @return int
	 */
	static public function count()
	{
		if(! self::$instance)
		{
			self::init();
		}

		return count(self::$instance);
	}

    /**************************************************
     * Filter scopes
    **************************************************/
    /**
     * Filter result set by type
     *
     * @param  object $query
     * @param  string $type
     * @return object
     */
	static public function filterByType($query, $type)
	{
		$return = new StdClass;

		foreach($query as $id => $q)
		{
			if($q->type == $type)
			{
				$return->$id = $q;
			}
		}

		return $return;
	}
}