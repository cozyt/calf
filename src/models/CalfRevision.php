<?php

use Carbon\Carbon;

/**
 * CalfRevision
 */
class CalfRevision extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'calf_revision';

    /**
     * Turn off automatic timestamps fields created by Eloquent
     *
     * @var bool
     */

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['record_id','permission','data'];


    /**
     * Returns the revisions for a specified record
     */

    public function getRevisionsByRecord($recordId)
    {
        return $this->filterByRecordId($recordId)
                    ->orderBy('id', 'DESC')
                    ->get();
    }


    /**
     * Returns the latest revision for a specified record
     */

    public function getLatestRevisionForRecord($recordId)
    {
        return $this->filterByRecordId($recordId)
                    ->orderBy('id', 'DESC')
                    ->limit(1)
                    ->get()
                    ->first();
    }


    /**
     * Returns the data property unserialized
     */

    public function getDataAttribute($value)
    {
        return json_decode($value);
    }


    /**
     * Serializes data property for storage
     */

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = json_encode($value);
    }


    /**
     * Returns the timestamp that the revision was created at
     */

    public function createdAt()
    {
        if(
            isset($this->data->updated_at) &&
            $this->data->updated_at !== '0000-00-00 00:00:00'
        )
        {
            $date = $this->data->updated_at;
        }
        else
        {
            $date = $this->data->created_at;
        }

        return new Carbon($date);
    }


    /**
     * Returns the the profile of the user that created the revision
     */

    public function createdBy()
    {
        if(
            isset($this->data->updated_by) &&
            $this->data->updated_by !== 0
        )
        {
            $userId = $this->data->updated_by;
        }
        else
        {
            $userId = $this->data->created_by;
        }

        return CalfUser::find($userId);
    }


    /**
     * Scope filter by record_id
     * @param int $recordId
     * @return object
     */

    public function scopeFilterByRecordId($query, $recordId)
    {
        return $query->where('record_id', '=', $recordId);
    }

}
