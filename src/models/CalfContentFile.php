<?php

/**
 * CalfContentFile
 */
class CalfContentFile extends CalfModel
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'calf_content_file';

	/**
	 * Turn off automatic timestamps fields created by Eloquent
	 *
	 * @var bool
	 */
	public $timestamps = false;

    /**************************************************
     * Relationships
     **************************************************/
	/**
	 * Content file belongs to Content on content id
	 */
	public function contents()
	{
		return $this->belongsTo('CalfContent', 'content_id');
	}

    /**
     * Content file belongs to File on file id
     */
    public function files()
    {
    	return $this->belongsTo('CalfFile', 'file_id');
    }

}