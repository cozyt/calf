<?php

class CalfTag
{
	/**
	 * Tags table column name
	 *
	 * @var  string
	 */
	private $column = '';

	/**
	 * Registered models to search in
	 *
	 * @var array|string
	 */
	private $models = array();

	/**
	 * Stores all keyword tags
	 *
	 * @var array
	 */
	private $tags = array();

	/**
	 * Init
	 */
	public function __construct($models, $column='tags')
	{
		$this->load($models, $column);
	}

	/**
	 * Returns all loaded tags
	 *
	 * @return array
	 */
	public function get()
	{
		return $this->tags;
	}

	/**
	 * Return the first tag in the current recordset
	 *
	 * @return array
	 */
	public function first()
	{
		return $this->top(1);
	}

	/**
	 * Return the first $limit tags in current recordset
	 *
	 * @param  int $limit
	 * @return array
	 */
	public function top($limit=1)
	{
		return $this->limit($limit, 0);
	}

	/**
	 * Limit recordset
	 *
	 * @param  integer $upper
	 * @param  integer $lower
	 * @return object
	 */
	public function limit($upper=1, $lower=0)
	{
		$this->tags = array_slice($this->get(), $lower, $upper);

		return $this;
	}

	/**
	 * Returns a count of current recordset
	 *
	 * @return int
	 */
	public function count()
	{
		return count($this->get());
	}


	/**
	 * Returns distinct tags as array key with a count of number of times used.
	 * If a tag is defined, then the count for that tag is returned.
	 *
	 * @param string $tag
	 * @return array|int
	 */
	public function tagCount($tag='')
	{
		$tags = array_count_values($this->get());

		arsort($tags);

		if($tag)
		{
			if(isset($tags[$tag]))
			{
				return $tags[$tag];
			}
			else
			{
				return 0;
			}
		}

		return $tags;
	}


	/**
	 * Returns usages stats about the current record set
	 *
	 * @param  integer $tagCount    Total number of tags found
	 * @param  integer $recordCount Total number of records
	 * @return array
	 */
	public function stats($tagCount=0, $recordCount=0)
	{
		$tags = array();

        foreach($this->tagCount() as $tag => $count)
        {
            $temp = new StdClass;
            $temp->title = $tag;
            $temp->instances = $count;
            $temp->percent_tag = $tagCount > 0 ? 100 / $tagCount * $count : 0;
            $temp->percent_record = $recordCount > 0 ? 100 / $recordCount * $count : 0;

            $tags[] = $temp;
        }

        return $tags;
	}


	/**
	 * Load tags from database
	 *
	 * @param array|string $models
	 * @param string $column
	 * @return object
	 */
	public function load($models=NULL, $column='')
	{
        $rows = array();
        $this->tags = array();

        if($models !== NULL)
        {
        	$this->models = $models;

	        if(is_string($this->models))
	        {
	        	$this->models = array($this->models);
	        }
        }

        if($column !== '')
        {
        	$this->column = $column;
        }

        foreach($this->models as $model)
        {
			$rows = array_merge($rows, $model::where($this->column, '!=', '')->lists($this->column));
        }

        foreach($rows as $row)
        {
        	$this->tags = array_merge($this->get(), explode(',', $row));
        }

        foreach($this->get() as $key => $tag)
        {
        	$this->tags[$key] = trim($tag);
        }

        $this->sort('asc');

        return $this;
	}

	/**
	 * Filter distinct tags
	 *
	 * @return object
	 */
	public function filterByDistinct()
	{
		$this->tags = array_unique($this->get());

		return $this;
	}

	/**
	 * Returns tags where that have been used >= $count times
	 *
	 * @param int $count
	 * @return array
	 */
	public function filterByPopularity($count=5)
	{
		$tags = array();

		foreach($this->get() as $key => $tag)
		{
			if($count <= $this->tagCount($tag))
			{
				$tags[] = $tag;
			}
		}

		$this->tags = $tags;

		return $this;
	}

	/**
	 * Sort result
	 *
	 * @param  string $direction
	 * @return object
	 */
	public function sort($direction='asc')
	{
		if($direction == 'desc')
		{
			rsort($this->tags);
		}
		else
		{
			sort($this->tags);
		}

		return $this;
	}
}