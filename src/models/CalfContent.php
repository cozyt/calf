<?php

/**
 * CalfContent
 */
class CalfContent extends CalfModel
{
	/**
	 * The database table used by the model.
	 * @var string
	 */

	protected $table = 'calf_content';


    /**
     * Define relationship properties for this model
     * @var array
     */

    protected $relationshipProperties = array('files');


    /**
     * The content type used by the model.
     * @var string
     */

    protected $contentType = '';


	/**
	 * Name of the form index that stores file data
	 * @var string
	 */

	public $filesKey = 'files';


	/**
	 * Name of the form index that stores new linked asset data from asset library
	 * @var string
	 */

	public $assetsKey = 'assets';


    /**
     * New queries. Automatically filters queries for the model by the defined type
     */
    
    public function newQuery()
    {
        $query = parent::newQuery();

        if($this->contentType !== '')
        {
            $query->filterByType($this->contentType);
        }

        return $query;
    }


	/**
	 * Create new content
	 *
	 * @param  array $data
	 * @param  array $files
	 * @return object
	 */
    public function createRecord($data, $prepareOnly=false)
    {
    	$this->setType($this->contentType)
             ->setSlug(isset($data['slug']) ? $data['slug'] : '', $data['title']);

		$this->parent_id = isset($data['parent_id']) ? $data['parent_id'] : 0;
    	$this->layout = isset($data['layout']) ? $data['layout'] : '';
        $this->title = $data['title'];
        $this->short_title = isset($data['short_title']) && $data['short_title'] ? $data['short_title'] : $data['title'];
        $this->lead = isset($data['lead']) ? $data['lead'] : '';
        $this->body = isset($data['body']) ? $data['body'] : '';
        $this->tags = isset($data['tags']) ? $data['tags'] : '';
        $this->meta = isset($data['meta']) ? $data['meta'] : '';

        parent::createRecord($data, false);

        $assetPriority = 0;

        if($this->hasUploadedFiles($data))
        {
			$assetPriority = count($data[$this->filesKey]);

			foreach($data[$this->filesKey] as $priority => $filename)
			{
			   	if($filename != '')
			   	{
			        $file = new CalfFile;
			        $file->createRecord(array('priority' => $assetPriority + $priority, 'files' => $filename));

						$this->files()->save($file);
			   	}
			}
        }

        if($this->hasNewLinkedAssets($data))
        {
        	foreach($data[$this->assetsKey] as $priority => $assetId)
        	{
				$this->files()->attach($assetId, array('priority' => $assetPriority + $priority));
        	}
        }

        if(self::revisionsAllowed())
        {
            $this->createNewRevision($this->contentType);
        }

        return $this;
    }

    /**
     * Edit existing content
     * @param  array $data
     * @return object
     */
    public function editRecord($data, $prepareOnly=false)
    {
        $this->setType($this->contentType)
             ->setSlug(isset($data['slug']) ? $data['slug'] : '', $data['title']);

		$this->parent_id = isset($data['parent_id']) ? $data['parent_id'] : 0;
    	$this->layout = isset($data['layout']) ? $data['layout'] : '';
        $this->title = $data['title'];
        $this->short_title = isset($data['short_title']) && $data['short_title'] ? $data['short_title'] : $data['title'];
        $this->lead = isset($data['lead']) ? $data['lead'] : '';
        $this->body = isset($data['body']) ? $data['body'] : '';
        $this->tags = isset($data['tags']) ? $data['tags'] : '';
        $this->meta = isset($data['meta']) ? $data['meta'] : '';

        parent::editRecord($data, $prepareOnly);

        $assetPriority = 0;

		if($this->hasUploadedFiles($data))
		{
			$assetPriority = count($data[$this->filesKey]);

			foreach($data[$this->filesKey] as $priority => $filename)
			{
				if($filename != '')
				{
					$file = $this->files()->filterByFilename($filename);

					// Existing associated file
					if($file->count())
					{
						// There are files that should be removed
						// The current filename is in the list of files that need to be removed
						if( isset($data['remove_'.$this->filesKey]) && in_array($filename, $data['remove_'.$this->filesKey]))
						{
							$this->files()->detach($file->first()->id);
						}
					}

					// New files
					else
					{
						$file = new CalfFile;
						$file->createRecord(array('priority' => $priority, 'files' => $filename));

						$this->files()->save($file);
					}
				}
			}
		}

        if($this->hasNewLinkedAssets($data))
        {
        	foreach($data[$this->assetsKey] as $priority => $assetId)
        	{
				$this->files()->attach($assetId, array('priority' => $assetPriority + $priority));
        	}
        }

        // Handle revisions
        if(self::revisionsAllowed())
        {
            $this->createNewRevision($this->contentType, isset($data['current_revision_notes']) ? $data['current_revision_notes'] : '');
        }

        // Force reload of relationships now
        $this->touchRelations();

		return $this;
    }

    /**************************************************
     * Filter scopes
    **************************************************/
	/**
	 * Scope filter by parent ID
	 *
	 * @param int $id
	 * @return object
	 */
	public function scopeFilterByParentId($query, $id)
	{
		return $query->where('parent_id', '=', $id);
	}

	/**
	 * Scope filter by layout
	 *
	 * @param string $layout
	 * @return object
	 */
	public function scopeFilterByLayout($query, $layout)
	{
		return $query->where('layout', '=', $layout);
	}

	/**
	 * Scope filter by slug
	 *
	 * @param string $slug
	 * @return object
	 */
	public function scopeFilterBySlug($query, $slug)
	{
		return $query->where('slug', '=', $slug);
	}

    /**************************************************
     * Create/Edit scopes
     **************************************************/
	/**
	 * Scope set parent ID
	 *
	 * @param string $id
	 * @return object
	 */
	public function scopeSetParentId($query, $id)
	{
		$this->parent_id = $id;

		return $this;
	}

	/**
	 * Scope set type
	 *
	 * @param string $type
	 * @return object
	 */
	public function scopeSetType($query, $type)
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * Scope set layout
	 *
	 * @param string $layout
	 * @return object
	 */
	public function scopeSetLayout($query, $layout)
	{
		$this->layout = $layout;

		return $this;
	}

	/**
	 * Scope set slug
	 *
	 * @param string slug
	 * @return object
	 */
	public function scopeSetSlug($query, $slug, $fallback='')
	{
        $slug = Str::slug($slug ? $slug : $fallback);

        $current = DB::table($this->table)
                    ->select('id', 'slug')
                    ->where('slug', $slug)
                    ->first();

        if(
            $current !== null &&
            $current->id !== $this->id
        )
        {
            $slug .= date('-Y-m-d');
        }

        $this->slug = $slug;

		return $this;
	}

    /**************************************************
     * Relationships
     **************************************************/

    /**
     * Parent
     */
    
    public function parent()
    {
        return $this->belongsTo(get_class(), 'parent_id', 'id');
    }


    /**
     * Files
     */

	public function files()
	{
		return $this->belongsToMany('CalfFile', CalfContentFile::tableName(), 'content_id', 'file_id');
	}


	/**
	 * Content has many ContentFiles on content id
	 */

	public function contentFiles()
	{
		return $this->hasMany('CalfContentFile', 'content_id');
	}





    /**************************************************************************
     * Utilities
     *************************************************************************/

    /**
     * Determines if the current model allows revisions to be stored
     */

    static public function revisionsAllowed($permission=NULL)
    {
        return parent::revisionsAllowed(with(new static)->contentType);
    }


    private function hasUploadedFiles($data)
    {
        return  isset($data[$this->filesKey]) &&
                $data[$this->filesKey] &&
                count($data[$this->filesKey]);
    }


    private function hasNewLinkedAssets($data)
    {
        return  isset($data[$this->assetsKey]) &&
                $data[$this->assetsKey] &&
                count($data[$this->assetsKey]);
    }
}
