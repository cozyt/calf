<?php

class CalfStats
{
	private $models;
	private $user;

	/**
	 * Returns the total number of records for the defined models
	 *
	 * @return int
	 */
	public function totalRecords()
	{
		$count = 0;

		foreach($this->models as $model)
		{
			$count += $model->count();
		}

		return $count;
	}

	/**
	 * Returns the number of records with a given status type for the defined models
	 *
	 * @param string $status
	 * @return int
	 */
	public function byStatus($status='')
	{
		$count = 0;

		foreach($this->models as $model)
		{
			switch($status)
			{
				case 'published' :
					$model = $model->filterByPublished();
					break;

				case 'queued' :
					$model = $model->filterByQueued();
					break;

				case 'unpublished' :
					$model = $model->filterByUnpublished();
					break;

				case 'draft' :
					$model = $model->filterByDraft();
					break;
			}

			$count += $model->count();
		}

		return $count;
	}

	/**
	 * Record activity for last $days
	 *
	 * @param string $status
	 * @param int $days
	 * @param string $format
	 * @return array
	 */
	public function byActivity($status, $days=30, $format='M jS')
	{
		$activity = array();

        for($i = $days; $i > 0; $i--)
        {
            $date = date('Y-m-d', strtotime('-'.$i.' days'));
            $d = date($format, strtotime('-'.$i.' days'));

            foreach($this->models as $model)
            {
				switch($status)
				{
					case 'created' :
						$model = $model->filterByCreatedAt($date, 'on');
						break;

					case 'updated' :
						$model = $model->filterByUpdatedAt($date, 'on');
						break;

					case 'published' :
						$model = $model->filterByPublishedAt($date, 'on');
						break;
				}

            	$count = $model->count();

            	if(isset($activity[$d]))
            	{
            		$activity[$d] += $count;
            	}
            	else
            	{
            		$activity[$d] = $count;
            	}
            }
        }

        return $activity;
	}

	/**
	 * Define models to use on stats collection
	 *
	 * @param string $model
	 * @todo document method
	 */
	public function filterByModel($model)
	{
		if(is_array($model))
		{
	        foreach($model as $m)
	        {
	            $this->filterByModel($m);
	        }
		}
		else
		{
			$this->models[$model] = new $model;
		}

		return $this;
	}


	/**
	 * filterByUser
	 * @param int $userId
	 * @return object
	 */
	// public function filterByUser($userId)
	// {
	// 	$this->user[] = $userId;

	// 	return $this;
	// }
}