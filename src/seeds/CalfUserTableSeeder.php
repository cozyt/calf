<?php

class CalfUserTableSeeder extends Seeder
{
	public function run()
	{
		$rows = array(
			array(
				'type' => 'contributor', 
				'username' => 'contributor@domain.com',
				'password' => Hash::make('password'),
				'firstname' => 'Contributor',
				'surname' => 'Account',
				'tags' => 'contributor, user, admin',
				'meta' => 'Default contributor account for managing content on the site.',
			),

			array(
				'type' => 'editor', 
				'username' => 'editor@domain.com',
				'password' => Hash::make('password'),
				'firstname' => 'Editor',
				'surname' => 'Account',
				'tags' => 'editor, user, admin',
				'meta' => 'Default editor account for managing content on the site.',
			),

			array(
				'type' => 'administrator', 
				'username' => 'administrator@domain.com',
				'password' => Hash::make('password'),
				'firstname' => 'Administrator',
				'surname' => 'Account',
				'tags' => 'administrator, user, admin',
				'meta' => 'Default administrator account for managing content on the site.',
			),

			array(
				'type' => 'developer', 
				'username' => 'developer@domain.com',
				'password' => Hash::make('password'),
				'firstname' => 'Developer',
				'surname' => 'Account',
				'tags' => 'developer, user, admin',
				'meta' => 'Default developer account for managing content on the site.',
			),
		);
		
		foreach($rows as $row)
		{
			if(DB::table('calf_user')->where('username', $row['username'])->count() === 0)
			{
				DB::table('calf_user')->insert($row);
			}
		}
	}

}
