<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalfUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (! Schema::hasTable('calf_user'))
		{
			Schema::create('calf_user', function($table) {
	      		$table->increments('id'); 
		  		$table->string('type', 16)->default('general'); 
		  		$table->string('username', 128)->unique(); 
		  		$table->string('password', 128); 
		  		$table->string('firstname', 128); 
		  		$table->string('surname', 128); 
		  		$table->string('remember_token', 100)->nullable();
		  		$table->text('tags'); 
		  		$table->text('meta'); 
		  		$table->integer('created_by')->default(0); 
		  		$table->integer('updated_by')->default(0); 
		  		$table->integer('published_by')->default(0); 
		  		$table->timestamps();
		  		$table->dateTime('published_at')->default('0000-00-00 00:00:00'); 
		  		$table->integer('priority')->default(0); 
		  		$table->integer('revision')->default(0); 
		  		$table->tinyInteger('is_published')->default(1); 
		  		$table->tinyInteger('is_deleted')->default(0); 
			});			
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calf_user');
	}

}
