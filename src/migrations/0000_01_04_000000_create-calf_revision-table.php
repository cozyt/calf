<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalfRevisionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (! Schema::hasTable('calf_revision'))
		{
			Schema::create('calf_revision', function(Blueprint $table)
			{
	      		$table->increments('id');
	      		$table->integer('record_id');
	      		$table->text('permission');
		  		$table->text('data');
		  		$table->text('notes');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calf_revision');
	}

}
