<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalfContentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (! Schema::hasTable('calf_content'))
		{
			Schema::create('calf_content', function($table) {
	      		$table->increments('id'); 
	      		$table->integer('parent_id')->unsigned()->foreign('parent_id')->references('id')->on('calf_content'); 
	      		$table->string('type', 16);
	      		$table->string('layout', 32);
	      		$table->string('slug', 128)->unique();
	      		$table->string('title', 256);
	      		$table->string('short_title', 128);
		  		$table->text('lead');
		  		$table->text('body');
		  		$table->text('tags');
		  		$table->text('meta');
		  		$table->timestamps();
		  		$table->dateTime('published_at')->default('0000-00-00 00:00:00'); 
		  		$table->integer('created_by')->unsigned()->foreign('created_by')->references('id')->on('calf_user');
		  		$table->integer('updated_by')->unsigned()->foreign('updated_by')->references('id')->on('calf_user');
		  		$table->integer('published_by')->unsigned()->foreign('published_by')->references('id')->on('calf_user');
		  		$table->integer('priority')->default(0);
		  		$table->integer('revision')->default(0);
		  		$table->tinyInteger('is_published')->default(1);
		  		$table->tinyInteger('is_deleted')->default(0);
			});
		}

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calf_content');
	}

}
