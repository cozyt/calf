<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCalfUserRenameColumnUsername extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('calf_user') && Schema::hasColumn('calf_user', 'email'))
		{
			Schema::table('calf_user', function($table) {
			    $table->renameColumn('username', 'email');
			});		
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('calf_user') && Schema::hasColumn('calf_user', 'username'))
		{
			Schema::table('calf_user', function($table) {
			    $table->renameColumn('email', 'username');
			});		
		}
	}

}
