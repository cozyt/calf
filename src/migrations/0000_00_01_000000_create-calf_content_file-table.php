<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalfContentFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (! Schema::hasTable('calf_content_file'))
		{
			Schema::create('calf_content_file', function($table){
				$table->increments('id');
				$table->integer('content_id')->unsigned()->foreign('content_id')->references('id')->on('calf_content');
				$table->integer('file_id')->unsigned()->foreign('file_id')->references('id')->on('calf_file'); 
		  		$table->integer('priority')->default(0); 
			});		
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calf_content_file');
	}

}
