<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalfFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (! Schema::hasTable('calf_file'))
		{
			Schema::create('calf_file', function($table) {
	      		$table->increments('id'); 
		  		$table->string('filename', 64); 
			    $table->string('type', 16);
			    $table->integer('width')->default(0);
			    $table->integer('height')->default(0);
			    $table->integer('bits')->default(0);
			    $table->integer('channels')->default(0);
		  		$table->string('extension', 4); 
		  		$table->string('mime_type', 16); 
		  		$table->integer('size');
		  		$table->string('title', 128);
		  		$table->string('alt', 128);
		  		$table->text('caption');
		  		$table->text('tags'); 
		  		$table->text('meta'); 
		  		$table->integer('created_by')->default(0); 
		  		$table->integer('updated_by')->default(0); 
		  		$table->integer('published_by')->default(0); 
		  		$table->timestamps();
		  		$table->dateTime('published_at')->default('0000-00-00 00:00:00'); 
		  		$table->integer('priority')->default(0); 
		  		$table->integer('revision')->default(0); 
		  		$table->tinyInteger('is_published')->default(1); 
		  		$table->tinyInteger('is_deleted')->default(0); 
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calf_file');
	}

}
