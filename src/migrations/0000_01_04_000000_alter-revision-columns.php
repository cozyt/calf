<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRevisionColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calf_content', function($table) {
		    $table->integer('revision_id');
		    $table->dropColumn('revision');
		});

		Schema::table('calf_file', function($table) {
		    $table->integer('revision_id');
		    $table->dropColumn('revision');
		});

		Schema::table('calf_user', function($table) {
		    $table->integer('revision_id');
		    $table->dropColumn('revision');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calf_content', function($table) {
		    $table->integer('revision');
		    $table->dropColumn('revision_id');
		});

		Schema::table('calf_file', function($table) {
		    $table->integer('revision');
		    $table->dropColumn('revision_id');
		});

		Schema::table('calf_user', function($table) {
		    $table->integer('revision');
		    $table->dropColumn('revision_id');
		});
	}

}
