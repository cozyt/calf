<?php 
class CalfAuthLevelFilter 
{
    public function filter() 
    {
        $permission_type_slug = sprintf('%s', Request::segment(2));
        $permission_slug = sprintf('%s', Request::segment(3));

        $permission_type = CalfPermissionType::find($permission_type_slug);
        $permission = CalfPermission::find($permission_slug);

        if(
            (isset($permission_type->auth_type) && CalfHelper::authUserTypeIsAuthorized($permission_type->auth_type) === false) ||
            (isset($permission->auth_type) && CalfHelper::authUserTypeIsAuthorized($permission->auth_type) === false) 
        )
        {
            return Redirect::route('calf.unauthorized');
        }

    }

}