<?php 
class CalfAuthFilter 
{
    public function filter() 
    {
        if(Auth::check())
        {
            if(Route::currentRouteNamed('calf.root'))
            {
                return Redirect::route('calf.dashboard');
            }
        }
        elseif(! in_array(Route::currentRouteName(), Config::get('calf::unprotected_routes')))
        {
            return Redirect::route('calf.login');
        }
    }

}