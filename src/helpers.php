<?php

/******************************************************************************
 * RECORD HANDLERS
 ******************************************************************************/

function published_status($published, $publishDate)
{
    if($published)
    {
        $date = new DateTime($publishDate);

        if($date->format('YmdHis') <= date('YmdHis'))
        {
            return 'published';
        }
        else
        {
            return 'queued';
        }
    }
    else
    {
        if($publishDate == '' || $publishDate == '0000-00-00 00:00:00')
        {
            return 'draft';
        }
        else
        {
            return 'unpublished';
        }
    }
}



/******************************************************************************
 * DEBUG UTILITIES
 ******************************************************************************/

/**
 * [console_dump description]
 * @param  [type] $data [description]
 * @param  string $type [description]
 * @return [type]       [description]
 */

function console_dump($data=NULL, $type='log')
{
    $data = json_encode($data);

    print "<script>console.".$type."('".$data."');</script>";
}


/**
 * [printr description]
 * @param  [type] $data [description]
 * @return [type]       [description]
 */

function printr($data)
{
    print "<pre>";
    print_r($data);
    print "</pre>";
}


/**
 * Returns array of registered content models
 *
 * @return array
 */

function registered_models()
{
    $permissions = CalfPermission::get();
    $models = array('CalfContent');

    foreach($permissions as $p)
    {
        if(
            isset($p->model) &&
            ! in_array($p->model, $models) &&
            $p->model != 'CalfContent' &&
            is_subclass_of($p->model, 'CalfContent') === false
        )
        {
            $models[] = $p->model;
        }
    }

    return $models;
}





/******************************************************************************
 * BOOTSTRAP HELPERS
 ******************************************************************************/


/**
 * [glyphicon description]
 * @param  [type] $icon [description]
 * @return [type]       [description]
 */

function glyphicon($icon, $iconNameOnly=false, $class='')
{
    switch($icon)
    {
        case 'action':
        case 'setting':
        case 'settings':
        case 'system':
            $icon = 'cog';
            break;

        case 'back':
        case 'previous':
            $icon = 'chevron-left';
            break;

        case 'forward':
        case 'next':
            $icon = 'chevron-right';
            break;

        case 'batch' :
            $icon = 'flag';
            break;

        case 'cancel' :
        case 'remove' :
            $icon = 'remove';
            break;

        case 'create' :
        case 'plus' :
            $icon = 'plus';
            break;

        case 'delete' :
        case 'trash' :
            $icon = 'trash';
            break;

        case 'download' :
            $icon = 'download';
            break;

        case 'edit' :
        case 'pencil' :
            $icon = 'pencil';
            break;

        case 'export' :
            $icon = 'save';
            break;

        case 'file' :
        case 'draft' :
            $icon = 'file';
            break;

        case 'move' :
        case 'arrange' :
            $icon = 'move';
            break;

        case 'publish' :
        case 'published' :
        case 'success' :
        case 'ok' :
            $icon = 'ok';
            break;

        case 'save':
            $icon = 'floppy-disk';
            break;

        case 'unpublish' :
        case 'unpublished' :
        case 'error' :
        case 'danger' :
            $icon = 'ban-circle';
            break;

        case 'upload' :
            $icon = 'upload';
            break;

        case 'queued' :
        case 'queue' :
            $icon = 'time';
            break;

        case 'warning' :
            $icon = 'warning-sign';
            break;

        case 'refresh' :
        case 'replace' :
        case 'rollback' :
        case 'revision' :
        case 'revisions' :
            $icon = 'refresh';
            break;

        case 'view' :
        case 'search' : 
            $icon = 'search';
            break;

        case 'image' :
        case 'picture' :
            $icon = 'picture';
            break;

        case 'video' :
        case 'film' :
            $icon = 'film';
            break;

        case 'audio' :
        case 'music' :
            $icon = 'music';
            break;

        case 'file' :
            $icon = 'file';
            break;

        case 'link'  :
        case 'links' :
        case 'url'   :
            $icon = 'link';
            break;

        case 'new-window' :
            $icon = 'new-window';
            break;

        case 'modal' :
            $icon = 'modal';
            break;

        default :
            $icon = '';
            break;
    }

    if($iconNameOnly === true)
    {
        return $icon;
    }

    return '<i class="glyphicon glyphicon-' . $icon . $class . '"></i>';
}


/**
 * published_status_color
 * @param  [type] $status [description]
 * @return [type]         [description]
 */

function published_status_color($status)
{
    switch($status)
    {
        case 'success' :
        case 'publish' :
        case 'published' :
            return 'success';

        case 'danger' :
        case 'unpublish' :
        case 'unpublished' :
            return 'danger';

        case 'warning' :
        case 'queue' :
        case 'queued' :
            return 'warning';

        case 'default' :
        case 'draft' :
        default;
            return 'default';
    }
}


function published_status_icon($status)
{
    return glyphicon($status);
}


/**
 * Renders the file as an asset with the appropriate HTML tags for the
 * asset type.
 */

function asset_html($filename, $attr='')
{
    return CalfFile::asset($filename, $attr='');
}


/**
 * Returns allowed raw types
 */

function raw_asset_types()
{
    return array_keys(Config::get('calf::allowed_extensions'));
}


/**
 * Returns the base file type icon 
 */

function asset_type_icon($type='file')
{
    return CalfFile::typeIcon($type);
}
