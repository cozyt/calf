<?php

abstract class CalfBaseController extends Controller
{
	public function __construct()
	{
		$this->data = array();

		$this->data['calf'] = Config::get('calf::call_me');
		$this->data['body_class'] = 'calf admin';
	    $this->data['permission_types'] = CalfPermissionType::get();
	    $this->data['permission_type_slug'] = sprintf('%s', Request::segment(2));
	}

	/**
	 * Handling missing methods
	 * A catch-all method may be defined which will be called when no other matching method
	 * is found on a given controller. The method should be named missingMethod, and receives
	 * the method and parameter array for the request:
	 */
	public function missingMethod($parameters = array())
	{
		echo 'Missing method';

		var_dump($parameters);

	    return;
	}
}