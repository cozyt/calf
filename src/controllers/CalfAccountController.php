<?php

class CalfAccountController extends CalfBaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->data['body_class'] .= ' account';
    }

    /**
     * Index
     *
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['body_class'] .= ' index';

        return;
    }

    /**
     * Account management
     *
     * @return string
     */
    public function account()
    {
        $this->data['body_class'] .= ' edit';
        $this->data['action'] = 'account';

        $permission = CalfPermission::find('user');
        $model = isset($permission->model) ? $permission->model : 'CalfUser';
        $view = isset($permission->views['account']) ? $permission->views['account'] : 'calf::layout.edit';

        $id = Auth::user()->id;
        $row = $model::find($id);

        $this->data['permission'] = $permission;
        $this->data['page_header'] = '';

        if(Input::has('form-submit'))
        {
            $validator = Validator::make(Input::all(), $permission->validation_rules['account']);

            if (! $validator->fails())
            {
                $row->editRecord(Input::all());

                if($row->id)
                {
                    $this->data['messages'] = View::make('calf::regions.messages.general', array(
                        'type' => 'success',
                        'title' => '<span class="glyphicon glyphicon-ok-sign"></span> Account details updated!'
                    ));
                }
                else
                {
                    $this->data['messages'] = View::make('calf::regions.messages.general', array(
                        'type' => 'danger',
                        'title' => '<span class="glyphicon glyphicon-remove-circle"></span> An error has occured',
                        'messages' => array('Could not update your account details at this time')
                    ));

                    $row = (object) Input::except('form-submit');
                }

            }
            else
            {
                $this->data['messages'] = View::make('calf::regions.messages.general', array(
                    'type' => 'danger',
                    'title' => '<span class="glyphicon glyphicon-remove-circle"></span> An error has occured',
                    'messages' => $validator->messages()->all(':message')
                ));

                $row = (object) Input::except('form-submit');
            }
        }

        $this->data['row'] = $row;

        return View::make($view, $this->data);
    }

    /**
     * Login
     *
     * @return string
     */
    public function login()
    {
        $this->data['body_class'] .= ' login';

        if(Input::has('form-submit'))
        {
            $rules = array(
                'username' => 'required|email|exists:calf_user',
                'password' => 'required|alphaNum|min:3'
            );

            $validator = Validator::make(Input::all(), $rules);

            if (! $validator->fails())
            {
                $userdata = array(
                    'username'  => Input::get('username'),
                    'password'  => Input::get('password'),
                    'is_published' => 1,
                    'is_deleted' => 0,
                );

                if (Auth::attempt($userdata))
                {
                    $redirect = Config::get('calf::redirect_on_login');

                    if(is_array($redirect))
                    {
                        return Redirect::route($redirect[0], $redirect[1]);
                    }

                    return Redirect::route($redirect);
                }
                else
                {
                    $this->data['messages'] = View::make('calf::regions.messages.general', array(
                        'type' => 'danger',
                        'title' => '<span class="glyphicon glyphicon-remove-circle"></span> Could not find a user matching those credentials.',
                        'messages' => array('Please check your email/password and try again.'),
                    ));
                }
            }
            else
            {
                $this->data['messages'] = View::make('calf::regions.messages.general', array(
                    'type' => 'danger',
                    'title' => '<span class="glyphicon glyphicon-remove-circle"></span> An error has occured',
                    'messages' => $validator->messages()->all(':message')
                ));
            }
        }

        $this->data['allow_reset'] = Config::get('mail.from.address') ? true : false;

        return View::make('calf::pages.account.login', $this->data);
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::route('calf.login');
    }

    /**
     * User credential reset
     *
     * @return void
     */
    public function reset()
    {
        $this->data['body_class'] .= ' reset stage-1';

        if(Input::has('form-submit'))
        {
            $this->data['email'] = Input::get('email');

            $response = Password::remind(Input::only('email'));

            switch ($response)
            {
                case Password::INVALID_USER :
                    $this->data['messages'] = View::make('calf::regions.messages.general', array(
                        'type' => 'danger',
                        'title' => '<span class="glyphicon glyphicon-remove-circle"></span> An error has occured',
                        'messages' => array(Lang::get($response)),
                    ));
                    break;

                case Password::REMINDER_SENT :
                    return View::make('calf::pages.account.reset-sent', $this->data);
            }
        }

        return View::make('calf::pages.account.reset-stage-1', $this->data);
    }

    /**
     * User reset token
     */
    public function reset_token($token = NULL)
    {
        if($token !== NULL)
        {
            $this->data['token'] = $token;

            if(Input::has('form-submit'))
            {
                $credentials = Input::only(
                    'email', 'password', 'password_confirmation', 'token'
                );

                $response = Password::reset($credentials, function($user, $password)
                {
                    $user->password = Hash::make($password);

                    $user->save();
                });

                switch ($response)
                {
                    case Password::INVALID_PASSWORD:
                    case Password::INVALID_TOKEN:
                    case Password::INVALID_USER:
                        $this->data['messages'] = View::make('calf::regions.messages.general', array(
                            'type' => 'danger',
                            'title' => '<span class="glyphicon glyphicon-remove-circle"></span> An error has occured',
                            'messages' => array(
                                Lang::get($response),
                                'If you can think of any other reason as to why this may have happened, then please contact an administrator to get this sorted as soon as possible, so that other users do not experience the same&nbsp;problem.',
                                'Alternatively, please feel free to <a href="'.URL::route('calf.reset').'">start the entire reset process from scratch</a>.',
                            ),
                        ));
                        break;

                    case Password::PASSWORD_RESET:
                        return View::make('calf::pages.account.reset-success', $this->data);
                }
            }

            return View::make('calf::pages.account.reset-stage-2', $this->data);
        }
    }

}

