<?php

class CalfRecordController extends CalfBaseController
{
    public function __construct()
    {
        parent::__construct();

        // Permission types
        $permission_type_slug = $this->data['permission_type_slug'];

        if(isset($this->data['permission_types']->$permission_type_slug))
        {
            $this->data['permission_type'] = CalfPermissionType::find($permission_type_slug);

            // Primary permissions
            $this->data['permissions'] = CalfPermission::get();

            $this->data['body_class'] .= ' '.$this->data['permission_type']->id;

            // Secondary permissions
            $this->data['permissions_by_type'] = CalfPermission::filterByType($this->data['permissions'], $this->data['permission_type']->id);
            $permission_slug = sprintf('%s', Request::segment(3));

            if(isset($this->data['permissions_by_type']->$permission_slug))
            {
                $this->data['permission'] = CalfPermission::find($permission_slug);

                $this->data['body_class'] .= ' '.$this->data['permission']->id;
                $this->data['page_header'] = $this->data['permission']->title;

                $active = true;
            }

            $this->data['primary_nav'] = View::make('calf::regions.nav.primary', array(
                'active' => $this->data['permission_type']->id,
                'items' => $this->data['permission_types']
            ));

            $this->data['secondary_nav'] = View::make('calf::regions.nav.secondary', array(
                'active' => isset($active) ? $this->data['permission']->id : '',
                'parent' => $this->data['permission_type']->id,
                'items' => $this->data['permissions_by_type']
            ));
        }
    }

    /**
     * Type landing page
     */
    public function index()
    {
        $this->data['body_class'] .= ' index';

        if(count($this->data['permissions_by_type']))
        {
            $this->data['create_btn'] = View::make('calf::regions.btn.create-permissions-record-dropdown', array(
                'type' => $this->data['permission_type']->id,
                'permissions' => $this->data['permissions_by_type']
            ));
        }

        return View::make($this->data['permission_type']->view, $this->data);
    }


    /**
     * Record listing
     */
    public function listing($type, $permission)
    {
        $this->data['body_class'] .= ' listing';

        $permission = $this->data['permission'];
        $model = $this->model();
        $rows = new $model;
        $sortBy = CalfSort::by(isset($permission->sort_order) ? $permission->sort_order[0] : 'created_at');
        $sortDir = CalfSort::dir(isset($permission->sort_order) ? $permission->sort_order[1] : 'desc');

        $rows = $this->filter($rows);

        // Handle filtering fields
        if(isset($permission->filter_fields))
        {
            $rows = $rows->filter(Input::get(), $permission->filter_fields);
        }

        // Handle pagination config
        if(isset($permission->paginate))
        {
            $rows = $rows->paginate(Config::get('calf::pagination_count'));
        }
        else
        {
            $rows = $rows->get();
        }

        // Handle record count
        if($rows->count())
        {
            $this->data['records_list'] = View::make('calf::regions.records.list', array(
                'headers' => $permission->table_headers,
                'rows' => $rows,
                'sort_by' => $sortBy,
                'sort_dir' => $sortDir,
                'sort_args' => array(
                    'type' => $permission->type,
                    'permission' => $permission->id
                ),
                'options' => $permission->table_options ? $permission->table_options : array(),
                'actions' => isset($permission->actions) ? $permission->actions : array(),
                // 'type' => $permission->type,
                'permission' => $permission,
            ));

            if(isset($permission->paginate))
            {
                $this->data['pagination'] = $rows->appends(Input::except('page'))->links();
            }

            // Handle batch actions form
            if(isset($permission->actions))
            {
                $this->data['records_list'] = View::make('calf::regions.forms.listing', array(
                    'records_list' => $this->data['records_list'],
                    'actions' => $permission->actions,
                    // 'type' => $permission->type,
                    'permission' => $permission,
                ));
            }
        }

        // Handle filtering config
        if(isset($permission->filter_fields) && (Input::get('form-submit') || $rows->count()))
        {
            $this->data['forms_filter'] = View::make('calf::regions.forms.filter', array(
                'title' => strtolower($permission->title),
                'with_advanced' => isset($permission->advanced_filter),
                'data' => Input::get()
            ));
        }

        // Handle record count config
        if(isset($permission->show_record_count))
        {
            $this->data['records_count'] = View::make('calf::regions.records.count', array(
                'title' => $permission->title,
                'total' => $rows->getTotal(),
                'count' => $rows->count()
            ));
        }

        return View::make('calf::layout.records-listing', $this->data);
    }


    /**
     * Record create
     */
    public function create($type, $permission)
    {
        $this->data['body_class'] .= ' create';
        $this->data['action'] = 'create';

        $permission = $this->data['permission'];
        $model = $this->model();
        $row = array();
        $view = isset($permission->views['create']) ? $permission->views['create'] : 'calf::layout.edit';

        if(Input::has('form-submit'))
        {
            $input = Input::except('form-submit');
            $validator = Validator::make(Input::all(), $permission->validation_rules['create']);

            if (! $validator->fails())
            {
                $row = new $model;

                $row->createRecord($input);

                if($row->id)
                {
                    CalfNotification::recordCreated(array('permission' => $permission, 'record' => $row));

                    return Redirect::route('calf.edit', array(
                        'type' => $permission->type,
                        'permission' => $permission->id,
                        'id' => $row->id,
                        'new' => 1
                    ));
                }
                else
                {
                    $this->data['messages'] = View::make('calf::regions.messages.general', array(
                        'type' => 'danger',
                        'title' => glyphicon('error') . ' An error has occured',
                        'messages' => array('Could not '.(isset($permission->buttons['create']) ? strtolower($permission->buttons['create']) : 'create new '.$permission->id).' at this time')
                    ));
                }
            }
            else
            {
                $this->data['messages'] = View::make('calf::regions.messages.general', array(
                    'type' => 'danger',
                    'title' => glyphicon('error') . ' An error has occured',
                    'messages' => $validator->messages()->all(':message')
                ));
            }

            $row = $this->reassignRowInput($row, $input);
        }

        $this->data['row'] = $row;

        return View::make($view, $this->data);
    }


    /**
     * Record edit
     */

    public function edit($type, $permission, $id, $rId=NULL)
    {
        $this->data['body_class'] .= ' edit';
        $this->data['action'] = 'edit';

        $permission = $this->data['permission'];
        $model = $this->model();
        $view = isset($permission->views['edit']) ? $permission->views['edit'] : 'calf::layout.edit';
        $revisionsAllowed = $this->revisionsAllowed();

        // Load the current record
        $row = new $model;
        $row = $row->find($id)->touchRelations();

        if($row === NULL)
        {
            return Redirect::route('calf.error.record');
        }

        // Load the current revision data
        if($revisionsAllowed === true)
        {
            $revision = new CalfRevision;
            $currentRevision = $row->currentRevision;
        }


        // Handle form submission
        if(Input::has('form-submit'))
        {
            $input = Input::except('form-submit');
            $validator = Validator::make($input, $permission->validation_rules['edit']);

            if (! $validator->fails())
            {
                if($row->id)
                {
                    // Store the update
                    // If the user is a contributor, amends are now only stored in the revisions
                    // and so the data is only prepared by eloquent
                    $row = $row->editRecord($input, $revisionsAllowed && CalfHelper::authUserTypeIsAuthorized('contributor', '='));

                    // Renew the current revision data to match the update
                    // If this is a contributor user, you can expect to still
                    // see the previous data in this property's value
                    if($revisionsAllowed === true)
                    {
                        $currentRevision = $row->currentRevision;
                        $selectedRevision = $revision->getLatestRevisionForRecord($id); // $this->revisions($id, 1)->first();
                    }

                    $this->data['messages'][] = View::make('calf::regions.messages.general', array(
                        'type' => 'success',
                        'title' => glyphicon('success') . ' ' . ucfirst($permission->id) . ' updated!'
                    ));

                    if($revisionsAllowed === true)
                    {
                        $this->selectedRevisionAlert($currentRevision, $selectedRevision);
                    }
                }
                else
                {
                    $this->data['messages'] = View::make('calf::regions.messages.general', array(
                        'type' => 'danger',
                        'title' => glyphicon('error') . ' An error has occured',
                        'messages' => array('Could not '.(isset($permission->buttons['edit']) ? strtolower($permission->buttons['edit']) : 'edit '.$permission->id).' at this time')
                    ));

                    $row = $this->reassignRowInput($row, $input);
                }
            }
            else
            {
                $this->data['messages'] = View::make('calf::regions.messages.general', array(
                    'type' => 'danger',
                    'title' => glyphicon('error') . ' An error has occured',
                    'messages' => $validator->messages()->all(':message')
                ));

                $row = $this->reassignRowInput($row, $input);
            }
        }

        // Handle new record flag
        elseif(Input::get('new') == 1)
        {
            $this->data['messages'] = View::make('calf::regions.messages.general', array(
                'type' => 'success',
                'title' => glyphicon('success') . ' New ' . $permission->id . ' created!'
            ));
        }


        // Load a specific revision (if required)
        elseif($revisionsAllowed === true && $rId !== NULL)
        {
            $selectedRevision = $revision->filterByRecordId($id)
                                         ->find($rId);

            if($selectedRevision === NULL)
            {
                return Redirect::route('calf.error.record');
            }

            $this->selectedRevisionAlert($currentRevision, $selectedRevision);

            $row = $selectedRevision->data;
        }


        // Load all revisions for this record and prep the data
        if($revisionsAllowed === true)
        {
            $this->prepareRevisions($revision->getRevisionsByRecord($id), $currentRevision, isset($selectedRevision) ? $selectedRevision : NULL);
        }


        $this->data['row'] = $row;

        return View::make($view, $this->data);
    }


    /**
     * Record delete
     */
    public function delete($type, $permission, $id)
    {
        $this->data['body_class'] .= ' delete';
        $this->data['action'] = 'delete';
        $this->data['is_deleted'] = false;

        $permission = $this->data['permission'];
        $model = $this->model();
        $row = new $model;
        $view = isset($permission->views['delete']) ? $permission->views['delete'] : 'calf::layout.delete';

        // Handle user type - Contributors can only edit their own records
        if(CalfHelper::authUserTypeIsAuthorized('contributor', '='))
        {
            $row = $row->filterByCreatedBy(CalfHelper::authUserId());
        }

        $row = $row->find($id);

        if($row === NULL)
        {
            return Redirect::route('calf.error.record');
        }

        if(Input::has('form-submit'))
        {
            $row->delete();

            $this->data['messages'] = View::make('calf::regions.messages.general', array(
                'type' => 'success',
                'title' => glyphicon('success') . ' ' . $permission->id . ' deleted!'
            ));

            $this->data['is_deleted'] = true;
        }

        $this->data['row'] = $row;

        return View::make($view, $this->data);
    }


    /**
     * Perform action on records
     * @param  string $type
     * @param  string $permission
     * @return response object
     */

    public function action($type, $permission)
    {
        $this->data['body_class'] .= ' action';
        $permission = $this->data['permission'];
        $model = $this->model();
        $sortBy = CalfSort::by(isset($permission->sort_order) ? $permission->sort_order[0] : 'created_at');
        $sortDir = CalfSort::dir(isset($permission->sort_order) ? $permission->sort_order[1] : 'desc');
        // $actionComplete = false;
        $ids = Input::get('ids');
        $action = Input::get('action');

        // Confirm that the user can perform batch actions
        if(
            ! isset($permission->actions) ||
            count($permission->actions) <= 0 ||
            count($ids) <= 0 ||
            $action === ''
        )
        {
            return Redirect::route('calf.error.record');
        }

        // Authorise the user
        if(
            Config::get('calf::actions.' . $action) === NULL ||
            CalfHelper::authUserTypeIsAuthorized(Config::get('calf::actions.' . $action . '.auth_type'), '>=') === false
        )
        {
            return Redirect::route('calf.unauthorized');
        }

        // Handle confirmation form submissions
        if(Input::has('form-submit') && $action !== 'export')
        {
            foreach($ids as $key => $id)
            {
                $record = $model::find($id);

                switch ($action)
                {
                    case 'publish':
                        $record = $record->setPublishedMeta(1, Input::get('publish_at'))->save();
                        break;

                    case 'unpublish':
                        $record = $record->setPublishedMeta(1)->save();
                        break;

                    case 'queue':
                        $record = $record->setPublishedMeta(1, Input::get('publish_at'))->save();
                        break;

                    case 'delete':
                        $record = $record->delete();
                        break;

                    case 'arrange':
                        $record = $record->setPriority($key)->save();
                        break;
                }

                // $record = $this->filter($record);

                // $record->save();
            }

            $this->data['messages'] = View::make('calf::regions.messages.general', array(
                'type' => 'success',
                'title' => glyphicon('success') . ' ' . ucfirst($action) . ' action for ' . Str::plural($permission->title) . ' completed successfully',
            ));

            // $actionComplete = true;
        }

        // Handle pre-confirmation
        // else
        // {
            $rows = new $model;

            $rows = $rows->where(function($query) {
                foreach(Input::get('ids') as $id)
                {
                    $query->orWhere('id', '=', $id);
                }
            });

            if($action === 'arrange')
            {
                $rows = $rows->orderBy('priority', 'ASC');
            }

            $rows = $this->filter($rows)
                         ->get();


            // Handle record count
            if($rows->count())
            {
                // Handle data exports
                if(Input::has('form-submit') && $action === 'export')
                {
                    $filename = $permission->id . '-' . date('YmdHis') . '.csv';
                    $filepath = storage_path('exports/' . $filename);
                    $exportData = $rows->toArray();

                    // Where the export path does not exist, create it
                    if(! File::exists(dirname($filepath)))
                    {
                        File::makeDirectory(dirname($filepath));
                    }

                    // Create the contents of the CSV file using the data
                    $file = fopen($filepath, 'w');

                    fputcsv($file, array_keys($exportData[0]));

                    foreach ($exportData as $row)
                    {
                        fputcsv($file, $row);
                    }

                    fclose($file);

                    // Return download response
                    return Response::download($filepath);
                }


                $this->data['records_list'] = View::make('calf::regions.forms.action', array(
                    'records_list' => View::make('calf::regions.records.list', array(
                        'headers' => $permission->table_headers,
                        'rows' => $rows,
                        'sort_by' => $sortBy,
                        'sort_dir' => $sortDir,
                        'sort_args' => array(
                            'type' => $permission->type,
                            'permission' => $permission->id
                        ),
                        // 'type' => $permission->type,
                        'permission' => $permission,
                        'sortable' => $action === 'arrange',
                        'with_id_field' => 'hidden',
                    )),
                    // 'action_complete' => $actionComplete,
                    'action' => $action,
                    'rows' => $rows,
                    // 'actions' => $permission->actions,
                    // 'type' => $permission->type,
                    'permission' => $permission,
                ));
            }

            // Handle record count config
            if(isset($permission->show_record_count))
            {
                $this->data['records_count'] = View::make('calf::regions.records.count', array(
                    'title' => $permission->title,
                    'total' => $rows->count(),
                    'count' => $rows->count(),
                ));
            }
        // }

        $this->data['page_header'] = ucfirst($action) . ' ' . strtolower($this->data['page_header']);

        return View::make('calf::layout.action', $this->data);
    }







    /**
     * Return permission model
     * @param  object $permission
     * @return string
     */

    private function model()
    {
        return isset($this->data['permission']->model) ? $this->data['permission']->model : 'CalfContent';
    }


    /**
     * Filter records
     * @param  object $permission
     * @param  object $rows
     * @return object
     */

    private function filter($rows)
    {
        $permission = $this->data['permission'];
        $model = $this->model();

        // Handle model config
        if($model == 'CalfContent' || is_subclass_of($model, 'CalfContent'))
        {
            $rows = $rows->filterByType($permission->id);
        }

        // Handle sorting config
        if(isset($permission->allow_sorting))
        {
            $sortBy = CalfSort::by(isset($permission->sort_order) ? $permission->sort_order[0] : 'created_at');
            $sortDir = CalfSort::dir(isset($permission->sort_order) ? $permission->sort_order[1] : 'desc');

            $rows = $rows->orderBy($sortBy, $sortDir);
        }

        // Handle user type
        if(CalfHelper::authUserTypeIsAuthorized('contributor', '='))
        {
            $rows = $rows->filterByCreatedBy(CalfHelper::authUserId());
        }

        return $rows;
    }


    /**
     * Row input reassignment
     * @param  object $row
     * @param object $input
     * @return object
     */

    private function reassignRowInput($row, $input)
    {
        $row = $input;

        return (object) $row;
    }





    /**************************************************************************
     * Revision handlers for this controller
     *************************************************************************/

    /**
     * Determine if the current permission allows revisions
     * @return boolean
     */

    private function revisionsAllowed()
    {
        $model = $this->model();
        return method_exists($model, 'revisionsAllowed') && $model::revisionsAllowed();
    }


    /**
     * Prepare revision data
     * @param  object $revisions
     * @param  object $currentRevision
     * @param  object|NULL $selectedRevision
     * @return [type] [description]
     */

    private function prepareRevisions($revisions, $currentRevision, $selectedRevision=NULL)
    {
        // if(count($revisions) > 1)
        // {
            foreach($revisions as $key => $revision)
            {
                if($currentRevision->id == $revision->id)
                {
                    $row_class = 'success';

                    $this->data['current_revision_notes'] = $revision->notes;
                }
                elseif($selectedRevision !== NULL && $selectedRevision->id == $revision->id)
                {
                    $row_class = 'warning';

                    $this->data['current_revision_notes'] = $revision->notes;
                }
                else
                {
                    $row_class = '';
                }

                $this->data['revisions'][] = (object) array(
                    'row_class' => $row_class,
                    'id' => $revision->record_id,
                    'revision' => $revision->id,
                    // 'revision_number' => $revision->revisionNumber($key),
                    'created_by' => $revision->createdBy()->fullname(true),
                    'created_at' => $revision->createdAt()->format('d-m-Y H:i:s'),
                    'published_at' => isset($revision->data->published_at) ? $revision->data->published_at : '',
                    'is_published' => $revision->data->is_published,
                );
            }
        // }

        return $this;
    }


    /**
     * Generates an alert to show the selected revision being viewed/edited
     * @param  object $currentRevision
     * @param  object $selectedRevision
     * @return object
     */

    private function selectedRevisionAlert($currentRevision, $selectedRevision)
    {
        if($currentRevision->id !== $selectedRevision->id)
        {
            $this->data['messages'][] = View::make('calf::regions.messages.general', array(
                'type' => 'warning',
                'title' => glyphicon('warning') . ' Editing revision #' . $selectedRevision->id,
                'messages' => array('Created' .
                    ' by ' . $selectedRevision->createdBy()->fullname() . '' .
                    ' on ' . $selectedRevision->createdAt()->format('d-m-Y') . '' .
                    ' at ' . $selectedRevision->createdAt()->format('H:i:s') . '' .
                    ''
                )
            ));
        }

        return $this;
    }
}
