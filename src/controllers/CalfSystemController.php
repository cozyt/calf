<?php

class CalfSystemController extends CalfBaseController
{

    /**
     * Unauthorized access page
     */
    public function unauthorized()
    {
        $this->data['body_class'] .= ' error unauthorized';

        return View::make('calf::pages.system.errors.unauthorized', $this->data);
    }

    /**
     * Page not found
     */
    public function error_page_not_found()
    {
        $this->data['body_class'] .= ' error page-not-found';

        return View::make('calf::pages.system.errors.page', $this->data);
    }

    /**
     * Record not found
     */
    public function error_record_not_found()
    {
        $this->data['body_class'] .= ' error record-not-found';

        return View::make('calf::pages.system.errors.record', $this->data);
    }

    /**
     * Dashboard
     */
    public function dashboard()
    {
        $userId = CalfHelper::authUserId();
        $permissions = CalfPermission::get();
        $contentPermissions = CalfPermission::filterByType($permissions, 'content');
        $models = registered_models();

        $stats = new CalfStats;
        $stats = $stats->filterByModel($models);

        $this->data['permission_type'] = CalfPermissionType::find($this->data['permission_type_slug']);
        $this->data['permissions_by_type'] = CalfPermission::filterByType($permissions, $this->data['permission_type']->id);

        $this->data['total_records'] = $stats->totalRecords();

        $this->data['content_review']['all'] = array(
            'status' => array(
                'published' => $stats->byStatus('published'),
                'queued' => $stats->byStatus('queued'),
                'unpublished' => $stats->byStatus('unpublished'),
                'draft' => $stats->byStatus('draft'),
            ),
            'count' => $this->data['total_records'],
            // 'records' => $content->orderBy('created_at', 'desc')->limit(6)->get()
        );

        foreach($stats->byActivity('created', 30, 'm/d') as $date => $count)
        {
            $this->data['content_review']['all']['activity'][$date]['created'] = $count;
        }

        foreach($stats->byActivity('updated', 30, 'm/d') as $date => $count)
        {
            $this->data['content_review']['all']['activity'][$date]['updated'] = $count;
        }

        foreach($stats->byActivity('published', 30, 'm/d') as $date => $count)
        {
            $this->data['content_review']['all']['activity'][$date]['published'] = $count;
        }

        $tag = new CalfTag($models, 'tags');
        $this->data['tags'] = array(
            'count' => $tag->load()->count(),
            'distinct' => $tag->load()->filterByDistinct()->count(),
            'popular' => array_slice($tag->load()->tagCount(), 0, 5),
        );

        if(count($contentPermissions))
        {
            $this->data['create_btn'] = View::make('calf::regions.btn.create-permissions-record-dropdown', array(
                'type' => 'content',
                'permissions' => $contentPermissions
            ));
        }

        return View::make($this->data['permission_type']->view, $this->data);
    }

    /**
     * User notifications
     */
    public function notifications()
    {

    }

    /**
     * About Calf
     */
    public function about()
    {
        $permissions = CalfPermission::get();
        $permission_slug = sprintf('%s', Request::segment(3));
        $filepath = __DIR__ . '/../../readme.md';
        $filecontent = File::get($filepath);
        $parsedown = new Parsedown();

        $this->data['permission_type'] = CalfPermissionType::find($this->data['permission_type_slug']);
        $this->data['permissions_by_type'] = CalfPermission::filterByType($permissions, $this->data['permission_type']->id);
        $this->data['permission'] = CalfPermission::find($permission_slug);

        $this->data['secondary_nav'] = View::make('calf::regions.nav.secondary', array(
            'active' => $this->data['permission']->id,
            'parent' => $this->data['permission_type']->id,
            'items' => $this->data['permissions_by_type']
        ));

        $this->data['readme'] = $parsedown->text($filecontent);

        if(preg_match_all("/<h2>(.+)<\/h2>/", $this->data['readme'], $headers))
        {
           $this->data['readme_headers'] = $headers[1];
        }

        $this->data['readme'] = preg_replace("/<h2>(.+)<\/h2>/", "<a name=\"$1\"></a><h2>$1</h2>", $this->data['readme']);
        $this->data['readme_date_modified'] = date ("F jS Y H:i:s", filemtime($filepath));


        return View::make('calf::pages.system.about.index', $this->data);
    }

    /**
     * [upload description]
     */
    public function upload()
    {
        $fieldname = Input::has('fieldname') ? Input::get('fieldname') : 'files';
        $fieldname = substr($fieldname, -2) == '[]' ? substr($fieldname, 0, -2) : $fieldname;

        $version = Input::has('version') ? Input::get('version') : 'original';
        $versions = $version === 'original' ? CalfFile::imageVersionSettings() : [];
        $path = CalfFile::path($version !== 'original' ? $version . '/' : '');
        $url = CalfFile::url($version !== 'original' ? $version : '', false);

        $upload = new UploadHandler(array(
            'upload_dir' => $path,
            'upload_url' => $url,
            'param_name' => $fieldname,
            'image_versions' => $versions
        ));
    }

    /**
     * [upload description]
     */
    public function upload_list()
    {
        $version = Input::has('version') ? Input::get('version') : 'original';

        if(
            Input::has('name') &&
            Input::has('fieldname') &&
            (
                $version == 'original' ||
                CalfFile::isValidImageVersion($version)
            )
        )
        {
            $fieldname = Input::has('fieldname') ? Input::get('fieldname') : 'files[]';
            
            return View::make('calf::regions.records.file-data', array(
                'fieldname' => $fieldname,
                'file'      => array('filename' => Input::get('name')),
                'version'   => $version,
                'options'   => array('view')
            ));
        }
        else
        {
            return '<div class="alert alert-danger">' . glyphicon('error') . ' Upload list request failed</div>';
        }
    }


    /**
     * Display uploaded files in web browser from outside web root
     */

    public function upload_view($filename)
    {
        $slash = substr_count(CalfFile::url('', false), '/') + 1;
        $prefix = Request::segment($slash);

        if(in_array($prefix, CalfFile::imageVersions()))
        {
            $filename = $prefix.'/'.$filename;
        }

        $path = CalfFile::path($filename);
        $mime = CalfFile::rawMimeType($filename);

        $file = File::get($path);

        $response = Response::make($file);
        $response->header('Content-Type', $mime);

        return $response;
    }


    /**
     * Display files stored in the asset library
     *
     * @return string
     */
    public function asset_library()
    {
        $rows = CalfFile::filterByPublished();

        if(Input::has('type'))
        {
            $rows = $rows->where(function ($query) {
                foreach(Input::get('type') as $key => $type) {
                    if($key === 0) {
                        $query->where('type', '=', $type);
                    } else {
                        $query->orWhere('type', '=', $type);
                    }
                }
            });
        }

        $rows = $rows->paginate(6);

        $this->data['records_count'] = View::make('calf::regions.records.count', array(
            'title' => CalfPermission::find('file')->title,
            'total' => $rows->getTotal(),
            'count' => $rows->count()
        ));

        $this->data['pagination'] = $rows->appends(Input::except('page'))->links();

        $this->data['rows'] = $rows;

        return View::make('calf::regions.records.asset-library', $this->data);
    }


    // public function asset_version_create($id, $version='original')
    // {
    //     $file = new CalfFile;
    //     $file = $file->find($id);

    //     if(
    //         CalfFile::isValidImageVersion($version) &&
    //         CalfFile::exists($file->filename) &&
    //         ! CalfFile::exists($version . '/' . $file->filename)
    //     )
    //     {
    //         $imageVersionSettings = CalfFile::imageVersionSettings();

    //         $upload = new CalfUpload(array(
    //             'upload_dir' => CalfFile::path(),
    //             'image_versions' => $imageVersionSettings
    //         ), false);

    //         $upload->create_scaled_image($file->filename, $version, $imageVersionSettings[$version]);
    //     }

    //     return View::make('calf::regions.records.file-data', array(
    //         'file' => array(
    //             'id' => $file->id,
    //             'type' => $file->type,
    //             'filename' => $file->filename,
    //             'title' => ucfirst($version) . ' version'),
    //         'options' => array('view','replace','upload','create-version'),
    //         'version' => $version,
    //     ));
    // }


    public function asset_version_upload($id, $version='original')
    {

    }


    public function tags()
    {
        $this->data['body_class'] .= ' listing';

        $permissions = CalfPermission::get();
        $permission_slug = sprintf('%s', Request::segment(3));
        $permission = CalfPermission::find($permission_slug);
        $models = registered_models();

        $stats = new CalfStats;
        $statsTotalRecords = $stats->filterByModel($models)->totalRecords();

        $tags = new CalfTag($models, 'tags');
        $tagsTotalCount = $tags->count(); // Return count for ALL tags
        $tagsDistinctCount = $tags->filterByDistinct()->count(); // Return count for distinct tags
        $rows = $tags->load()->stats($tagsTotalCount, $statsTotalRecords);

        $this->data['page_header'] = $permission->title;
        $this->data['permission_type'] = CalfPermissionType::find($this->data['permission_type_slug']);
        $this->data['permissions_by_type'] = CalfPermission::filterByType($permissions, $this->data['permission_type']->id);
        $this->data['permission'] = $permission;

        $this->data['secondary_nav'] = View::make('calf::regions.nav.secondary', array(
            'active' => $this->data['permission']->id,
            'parent' => $this->data['permission_type']->id,
            'items' => $this->data['permissions_by_type']
        ));

        $this->data['records_count'] = View::make('calf::regions.records.count', array(
            'title' => 'Distinct ' . strtolower($permission->alt_title),
            'total' => $tagsDistinctCount . '',
            'count' => $tagsDistinctCount . '',
        ));
        if($tagsDistinctCount)
        {
            $this->data['records_list'] = View::make('calf::regions.records.list', array(
                'headers' => array(
                    'title' => 'Tag',
                    'instances' => 'Instances',
                    'percent_tag' => '% <small class="text-muted">of</small> Tags',
                    'percent_record' => '% <small class="text-muted">of</small> Records',
                ),
                'rows' => $rows,
                'permission' => $permission,
            ));
        }

        return View::make('calf::layout.records-listing', $this->data);
    }


    /**
     * Returns a list of distinct tags
     * @return JSON
     */
    public function tags_list()
    {
        $tags = new CalfTag(registered_models(), 'tags');
        return array_values($tags->filterByDistinct()->get());
    }


    public function layouts_create()
    {
        return View::make('calf::pages.system.layouts.create', $this->data);
    }
}
