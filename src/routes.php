<?php

/*
|--------------------------------------------------------------------------
| Calf Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => Config::get('calf::request_root'), 'before' => 'calf.auth'), function() {

    /**
     * Error pages
     */
    Route::group(array('prefix' => 'error'), function() {
        Route::get('/unauthorized', array('as' => 'calf.unauthorized', 'uses' => 'CalfSystemController@unauthorized'));
        Route::get('/record', array('as' => 'calf.error.record', 'uses' => 'CalfSystemController@error_record_not_found'));
        Route::get('/page', array('as' => 'calf.error.page', 'uses' => 'CalfSystemController@error_page_not_found'));
    });

    /**
     * System routes
     */
    Route::get('/notifications', array('as' => 'calf.notifications', 'uses' => 'CalfSystemController@notifications'));
    Route::get('/dashboard', array('as' => 'calf.dashboard', 'uses' => 'CalfSystemController@dashboard'));
    Route::get('/system/about', array('as' => 'calf.about', 'uses' => 'CalfSystemController@about'));
    Route::get('/system/tags', array('as' => 'calf.tags', 'uses' => 'CalfSystemController@tags'));

        /**
         * Ajax routes
         */
        Route::group(array('prefix' => 'ajax'), function() {
            Route::any('/', array('as' => 'calf.ajax', 'uses' => 'CalfSystemController@ajax'));

            Route::any('/upload', array('as' => 'calf.upload', 'uses' => 'CalfSystemController@upload'));
            Route::any('/upload/list', array('as' => 'calf.upload.list', 'uses' => 'CalfSystemController@upload_list'));

            // Route::any('/asset-library/create/{id}/{version}', array('as' => 'calf.asset-version.create', 'uses' => 'CalfSystemController@asset_version_create'));
            Route::any('/asset-library/upload/{id}/{version}', array('as' => 'calf.asset-version.upload', 'uses' => 'CalfSystemController@asset_version_upload'));
            Route::any('/asset-library', array('as' => 'calf.asset-library', 'uses' => 'CalfSystemController@asset_library'));

            Route::get('/tags', array('as' => 'calf.tags-list', 'uses' => 'CalfSystemController@tags_list'));
        });

    /**
     * Account routes
     */
    Route::any('/account', array('as' => 'calf.account', 'uses' => 'CalfAccountController@account'));
    Route::any('/reset/{token}', array('as' => 'calf.reset.token', 'uses' => 'CalfAccountController@reset_token'));
    Route::any('/reset', array('as' => 'calf.reset', 'uses' => 'CalfAccountController@reset'));
    Route::any('/login', array('as' => 'calf.login', 'uses' => 'CalfAccountController@login'));
    Route::any('/logout', array('as' => 'calf.logout', 'uses' => 'CalfAccountController@logout'));

    /**
     * Generic routes
     */
    Route::get('/{type}', array('as' => 'calf', 'before' => 'calf.auth.level', 'uses' => 'CalfRecordController@index'));
    Route::any('/{type}/{permission}', array('as' => 'calf.listing', 'before' => 'calf.auth.level', 'uses' => 'CalfRecordController@listing'));
    Route::any('/{type}/{permission}/create', array('as' => 'calf.create', 'before' => 'calf.auth.level', 'uses' => 'CalfRecordController@create'));
    Route::any('/{type}/{permission}/edit/{id}/{revision}', array('as' => 'calf.revision', 'before' => 'calf.auth.level', 'uses' => 'CalfRecordController@edit'));
    Route::any('/{type}/{permission}/edit/{id}', array('as' => 'calf.edit', 'before' => 'calf.auth.level', 'uses' => 'CalfRecordController@edit'));
    Route::any('/{type}/{permission}/delete/{id}', array('as' => 'calf.delete', 'before' => 'calf.auth.level', 'uses' => 'CalfRecordController@delete'));
    Route::any('/{type}/{permission}/action', array('as' => 'calf.action', 'before' => 'calf.auth.level', 'uses' => 'CalfRecordController@action'));

    /**
    * Root
    */
    Route::get('/', array('as' => 'calf.root', 'uses' => 'CalfAccountController@index'));

});

/**
 * Uploads route handler
 */
Route::group(array('prefix' => Config::get('calf::upload_url')), function() {
    Route::get('/{filename}', array('as' => 'calf.uploads', 'uses' => 'CalfSystemController@upload_view'));

    foreach(CalfFile::imageVersions() as $v)
    {
        Route::get('/'.$v.'/{filename}', array('as' => 'calf.uploads.'.$v, 'uses' => 'CalfSystemController@upload_view'));
    }
});
