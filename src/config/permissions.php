<?php
/**
 * Permissions are the user administrable sections of Calf
 *
 * @var array
 * @param bool          advanced_filter     Flag to allow advanced filtering on listing page
 * @param string        alt_title           Smaller alt. title to the typically longer title option
 * @param bool          allow_sorting       Flag to allow sorting options on record listing page
 * @param string        auth_type           General authorization level required to access permission
 * @param boolean|int   allow_uploads       Flag to allow and manage file uploads. Parse int to define upload limit
 * @param array         actions             List of available batch editing features for this permission
 * @param string        buttons             List of text to display in different button types
 * @param array         filter_fields       List of fields to search in for record keyword filters
 * @param string        icon                Bootstrap glyphicon
 * @param array         meta_fields         Fields that should be considered meta content for the record
 * @param string        model               Permission model that should be used to manage data (defaults to CalfContent)
 * @param int|bool      paginate            Paginate records on listing page (int for number of records or bool to default)
 * @param bool          revisions_allowed   Defines if the permission should store revisions against each record created
 * @param bool          show_record_count   Flag to display record count data
 * @param array         sort_order          Default sorting order of permission
 * @param array         table_headers       List of table headers on listing page
 * @param array         table_options       List of options for each record on listing page
 * @param string        title               Human readable title for the permission, usually capitalized & pluralized
 * @param string        type                Type of permission as defined in ['permission_types']
 * @param array         validation_rules    Rules to validate form data defined as array for individual actions (create, edit)
 * @param array         views               Specify override views for actions
 */

return array(

    'about' => array(
        'type' => 'system',
        'auth_type' => 'developer',
        'title' => 'About',
        'icon' => 'book',
    ),

    'tags' => array(
        'type' => 'system',
        'auth_type' => 'editor',
        'title' => 'Keyword tags',
        'alt_title' => 'Tags',
        'icon' => 'tag',
    ),

    'user' => array(
        'type' => 'system',
        'model' => 'CalfUser',
        'auth_type' => 'administrator',
        'title' => 'Registered users',
        'alt_title' => 'Users',
        'icon' => 'user',
        'allow_sorting' => true,
        'filter_fields' => array('tags','meta','type','username','firstname','surname'),
        'advanced_filter' => true,
        'show_record_count' => true,
        'paginate' => true,
        'table_headers' => array(
            'id' => 'ID',
            'username' => 'Email',
            'firstname' => 'First name',
            'surname' => 'Surname',
            'type' => 'Type',
            'is_published' => 'Status',
        ),
        'table_options' => array('edit', 'delete'),
        'validation_rules' => array(
            'create' => array(
                'type' => 'required|in:contributor,editor,administrator,developer',
                'username' => 'required|email|unique:calf_user,username',
                'new_password' => 'required|min:6|confirmed',
                'new_password_confirmation' => 'required_with:new_password',
                'firstname' => 'required|alpha_num',
                'surname' => 'required|alpha_num',
            ),
            'edit' => array(
                'id' => 'required|integer|exists:calf_user,id',
                'type' => 'required|in:contributor,editor,administrator,developer',
                'username' => 'required|email',
                'new_password' => 'sometimes|min:6|confirmed',
                'new_password_confirmation' => 'sometimes|required_with:new_password',
                'firstname' => 'required|alpha_num',
                'surname' => 'required|alpha_num',
            ),
            'account' => array(
                'id' => 'required|integer|exists:calf_user,id',
                'type' => 'required|in:contributor,editor,administrator,developer',
                'username' => 'required|email',
                'new_password' => 'sometimes|min:6|confirmed',
                'new_password_confirmation' => 'sometimes|required_with:new_password',
                'firstname' => 'required|alpha_num',
                'surname' => 'required|alpha_num',
            ),
        ),
        'views' => array(
            'create' => 'calf::pages.system.users.edit',
            'edit' => 'calf::pages.system.users.edit',
            'account' => 'calf::pages.system.users.edit',
        ),
        'buttons' => array(
            'create' => 'Register new user',
        ),
    ),

    'file' => array(
        'type' => 'system',
        'model' => 'CalfFile',
        'auth_type' => 'editor',
        'title' => 'Files',
        'icon' => 'file',
        'allow_sorting' => true,
        'filter_fields' => array('tags','meta','type','filename','extension','mime_type','size','title','alt','caption'),
        'advanced_filter' => true,
        'show_record_count' => true,
        'paginate' => true,
        'table_headers' => array(
            'id' => 'ID',
            'fileAsset' => 'File',
            'title' => 'Title',
            'size' => 'Size',
            'fileTypeIcon' => 'Type',
            'type' => '',
            'mime_type' => 'MIME Type',
            'extension' => 'Ext.',
            'is_published' => 'Status',
        ),
        'table_options' => array('edit', 'delete'),
        'validation_rules' => array(
            'create' => array(
                'title' => 'required',
                'alt' => 'required',
                'files' => 'required',
            ),
            'edit' => array(
                'id' => 'required|integer|exists:calf_file,id',
                'title' => 'required',
                'type' => 'required|in:' . implode(',', raw_asset_types()),
                'alt' => 'required',
                'files' => 'required_without:filename',
                'filename' => 'required_without:files',
            ),
        ),
        'views' => array(
            'create' => 'calf::pages.system.files.edit',
            'edit' => 'calf::pages.system.files.edit',
        ),
        'buttons' => array(
            'create' => 'Upload new file',
        ),
    ),

);
