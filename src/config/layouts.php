<?php
/**
 * Content layout types
 * A layout is typically used for defining the template a record should use.
 * It can also be used to define categories for use in blog posts.
 *
 * Along with a named key value that acts as an ID for the layout, there are three settings each layout must contain:
 *
 * name (string) Human readable version of the layout for users to understand
 * type (string) Permission the layout typically belongs to
 * view (string) The view file the layout uses
 *
 * @var array
 * @param string name Human readable version of the layout for users to understand
 * @param string type Permission the layout typically belongs to
 * @param string view The view file the layout uses
 */

return array(
    'default' => array(
        'name' => 'Default',
        'type' => 'page',
        'view' => 'layouts.default'
    ),
);