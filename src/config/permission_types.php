<?php
/**
 * Permission types
 * These are the main permissions categories that permissions are grouped under in Calf
 *
 * @var array
 * @param string title - Human readable title for the permission type
 * @param string icon - Bootstrap glyphicon
 * @param string view - Path to view for permission type index
 * @param string auth_type - General authorization level required to access permission
 */

return array(
    'dashboard' => array(
        'title' => 'Dashboard',
        'icon' => 'dashboard',
        'view' => 'calf::pages.system.dashboard.index',
        'auth_type' => 'contributor',
    ),

    'content' => array(
        'title' => 'Content',
        'icon' => 'pencil',
        'view' => 'calf::pages.content.index',
        'auth_type' => 'contributor',
    ),

    'system' => array(
        'title' => 'System',
        'icon' => 'cog',
        'view' => 'calf::pages.system.index',
        'auth_type' => 'editor',
    ),
);