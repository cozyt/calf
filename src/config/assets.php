<?php
/**
 * Assets
 *
 * @var array
 */

return array(
    'css' => array(
        '/styles/styles.min.css',
    ),
    'js' => array(
        'https://www.google.com/jsapi',
        "/scripts/scripts.min.js",
    ),
);