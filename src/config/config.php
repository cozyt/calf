<?php

return array(

    /**
     * What should we call Calf throughout the system? This could be Admin, CMS or Calf (default)
     *
     * @var string
     */

    'call_me' => 'Calf',


    /**
     * Style of text to display in pre-written copy sections such as titles and lead copy
     *
     * @var string professional|friendly
     */

    'tone_of_voice' => 'friendly',


    /**
     * Root request for Calf
     *
     * @var string
     * @example 'request_root' => 'admin' // sets Calf root requests to http://yourdomain.com/admin
     */

    'request_root' => 'admin',


    /**
     * Default route to redirect the user to on login.
     * Parsed as a string, a simple named route such as calf.dashboard will be used.
     * Parsed as an array, the first key in the array defines the route to be used, the second key defines parameters to parse to the route
     *
     * @var string|array
     * @example 'redirect_on_login' => 'calf.dashboard' // Redirects user to calf.dashboard route
     * @example 'redirect_on_login' => array('calf.listing', array('system','user')) // Redirects user to calf.listing route with system and user parameters
     */

    'redirect_on_login' => 'calf.dashboard',


    /**
     * Unprotected routes accessible by non-authorized users
     *
     * @var array
     * @example 'unprotected_routes' => array('calf.login') // The calf.login route should be accessible by ALL non-authorized users
     */

    'unprotected_routes' => array(
        'calf.login',
        'calf.login.post',
        'calf.logout',
        'calf.remind',
        'calf.reset',
        'calf.reset.token',
        'calf.unauthorized'
    ),


    /**
     * Email addresses used to send notifications from/to
     */

    'email_addresses' => array(
        'from' => array('address' => 'localhost@domain.dev', 'name' => 'locahost'),
    ),


    /**
     * Email views for notifications, auth reminders etc
     */

    'email_views' => array(
        'auth_reminder'  => 'calf::emails.user.reminder',
        'record_created' => 'calf::emails.record.created',
        'user_created'   => 'calf::emails.user.created'
    ),


    /**
     * Identify if notifications should be sent.
     * Deals with records created by users that require the attention of a
     * higher level user.
     */

    'send_notifications' => false,


    /**
     * URL of Calf's assets directory
     *
     * @var string
     * @example 'assets_url' => '/assets' // Assets located at http://yourdomain.com/assets
     */

    'assets_url' => '/packages/cozyt/calf/assets',


    /**
     * Default pagination value
     *
     * @var int
     * @example 'pagination_count' => 10 // Displays 10 records per page
     */

    'pagination_count' => 10,


    /**
     * Absolute uploads path where ALL files are uploaded to
     *
     * @var string
     */

    'upload_path' => storage_path().'/uploads/',


    /**
     * Absolute public uploads path where ALL MEDIA file types such as audio and video
     * are copied to. The path appended to the public_path() MUST BE IDENTICAL to the 
     * path in the 'upload_url' setting
     *
     * @var string
     */

    'upload_public_path' => public_path().'/uploads/',


    /**
     * URL to uploads directory
     *
     * @var string
     * @example 'upload_url' => '/uploads' // Uploaded files can be accessed from http://yourdomain.com/uploads
     */

    'upload_url' => '/uploads',


    /**
     * Maximum size of file uploads
     *
     * @var string
     * @example 'upload_max_filesize' => 2097152 // Sets maximum upload size to 2Mb
     */

    'upload_max_filesize' => substr(ini_get('upload_max_filesize'), 0, -1) * 1024 * 1024, // 2097152


    /**
     * Maximum size of chunks sent in file upload
     *
     * @var string
     * @example 'upload_chunk_size' => 512000 // Sets chunk size to 500Kb
     */

    'upload_chunk_size' => 512000, // || substr(ini_get('post_max_size'), 0, -1) * 1024 * 1024,


    /**
     * Flag to determine if uploaded video or audio files should be automatically
     * pushed into the public directory.
     *
     * @var  boolean
     * @example  'upload_media_to_public' => true
     */

    'upload_media_to_public' => true
);
