<?php
/**
 * Array of valid batch actions that can be performed by a user and the
 * authentication level required by the user to perform that
 */

return array(
    'publish' => array(
        'auth_type' => 'editor',
    ),

    'unpublish' => array(
        'auth_type' => 'editor',
    ),

    'queue' => array(
        'auth_type' => 'editor',
    ),

    'delete' => array(
        'auth_type' => 'editor',
    ),

    'export' => array(
        'auth_type' => 'contributor',
    ),

    'arrange' => array(
        'auth_type' => 'contributor',
    ),
);
