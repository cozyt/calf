<?php
/**
 * Array of allowed extensions by generic file type
 *
 * @var array
 */

return array(
    'image'   => array('png','jpg','jpeg','gif'),
    'video'   => array('mp4','ogg','webm','mov','swf','flv'),
    'audio'   => array('mpeg','mp3','wav','aiff'),
    'archive' => array('zip'),
    'file'    => array('doc','docx','xls','xlsx','pdf','txt')
);