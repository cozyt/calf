<?php
/**
 * Versions of images to create during upload
 * Named key values with dimensions.
 *
 * Empty key
 * The empty image version key defines options for the original image
 *
 * upload_dir & upload_url params
 * Uncomment the following to use a defined directory for the thumbnails instead of a subdirectory based on the version identifier.
 * Make sure that this directory doesn't allow execution of files if you don't pose any restrictions on the type of uploaded files, e.g. by
 * copying the .htaccess file from the files directory for Apache:
 *
 * `max_width` _(string)_ Maximum width of the image version
 * `max_height` _(string)_ Maximum height of the image version
 * `auto_orient` _(bool)_ Automatically orientate the image base don its meta data
 * `crop` _(bool)_ Force the max dimensions and e.g. create square thumbnails
 * `upload_dir` _(string)_ Full path to the directory where the image version is stored
 * `upload_url` _(string)_ Full URL for the image version
 *
 * @var array
 * @param string max_width
 * @param string max_height
 * @param bool auto_orient
 * @param bool crop - Force the max dimensions and e.g. create square thumbnails
 * @param string upload_dir - e.g. dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
 * @param string upload_url - e.g. $this->get_full_url().'/thumb/'
 */

return array(
    // 'tablet-x2' => array('max_width' => 1536, 'max_height' => 2048),
    // 'tablet'    => array('max_width' => 768,  'max_height' => 1024),
    // 'mobile-x2' => array('max_width' => 640,  'max_height' => 1136),
    // 'mobile'    => array('max_width' => 320,  'max_height' => 568),
    'thumbnail' => array('max_width' => 100,  'max_height' => 100),
);