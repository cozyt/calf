<?php

class CalfSort
{
	static public function by($default = 'id')
	{
		return Input::has('sort_by') ? Input::get('sort_by') : $default;
	}

	static public function dir($default = 'asc')
	{
		return Input::has('sort_dir') ? Input::get('sort_dir') : $default;
	}

	static public function querystring($column, $by=NULL, $dir=NULL, $args=array())
	{
		$by = $by !== NULL ? $by : self::by();
		$dir = $dir !== NULL ? $dir : self::dir();

	 	$args = array_merge(Input::get(), $args);

		$args['sort_by'] = $column;
		$args['sort_dir'] = $column == $by && $dir == 'asc' ? 'desc' : 'asc';

		return $args;
	}

	static public function url($column, $by=NULL, $dir=NULL, $args=array())
	{
		$args = self::querystring($column, $by, $dir, $args);

		return URL::route(Route::currentRouteName(), $args);
	}

}