<?php

Class CalfHelper {

    /**
     * Return the currently logged in users ID
     *
     * @return int
     */
    static public function authUserId()
    {
        return Auth::user()->id;
    }

    /**
     * Check if the current logged in user matches a specific type
     *
     * @param  string $type
     * @return boolean
     */
    static public function authUserIsType($type='')
    {
        return self::authUserTypeIsAuthorized($type, '=');
    }

    /**
     * Check if the current logged in user's type is authorized for a given permission type
     * Check explicitly for a permission level using the $scope parameter
     *
     * @param  string $permissionLevel
     * @param  string $scope
     * @return boolean
     * @todo  Implement scoping for permission level comparison
     */
    static public function authUserTypeIsAuthorized($permissionLevel, $scope='>=')
    {
        if($scope == '=')
        {
            return Auth::user()->type === $permissionLevel;
        }
        else
        {
            $userType = Auth::user()->type;

            switch ($userType)
            {
                // user is a contributor
                case 'contributor' :
                    return ! in_array($permissionLevel, array('editor', 'administrator', 'developer'));

                // user is an editor
                case 'editor' :
                    return ! in_array($permissionLevel, array('administrator', 'developer'));

                // user is an administrator
                case 'administrator' :
                    return ! in_array($permissionLevel, array('developer'));

                // user is a developer
                case 'developer' :
                    return true;

                // user type invalid or not given
                default :
                    return false;
            }
        }
    }

    /**
     * Returns an array of allowed file extensions from a given raw type
     * @param  string|array $type
     * @return array
     */
    static public function allowedExtensionsFromRawType($type)
    {
        $ext = array();

        if(is_array($type))
        {
            foreach($type as $t)
            {
                $ext = array_merge($ext, self::allowedExtensionsFromRawType($t));
            }
        }
        else
        {
            $ext = Config::get('calf::allowed_extensions.'.$type);
        }

        return $ext;
    }







    /**************************************************************************
     * DEPRECATED
     * These methods will be removed in future releases of Calf.
     * You should use the prefered methods to produce the same/similar results.
     **************************************************************************/

    static public function registeredModels()
    {
        return registered_models();
    }

    static public function publishedStatus($published, $publishDate='')
    {
        return published_status($published, $publishDate);
    }

    static public function publishedStatusLabel($published=NULL, $publishDate='0000-00-00 00:00:00')
    {
        return View::make('calf::regions.records.published-status-label', array('status' => $published, 'publishDate' => $publishDate));
    }

    static public function bootstrapColor($status)
    {
        return published_status_color($status);
    }

    static public function dumpToConsole($data, $type='log')
    {
        console_dump($data, $type);
    }

    static public function dump($data)
    {
        printr($data);
    }
}