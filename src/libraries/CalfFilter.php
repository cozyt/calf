<?php

class CalfFilter
{
    /**
     * Keywords filter
     *
     * Scope behaviour
     * - Contains
     *   Searches for matches in $columns where ALL words in the filter
     *   term ($value) are found. Essentially `where term AND term`.
     *   E.g. `Cat dog` - search for records that have `cat` and `dog` in them
     *
     * - Keywords
     *   Search for matches in $columns where ANY word in the filter
     *   term ($value) is cound. Essentially `where term or term`.
     *   E.g. `Cat dog` - search for records that contains `cat` or `dog`
     *
     * - Phrase
     *   Searches for matches in $columns where the filter term ($value)
     *   is matched exactly as a single term.
     *   E.g. `Cat dog` - search for records that have exactly `cat dog` in them
     *
     * @param  object $query
     * @param  array  $columns
     * @param  string|int $value
     * @param  string $scope
     * @return object
     * @todo Needs an alternative option to also search "word OR word"
     */
    static public function keyword($query, $columns=array(), $value=NULL, $scope='phrase')
	{
        if($value !== NULL && count($columns))
        {
            switch($scope)
            {
                case 'keywords' :
                    $query = self::scopeWithKeywords($query, $columns, explode(' ', $value));
                    break;

                case 'contains' :
                    $query = self::scopeContainsKeyword($query, $columns, explode(' ', $value));
                    break;

                case 'phrase' :
                default :
                    $query = self::scopeAsPhrase($query, $columns, $value);
                    break;
            }
        }

        return $query;
	}

    /**
     * Date filter
     */
    static public function date($query, $column, $value, $scope='on')
    {
        if($value !== NULL)
        {
            switch ($scope)
            {
                case 'before':
                    $scope = '<=';
                    break;

                case 'after':
                    $scope = '>=';
                    break;

                case 'not':
                    $scope = '!=';
                    break;

                case 'on' :
                    $scope = 'like';
                    $value .= '%';
                    break;
            }

            $query = $query->where($column, $scope, $value);
        }

        return $query;
    }

    /**
     * scopeAsPhrase
     *
     * @param  object $query
     * @param  array  $columns
     * @param  string $phrase
     * @return object
     */
    static public function scopeAsPhrase($query, $columns=array(), $phrase='')
    {
        $query = $query->where(function($query) use ($columns, $phrase) {
            foreach($columns as $column)
            {
                $query->orWhere($column, 'like', '%'.$phrase.'%');
            }
        });

        return $query;
    }

    /**
     * scopeAsPhrase
     *
     * @param  object $query
     * @param  array  $columns
     * @param  string $phrase
     * @return object
     */
    static public function scopeWithKeywords($query, $columns=array(), $keywords=array())
    {
        $query = $query->where(function($query) use ($columns, $keywords) {
            foreach($keywords as $keyword)
            {
                foreach($columns as $column)
                {
                    $query->orWhere($column, 'like', '%'.$keyword.'%');
                }
            }
        });

        return $query;
    }

    /**
     * scopeAsPhrase
     *
     * @param  object $query
     * @param  array  $columns
     * @param  string $phrase
     * @return object
     */
    static public function scopeContainsKeyword($query, $columns=array(), $keywords=array())
    {
        foreach($keywords as $keyword)
        {
            $query = $query->where(function($query) use ($columns, $keyword) {
                foreach($columns as $column)
                {
                    $query->orWhere($column, 'like', '%'.$keyword.'%');
                }
            });
        }

        return $query;
    }
}