<?php

/*
|--------------------------------------------------------------------------
| Calf Filters
|--------------------------------------------------------------------------
|
*/

Route::filter('calf.ajax', 'CalfAjaxFilter');
Route::filter('calf.auth', 'CalfAuthFilter');
Route::filter('calf.auth.level', 'CalfAuthLevelFilter');