{{--
Creates a button with an icon for displaying an option.
Used in record listings to show edit/delete options

@params $actions
@params $btn_style
--}}

<div class="btn-action-dropdown">
    @if(count($actions))
        <div class="btn-group">

            <a
             href="#action"
             class="btn btn-default {{ $btn_style or '' }} dropdown-toggle  js-action-button"
             data-toggle="dropdown">
                {{ glyphicon('action') }}
                Batch actions <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                @foreach($actions as $action)
                    @if( Config::get('calf::actions.' . $action) && CalfHelper::authUserTypeIsAuthorized(Config::get('calf::actions.' . $action . '.auth_type'), '>=') )
                        <li>
                            <button
                             type="submit"
                             name="action" value="{{ $action }}"
                             class="btn btn-link"
                             title="{{ ucfirst($action) }}">
                                {{ glyphicon($action) }}
                                {{ ucfirst($action) }}
                            </button>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>

    @endif
</div>
