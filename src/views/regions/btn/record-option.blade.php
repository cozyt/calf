{{--
Creates a button with an icon for displaying an option.
Used in record listings to show edit/delete options

@params $option
@params $btn_style
@params $show_text
--}}

<?php
$args = array('type' => $type, 'permission' => $permission, 'id' => $id);

if($option === 'revision')
{
    $args['revision'] = $revision;
}
?>

<a
 href="{{ URL::route('calf.'.$option, $args) }}"
 name="{{ $option }}"
 class="btn {{ isset($btn_style) ? 'btn-'.$btn_style : ''}}"
 title="{{ ucfirst($option) }}">
    {{ glyphicon($option) }}

    @if(isset($show_text) && $show_text)
        {{ ucfirst($option) }}
    @endif
</a>
