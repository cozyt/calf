{{--
Create permissions record dropdown menu button

@params $permissions
@params $btn_style
@params $type
--}}

<div class="btn-create-dropdown">

    @if(count($permissions))
        <div class="btn-group">

            <a
             href="#create"
             class="btn btn-primary {{ $btn_style or '' }} dropdown-toggle"
             data-toggle="dropdown">
                Create new {{ $type or '' }} record <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                @foreach($permissions as $permission)
                    @if( ! isset($permission->auth_type) || CalfHelper::authUserTypeIsAuthorized($permission->auth_type, '>=') )
                        <li>
                            <a href="{{ URL::route('calf.create', array('type' => $type, 'permission' => $permission->id)) }}">
                                {{ $permission->buttons['create'] or 'Create new ' . strtolower($permission->title) }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    @endif

</div>
