{{-----------------------------------------------------------------------------
 * Asset library
 * Renders results of the asset library items in a grid for 
 * selection/assignment to a record.
 *
 * @path regions.records
 * @return string
-----------------------------------------------------------------------------}}


<div class="asset-library">
	<div class="{{ $rows->getLastPage() <= 6 ? 'pull-right' : '' }}">
		{{ $pagination or '' }}
	</div>

	{{ $records_count or '' }}

	@if( $rows->count() )
		<div class="row">

			@foreach($rows as $row)
				<div class="col-xs-6 col-md-4 asset-library__item">
					<label class="thumbnail">

						<div class="asset-library__item__icon">
							<div class="file-icon">
								{{ asset_html($row->filename) }}
							</div>
						</div>

						<input class="asset-library__item__checkbox" type="checkbox" value="{{ $row->id }}" /> 
						<h6 class="asset-library__item__title">{{ $row->title }}</h6>

					</label>
				</div>
			@endforeach

		</div>
	@endif
</div>
