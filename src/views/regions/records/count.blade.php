{{-----------------------------------------------------------------------------
 * Record count
 * Renders a region containing count information for a given recordset
 * 
 * @path regions.records
 * @return string
-----------------------------------------------------------------------------}}


<div class="records-count">
	<p class="lead">{{ $total or 0 }} {{ $title or 'Records' }} found</p>
	<p>Showing {{ $count or 0 }} of {{ $total or 0 }} records</p>
</div>