<?php
if($status === 1 || $status === 0 || $status === true || $status === false)
{
    $status = published_status($status, isset($publishDate) ? $publishDate : '');
}
?>

<span class="{{ $type or 'label' }} {{ $type or 'label' }}-{{ published_status_color($status) }}">
    @if(isset($with_icon) && $with_icon === true || ! isset($with_icon))
        {{ published_status_icon($status) }}
    @endif

    {{ ucfirst($status) }}
</span>