{{--
 * File data
 * Creates a region containing file information for a given record
 *
 * @param (string) 	fieldname 	Defaults to files[]
 * @param (object) 	file 		Requires at least one property named filename
 * @param (array) 	options
 * @param (string) 	version		Defaults to original
 * @path regions.records
 * @return string
--}}

<?php
if(is_array($file)) $file = (object) $file; // cast to object for consistency

$version 	= isset($version) && $version ? $version : 'original';
$fileName 	= isset($file->filename) && $file->filename ? $file->filename : '';
$fileType   = CalfFile::rawType($fileName);
$fileExists	= false;

if($fileName)
{
	$fileRel 	= ($version !== 'original' ? $version .'/' : '') . $fileName;
	$fileExists = CalfFile::exists($fileRel, false); // $fileType !== 'video' && $fileType !== 'audio'
	$fileData 	= $fileExists ? CalfFile::rawFileData($fileRel) : [];

	$fieldname 	= isset($fieldname) ? $fieldname : 'files[]';
	$guid 		= time() . md5(isset($fileRel) ? $fileRel : '') . rand();
	$canReplace = $fileExists && isset($options) && in_array('replace', $options) && isset($replace_type);
}
?>


@if($fileName)
<div class="panel panel-default file-data" id="file-data-{{ $guid }}">

	{{--
	File title or name
	--}}

	<div class="panel-heading">
		<h4 class="panel-title">{{ $file->title or $fileName }}</h4>
	</div>





	<div class="panel-body file-data__body">

		{{--
		File icon or image
		--}}

		<div class="col-md-4 file-data__icon">
			<div class="file-icon" data-toggle="modal" data-target="#display-file-{{ $guid }}">
                @if((($fileType === 'video' || $fileType === 'audio') && ! CalfFile::exists($fileRel, true)) || ! $fileExists)
                    {{ asset_type_icon($fileType) }}
                @else
                    {{ asset_html($fileRel) }}
                @endif
			</div>
		</div>





		<div class="col-md-8 file-data__info">

			{{--
			Helpful alert when the file does not exist
			--}}

			@if(! $fileExists )
				<p>
					The file could not be found. <br>
					<small>
						It may be because {{ $version !== 'original' ? 'this version' : 'it' }} has not been created yet.

						{{-- <br>Sometimes this can occur if the developer has created additional versions of an asset that should be created after the original was uploaded. --}}

						{{-- @if(CalfHelper::authUserTypeIsAuthorized('administrator')) --}}
							{{-- <nobr><a href="{{ route('calf.asset-version.create', array('version' => $version, 'id' => $file->id)) }}"><strong>Fix this issue</strong></a></nobr> --}}
						{{-- @endif --}}
					</small>
				</p>
			@endif





			{{--
			File paths and URLS
			--}}

			@if( $fileExists )
				<ul class="list-unstyled">
					@if((isset($file->title) && $file->title) || $version !== 'version')
						<li><strong>Filename:</strong>&nbsp;{{ $fileName }}</li>
					@endif

					<li><strong>Url:</strong>&nbsp;{{ CalfFile::url($fileRel, false) }}</li>

					@if(CalfHelper::authUserTypeIsAuthorized('developer'))
						<li>
							<small>
								<strong>Path:</strong>&nbsp;{{ str_replace(base_path(), '~', CalfFile::path($fileRel, false)) }}
							</small>
						</li>
						@if($fileData['type'] === 'video' || $fileData['type'] === 'audio')
						<li>
							<small>
								<strong>Public:</strong>&nbsp;{{ str_replace(base_path(), '~', CalfFile::path($fileRel, true)) }}
							</small>
						</li>
						@endif
					@endif
				</ul>
			@endif





			{{--
			File meta data
			--}}

			<dl class="text-muted">
				@if( $fileExists )
					@foreach($fileData as $title => $data)
						@if($title != 'path' && $title != 'storage_path' && $title != 'url')
							<dt> {{ ucfirst($title) }}</dt>
							<dd>{{ $data }}</dd>
						@endif
					@endforeach
				@endif

				@if(isset($version) && $version !== 'original')
					<dt> Version</dt>
					<dd>{{ $version }}</dd>

					@foreach(CalfFile::imageVersionSettings($version) as $title => $data)
						<dt> {{ ucfirst(str_replace('_', ' ', $title)) }}</dt>
						<dd>{{ $data }}</dd>
					@endforeach
				@endif
			</dl>




			{{--
			File options and actions
			--}}

			@if( isset($options) )
				<div class="pull-right btn-group file-data__options">

					{{-- View option --}}
					@if( in_array('view', $options) && CalfFile::exists($fileRel, true))
					<a href="{{ CalfFile::url($fileRel) }}" target="_blank" class="btn btn-sm btn-default">
						{{ glyphicon('view') }}
						<span class="hidden-xs">View</span>
					</a>
					@endif

					@if( isset($file->id) || isset($file->file_id) )

						{{-- Edit option --}}
						@if( $fileExists && in_array('edit', $options) )
						<a href="{{ URL::route('calf.edit', array('system', 'file', $file->id)) }}" class="btn btn-sm btn-default">
							{{ glyphicon('edit') }}
							<span class="hidden-xs">Edit</span>
						</a>
						@endif


						{{-- Delete option --}}
						@if( $fileExists && in_array('delete', $options) )
						<a href="{{ URL::route('calf.delete', array('system', 'file', $file->id)) }}" class="btn btn-sm btn-danger">
							{{ glyphicon('trash') }}
							<span class="hidden-xs">Delete</span>
						</a>
						@endif


						{{-- Unlink option --}}
						@if( $fileExists && in_array('unlink', $options) )
						<label class="btn btn-sm btn-default btn-toggle">
							<input name="remove_{{ $fieldname }}" type="checkbox" value="{{ $fileName }}">
							{{ glyphicon('remove') }}
							<span class="hidden-xs">Remove</span>
						</label>
						@endif


						{{-- Replace option --}}
						@if( $canReplace )
						<button class="btn btn-sm btn-default" type="button" data-toggle="collapse" data-target="#replace-asset-{{ $guid }}" aria-expanded="false" aria-controls="replace-asset-{{ $guid }}">
							{{ glyphicon('replace') }}
							<span class="hidden-xs">Replace</span>
						</button>
						@endif


						{{-- Create new version option --}}
						{{--
						@if( ! $fileExists && in_array('create-version', $options) && $version && $file->type === 'image')
						<a href="{{ route('calf.asset-version.create', array('version' => $version, 'id' => $file->id)) }}" class="btn btn-sm btn-default"
						 data-ajax="true"
						 data-response-action="replace"
						 data-response-type="html"
						 data-response-url="{{ route('calf.asset-version.create', array('version' => $version, 'id' => $file->id)) }}"
						 data-response-target="#file-data-{{ $guid }}">
							{{ glyphicon('create') }}
							<span class="hidden-xs">Create</span>
						</a>
						@endif
						--}}

					@endif
				</div>{{-- /.file-data__options --}}
			@endif

		</div>{{-- /.file-data__info --}}





		{{--
		Upload field for replacing a file
		--}}

		@if( $canReplace )
			<div class="col-md-12 collapse file-data__replace" id="replace-asset-{{ $guid }}">
				@include('calf::regions.forms.fieldsets.upload', array('type' => $replace_type, 'fieldname' => $fieldname, 'legend' => false, 'label' => 'Select new file...', 'version' => $version, 'btn_class' => 'btn-primary btn-sm'))
			</div>
		@endif



	</div>{{-- /.file-data__body --}}





	{{--
	Modal window for displaying the file
	--}}

	@if( $fileExists && CalfFile::exists($fileRel, true))
		<div class="modal fade" tabindex="-1" role="dialog" id="display-file-{{ $guid }}">
		  <div class="modal-dialog modal-{{ $modal_size or 'lg' }}">
		    <div class="modal-content">
				<div class="modal-body">
					{{ asset_html($fileRel) }}
				</div>
		    </div>
		  </div>
		</div>
	@endif





	{{-- @if($version == 'original') --}}
		<input type="hidden" name="{{ $fieldname }}" value="{{ $file->filename }}" readonly/>
	{{-- @endif --}}

</div>
@endif
