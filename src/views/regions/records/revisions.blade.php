@if( isset($rows) && count($rows) )

    <?php
    // These act as our defaults
    // unfortunately we need some defaults sometimes and blade doesn't seem to have an elegant way of doing this
    if(! isset($headers) )
    {
        $headers = array(
            'revision' => 'ID',
            'created_by' => '<span class="visible-sm">User</span><nobr class="hidden-sm">Created by</nobr>',
            'created_at' => '<span class="visible-sm">Date</span><nobr class="hidden-sm">Date created</nobr>',
            'is_published' => 'Status',
        );
    }
    ?>

    <div class="revisions">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">{{ $title or 'Revisions' }}</h4>
            </div>

            @include('calf::regions.records.list', array('headers' => $headers, 'rows' => $rows, 'permission' => $permission, 'options' => array('revision')))

            @if( isset($current_revision_notes) )
                @include('calf::regions.forms.fields.textarea', array('name' => 'current_revision_notes', 'value' => $current_revision_notes, 'rowHeight' => 3))
            @endif
        </div>

        <p class="text-muted">
            <small>Any unsaved changes will be lost.</small>
        </p>
    </div>
@endif