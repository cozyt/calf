{{-----------------------------------------------------------------------------
 * Record listing
 * Display a given recordset in a formated table
 *
 * @path regions.records
 * @return string
-----------------------------------------------------------------------------}}

@if( isset($rows) && count($rows) )

<div class="table-responsive  records-list {{ isset($sortable) && $sortable ? 'record-list--sortable' : '' }}">
    <table name="records" role="grid" class="table table-striped table-hover"
     {{ isset($sortable) && $sortable ? 'data-type="sortable" data-column-count="' . count($headers) . '"' : '' }}>
        @if( $headers )

        <thead>
            <tr>
                @foreach($headers as $header_key => $header)
                <th>
                    @if( isset($sort_by) && $header_key != 'fileAsset')
                        <a href="{{ CalfSort::url($header_key, $sort_by, $sort_dir, isset($sort_args) ? $sort_args : array()) }}">
                            @if($header_key == $sort_by)
                                <span class="glyphicon glyphicon-sort-by-attributes{{ $sort_dir == 'desc' ? '-alt' : '' }}"></span>
                            @endif
                            {{ $header }}
                        </a>
                    @else
                        {{ $header }}
                    @endif
                </th>
                @endforeach

                @if( isset($options) )
                    <th width="{{ 40*(count($options) + (isset($actions) && $actions ? 1 : 0)) + 16 }}"> </th>
                @endif
            </tr>
        </thead>

        <tbody>
            @foreach($rows as $row)
            <tr class="{{ $row->row_class or '' }}">
                @foreach(array_keys($headers) as $header)
                <td>
                    @if($header === 'fileTypeIcon')
                        <div class="file-icon  records-list__file-icon">
                            @if(count($row->files))
                                {{ asset_type_icon($row->files[0]->type) }}
                            @else
                                {{ asset_type_icon($row->type) }}
                            @endif
                        </div>

                    @elseif($header === 'fileAsset')
                        <div class="file-icon  records-list__file-icon">
                            @if(count($row->files))
                                {{ asset_html($row->files[0]->filename) }}
                            @else
                                {{ asset_html($row->filename) }}
                            @endif
                        </div>

                    @elseif(substr($header, 0, '8') === 'percent_')
                        <div class="row">
                            <div class="hidden-xs col-sm-3">
                                {{ $row->$header }}%
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $row->$header }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $row->$header }}%;">
                                        <div class="visible-xs">
                                            {{ $row->$header }}%
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @elseif($header == 'layout')
                        @if(CalfLayout::find($row->layout))
                            {{ CalfLayout::find($row->layout)->name }}
                        @endif

                    @elseif($header == 'is_published')
                        {{-- CalfHelper::publishedStatusLabel($row->is_published, $row->published_at) --}}

                        @include('calf::regions.records.published-status-label', array('status' => $row->is_published, 'publishDate' => $row->published_at, 'type' => 'label'))

                    @elseif($header == 'id' && (isset($with_id_field) && $with_id_field) && (! isset($actions) || ! $actions))
                        <input type="{{ $with_id_field }}" name="ids[]" value="{{ $row->id }}" readonly>
                        {{ $row->id }}

                    @elseif(strstr($header, '->'))
                        <?php
                        $rowRelatedProp = $row;
                        foreach(explode('->', $header) as $h)
                        {
                            $rowRelatedProp = $rowRelatedProp->$h;
                        }
                        echo $rowRelatedProp;
                        ?>
                    @else
                        {{ $row->$header or '' }}
                    @endif
                </td>
                @endforeach

                @if( isset($options) )
                    <td>
                        <nav role="navigation" class="btn-group">
                            @if(count($options))
                                @foreach($options as $option)
                                    @include('calf::regions.btn.record-option', array('option' => $option, 'type' => $permission->type, 'permission' => $permission->id, 'id' => $row->id, 'revision' => $row->revision, 'btn_style' => 'default btn-sm'))
                                @endforeach

                                @if(isset($actions) && $actions)
                                    <label class="btn btn-default btn-toggle btn-sm  js-action-button-toggle" data-target=".js-action-button">
                                        <input type="checkbox" name="ids[]" value="{{ $row->id }}">
                                        {{ glyphicon('batch') }}
                                    </label>
                                @endif
                            @endif
                        </nav>
                    </td>
                @endif

            </tr>
            @endforeach
        </tbody>

        @endif
    </table>
</div>

@endif
