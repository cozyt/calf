<div class="alert{{ isset($type) ? ' alert-'.$type : '' }} messages-general">
	@if( isset($title) && $title )
		<p><strong>{{ $title }}</strong></p>
	@endif

	@if( isset($messages) && $messages )
		<ul class="list-unstyled">
		@foreach($messages as $message)
			<li>
				{{ $message }}
			</li>
		@endforeach
		</ul>
	@endif
</div>