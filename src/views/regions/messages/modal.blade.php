<?php $id = md5(rand(0, 1000)); ?>

<div class="messages-modal">
	<a href="#modal" class="btn btn-{{ $btn_type or 'default' }} messages-modal--btn" data-toggle="modal" data-target="#{{ $id }}">
		@yield('messages-modal--btn')
	</a>
	<div class="modal fade messages-modal--modal" tabindex="-1" role="dialog" id="{{ $id }}">
	  <div class="modal-dialog modal-{{ $modal_size or 'lg' }}">
	    <div class="modal-content">
			@yield('messages-modal--content')
	    </div>
	  </div>
	</div>
</div>