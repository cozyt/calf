<form action="{{ route('calf.action', array('type' => $permission->type, 'permission' => $permission->id)) }}" method="get" class="form" role="form" name="batch">
    <input type="hidden" name="action" value="{{ $action }}" />

        {{-- Publish action --}}
        @if($action === 'publish')
            <input type="hidden" name="is_published" value="1" />

            <div class="row">
                <div class="col col-sm-5 col-md-3">
                    <fieldset>
                        @include('calf::regions.forms.fields.datetime', array('name' => 'publish_at', 'label' => '', 'value' => date('Y-m-d H:i:s')))
                    </fieldset>
                </div>

                <div class="col col-sm-6">
                    @include('calf::regions.forms.fieldsets.form-options', array('action' => 'publish', 'permission' => $permission, 'save_text' => 'Publish ' . Str::plural($permission->title), 'save_icon' => 'publish', 'position' => ''))
                </div>
            </div>
        @endif

        {{-- Unpublish action --}}
        @if($action === 'unpublish')
            <input type="hidden" name="is_published" value="0" />

            @include('calf::regions.forms.fieldsets.form-options', array('action' => 'unpublish', 'permission' => $permission, 'save_text' => 'Unpublish ' . Str::plural($permission->title), 'save_icon' => 'unpublish', 'save_class' => 'btn-' . published_status_color('unpublish'), 'position' => ''))
        @endif

        {{-- Queue action --}}
        @if($action === 'queue')
            <input type="hidden" name="is_published" value="1" />

            <div class="row">
                <div class="col col-sm-6 col-md-3">
                    <fieldset>
                        @include('calf::regions.forms.fields.datetime', array('name' => 'publish_at', 'label' => '', 'value' => date('Y-m-d H:i:s')))
                    </fieldset>
                </div>

                <div class="col col-sm-6">
                    @include('calf::regions.forms.fieldsets.form-options', array('action' => 'queue', 'permission' => $permission, 'save_text' => 'Queue ' . Str::plural($permission->title), 'save_icon' => 'queue', 'save_class' => 'btn-' . published_status_color('queue'), 'position' => ''))
                </div>
            </div>
        @endif

        {{-- Delete action --}}
        @if($action === 'delete')
            @include('calf::regions.forms.fieldsets.form-options', array('action' => 'delete', 'permission' => $permission, 'save_text' => 'Delete ' . Str::plural($permission->title), 'save_icon' => 'delete', 'save_class' => 'btn-danger', 'position' => ''))
        @endif


        {{-- Export action --}}
        @if($action === 'export')
            @include('calf::regions.forms.fieldsets.form-options', array('action' => 'export', 'permission' => $permission, 'save_text' => 'Export ' . Str::plural($permission->title), 'save_icon' => 'export', 'position' => ''))
        @endif


        {{-- Arrange action --}}
        @if($action === 'arrange')
            @include('calf::regions.forms.fieldsets.form-options', array('action' => 'arrange', 'permission' => $permission, 'save_text' => 'Save ' . Str::singular($permission->title) . ' arrangement', 'save_icon' => 'arrange', 'position' => ''))
        @endif

    </fieldset>

    <br><br>

    <fieldset>
        <legend>Selected records</legend>
        {{ $records_list or '' }}
    </fieldset>

</form>
