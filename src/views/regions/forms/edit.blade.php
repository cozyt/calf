<form action="" method="post" role="form" class="form form-record form-record-{{ $action }}" name="{{ $action }}-record">
	@if($action == 'edit')
	<input type="hidden" name="id" value="{{ $row->id or 0 }}" />
	@endif

	<div class="col-sm-6 form-record--article">
		@yield('form')
	</div>

	<div class="col-sm-6 form-record--aside">
		@yield('aside_fields_header')

		@include('calf::regions.forms.fieldsets.meta', array('action' => $action, 'row' => $row, 'fields' => isset($permission->meta_fields) ? $permission->meta_fields : array('tags','meta','publish')))

		@yield('aside_fields_footer')

		@include('calf::regions.forms.fieldsets.form-options', array('action' => $action, 'permission' => $permission, 'record' => $row))

		@if( isset($revisions) )
			@include('calf::regions.records.revisions', array('rows' => $revisions, 'permission' => $permission, 'current_revision_notes' => $current_revision_notes))
		@endif

	</div>
</form>
