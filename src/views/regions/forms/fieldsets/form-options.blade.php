<fieldset class="form-record--options" name="form-options">
    <div class="{{ $position or 'pull-right' }}">
        <button type="submit" name="form-submit" id="form-submit" class="btn btn-primary {{ $save_class or '' }}" value="{{ $save_value or 1 }}">
            {{ glyphicon(isset($save_icon) ? $save_icon : 'save') }}
            <span class="hidden-sm">
                @if(isset($save_text))
                    {{ ucfirst(strtolower($save_text)) }}
                @else
                    {{ $permission->buttons['edit'] or 'Save '.$permission->id.' details' }}
                @endif
            </span>
        </button>

        @if($action == 'edit')
        <a href="{{ URL::route('calf.delete', array($permission->type, $permission->id, $record->id)) }}" class="btn btn-danger {{ $delete_class or '' }}">
            {{ glyphicon(isset($delete_icon) ? $delete_icon : 'delete') }}
            <span class="hidden-xs hidden-sm">
                @if(isset($delete_text))
                    {{ ucfirst(strtolower($delete_text)) }}
                @else
                    {{ $permission->buttons['delete'] or 'Delete' }}
                @endif
            </span>
        </a>
        @endif

        <a href="{{ URL::route('calf.listing', array($permission->type, $permission->id)) }}" class="btn btn-default {{ $cancel_class or '' }}">
            {{ glyphicon(isset($cancel_icon) ? $cancel_icon : 'cancel') }}
            <span class="hidden-xs hidden-sm">
                @if(isset($cancel_text))
                    {{ ucfirst(strtolower($cancel_text)) }}
                @else
                    {{ $permission->buttons['cancel'] or 'Cancel' }}
                @endif
            </span>
        </a>
    </div>
</fieldset>