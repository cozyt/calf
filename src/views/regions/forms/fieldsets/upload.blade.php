{{-----------------------------------------------------------------------------
 * Upload
 * Generates a fieldset for uploading files for a specific record
 *
 * @path regions.forms.fieldset
 * @return string
-----------------------------------------------------------------------------}}

<?php 
$type       = isset($type) ? $type : ['image','video','audio','file'];
$version    = isset($version) && $version ? $version : 'original';
$multiple   = isset($multiple) && $multiple ? 'multiple' : '';

// $fieldname = isset($fieldname) ? $fieldname : 'files';
// $fieldname = substr($fieldname, -2) == '[]' ? substr($fieldname, 0, -2) : $fieldname;
// $fieldname .= $version !== 'original' ? '_' . $version : '';

// List of allowed file extensions
$allowed_extensions = isset($allowed_extensions) ? $allowed_extensions : CalfHelper::allowedExtensionsFromRawType($type);
$allowed_extensions = $allowed_extensions !== null ? $allowed_extensions : array();

// ID of the upload element
$id = isset($id) ? $id : md5($fieldname . implode('', $allowed_extensions) . rand());

// URL that handles uploads of files
$upload_url = isset($upload_url) ? $upload_url : URL::route('calf.upload', array('fieldname' => $fieldname, 'version' => $version, ));

// JQuery selector that upload response should load into. If parsed, it is assumed you have created this element in your own HTML
$render_response_target = ! isset($response_target);
$response_target = isset($response_target) ? $response_target : '#' . $id . ' .fileupload__files';

// URL to load via AJAX, response content is loaded into $response_target
$upload_list_url = isset($upload_list_url) ? $upload_list_url : URL::route('calf.upload.list', array('fieldname' => $fieldname, 'version' => $version, ));

// Elements to show when upload is completed
$show_done = isset($show_done) && $show_done ? 'data-show-done="'.$show_done.'"' : '';
?>

<fieldset name="upload">


    {{--
    File upload legend
    --}}

    @if(! isset($legend) || $legend != false)
        <legend>{{ $legend or 'Upload new file' }}</legend>
    @endif


	<div class="form-group">
		<div class="fileupload" id="{{ $id }}">


            {{--
            File upload help
            --}}

            <div class="fileupload__options">
    			<div class="btn {{ $btn_class or 'btn-primary' }}  fileinput-button">

    				<label for="{{ $id }}-control" class="control-label">
    					{{ glyphicon(isset($label_icon) ? $label_icon : 'plus') }} {{ $label or 'Select file...' }}
    				</label>

    				<input type="file" name="{{ $fieldname }}" id="{{ $id }}-control" class="form-control" 
                        {{ $multiple }}
                        data-type="upload"
                        data-url="{{ $upload_url }}"
                        data-max-size="{{ Config::get('calf::upload_max_filesize') }}"
                        data-chunk-size="{{ Config::get('calf::upload_chunk_size') }}"
                        data-allowed-extensions="{{ implode('|', $allowed_extensions) }}"
                        data-progress-target="#{{ $id }} .fileupload__progress"
                        data-response-target="{{ $response_target }}"
                        data-response-url="{{ $upload_list_url }}"
                        data-response-type="html"
                        {{ $show_done }}
    				/>

    			</div>
            </div>





            {{--
            File upload help
            --}}

			<div class="help-block  fileupload__help">
				<p class="text-muted">
                    <strong>Files are uploaded automatically.</strong> Maximum file size for uploads is 
                    {{ Config::get('calf::upload_max_filesize') / 1024 / 1024 }}MB. <br>
                    Only {{ implode(', ', $allowed_extensions) }} file extensions are allowed.
					{{ $upload_help or '' }}
				</p>
			</div>





            {{--
            File upload progress
            --}}

            <div class="progress hidden  fileupload__progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>





            {{--
            File upload response target - file listing
            --}}

			@if($render_response_target)
                <div class="{{ str_replace('#'.$id.' .', '', $response_target) }}"></div>
			@endif





		</div>
	</div>

</fieldset>
