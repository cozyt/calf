<fieldset name="meta">
    <div class="panel panel-default">

        @if(isset($action) && $action == 'edit')
            <div class="pull-right">
                @include('calf::regions.records.published-status-label', array('status' => isset($row->is_published) && $row->is_published, 'publishDate' => isset($row->published_at) ? $row->published_at : '0000-00-00 00:00:00'))
            </div>
        @endif

        <div class="panel-heading">
            <h3 class="panel-title">{{ $legend or 'Meta' }}</h3>
        </div>

        <div class="panel-body">
            @if(in_array('tags', $fields))
                @include('calf::regions.forms.fields.tags', array('value' => isset($row->tags) ? $row->tags : '' ))
            @endif

            @if(in_array('meta', $fields))
                @include('calf::regions.forms.fields.meta', array('value' => isset($row->meta) ? $row->meta : '', 'label' => 'Description'))
            @endif

            @if(in_array('slug', $fields))
                @include('calf::regions.forms.fields.slug', array('value' => isset($row->slug) ? $row->slug : ''))
            @endif

            @if(in_array('priority', $fields))
                @include('calf::regions.forms.fields.priority', array('value' => isset($row->priority) ? $row->priority : ''))
            @endif

            @if(in_array('revision', $fields))
                @include('calf::regions.forms.fields.revision', array('value' => isset($row->revision) ? $row->revision : ''))
            @endif

            @yield('meta_fields')

            @include('calf::regions.forms.fields.publish', array('value' => isset($row->is_published) ? $row->is_published : 0, 'date_value' => isset($row->published_at) ? $row->published_at : '' ))
        </div>

    </div>
</fieldset>
