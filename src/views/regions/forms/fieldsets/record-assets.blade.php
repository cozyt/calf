{{-----------------------------------------------------------------------------
 * Record assets
 * Generates a fieldset with options for managing assets for the current record.
 * Combines child fieldset and record regions.
 *
 * @path regions.forms.fieldset
 * @return string
-----------------------------------------------------------------------------}}

<?php 
$hasFiles 	= isset($row->files) && count($row->files);
$hasAssets 	= isset($row->assets) && count($row->assets);
$multiple 	= isset($multiple) && $multiple === true ? true : false;
$fieldname 	= isset($fieldname) ? $fieldname : 'files' . ($multiple ? '[]' : '');
?>


<fieldset name="record-assets" class="record-assets">
	<legend>Files</legend>

	<ul class="nav nav-tabs  record-assets__nav" role="tablist">
		@if( isset($row->files) && count($row->files) )
			<li class="active"><a href="#record-assets__current-assets" role="tab" data-toggle="tab">Current file{{ $multiple ? 's' : '' }}</a></li>
		@endif

		<li class="{{ $hasFiles ? '' : 'active' }}"><a href="#record-assets__upload" role="tab" data-toggle="tab">Upload file{{ $multiple ? 's' : '' }}</a></li>

		<li><a href="#record-assets__asset-library" role="tab"
			 data-toggle="tab"
			 data-type="asset-library"
			 data-url="{{ URL::route('calf.asset-library', ['type' => $type]) }}"
			 data-response-target="#record-assets__asset-library__target"
		>File library</a></li>
	</ul>

	<div class="tab-content  record-assets__content">

		{{---------------------------------------------------------------------
		 * Currently assigned assets
		---------------------------------------------------------------------}}

		@if( $hasFiles )
			<div class="tab-pane  active" id="record-assets__current-assets">
				<p class="lead">File{{ $multiple ? 's' : '' }} currently assigned to this record</p>
				@include('calf::regions.forms.fieldsets.current-asset', array('row' => $row, 'legend' => false, 'fieldname' => $fieldname, 'multiple' => $multiple))
			</div>
		@endif


		{{---------------------------------------------------------------------
		 * Upload new asset
		---------------------------------------------------------------------}}

		<div class="tab-pane {{ $hasFiles ? '' : 'active' }}" id="record-assets__upload">
			<p class="lead">Upload new file{{ $multiple ? 's' : '' }} to this record</p>
			@include('calf::regions.forms.fieldsets.upload', array('type' => $type, 'fieldname' => $fieldname, 'multiple' => $multiple, 'legend' => false))
		</div>


		{{---------------------------------------------------------------------
		 * Assign from asset library
		---------------------------------------------------------------------}}

		<div class="tab-pane" id="record-assets__asset-library">
			<p class="lead">Assign file{{ $multiple ? 's' : '' }} from the file library to this record</p>

			<div class="progress">
			  <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar"
			  	 aria-valuenow="40"
			  	 aria-valuemin="0"
			  	 aria-valuemax="100"
			  style="width: 100%">
			    Loading...
			  </div>
			</div>

			<div id="record-assets__asset-library__target"></div>

			{{-- This is rendered from POST data only --}}
			@if( $hasAssets )
				@foreach($row->assets as $asset)
					<input type="hidden" name="assets[]" value="{{ $asset }}" readonly/>
				@endforeach
			@endif

		</div>
	</div>
</fieldset>
