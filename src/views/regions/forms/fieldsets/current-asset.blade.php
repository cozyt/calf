{{-----------------------------------------------------------------------------
 * Current assets
 * Renders a fieldset of currently assigned assets to a given fieldset
 *
 * @path regions.forms.fieldsets
 * @return string
-----------------------------------------------------------------------------}}

<?php
$fieldname 	= isset($fieldname) ? $fieldname : 'files[]';
$options 	= array('view','edit','unlink');
?>

<fieldset name="current-assets">

	@if(! isset($legend) || $legend != false)
		<legend>{{ $legend or 'Current files' }}</legend>
	@endif

	@if( isset($row->files) )
		<div class="form-group">
			@if( is_string($row->files) )
				@include('calf::regions.records.file-data', array('fieldname' => $fieldname, 'options' => $options, 'file' => array('filename' => $row->files)))
			@elseif( count($row->files) )
				@foreach($row->files as $file)
					@if( is_string($file) )
						@include('calf::regions.records.file-data', array('fieldname' => $fieldname, 'options' => $options, 'file' => array('filename' => $file)))
					@else
						@include('calf::regions.records.file-data', array('fieldname' => $fieldname, 'options' => $options, 'file' => $file))
					@endif
				@endforeach
			@endif
		</div>
	@endif

</fieldset>
