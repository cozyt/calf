@extends('calf::regions.messages.modal')

@section('messages-modal--btn')
<span class="glyphicon glyphicon-search"></span> Filter {{ $title or 'records' }}
@endsection

@section('messages-modal--content')
<div class="clearfix">

	<div class="modal-header">
		<h3><span class="glyphicon glyphicon-search"></span> Filter {{ $title or 'records' }}</h3>
	</div>

	<div class="modal-body">
		<form action="" method="get" role="form" class="form-horizontal form-filter" name="filter" >
			<fieldset name="basic">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="id">Record</label>
					<div class="col-sm-10">
						<input type="search" name="id" id="id" class="form-control" value="{{ $data['id'] or '' }}" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="tags">Keyword/s</label>
					<div class="col-sm-10">
						<input type="search" name="tags" id="tags" class="form-control" value="{{ $data['tags'] or '' }}" />

						<div class="well well-sm help-block">
							<span>Match:&nbsp;</span>
							<label class="radio-inline">
								<input type="radio" name="tags_scope" id="tags_scope" value="phrase" {{ isset($data['tags_scope']) && $data['tags_scope'] == 'phrase' ? 'checked="checked"' : '' }}/>
								<span>as phrase</span>
							</label>
							<label class="radio-inline">
								<input type="radio" name="tags_scope" id="tags_scope" value="contains" {{ isset($data['tags_scope']) && $data['tags_scope'] == 'contains' ? 'checked="checked"' : '' }}/>
								<span>all words</span>
							</label>
							<label class="radio-inline">
								<input type="radio" name="tags_scope" id="tags_scope" value="keywords" {{ isset($data['tags_scope']) && $data['tags_scope'] == 'keywords' ? 'checked="checked"' : '' }}/>
								<span>any word</span>
							</label>
						</div>
					</div>
				</div>

				{{ $basic_fields or '' }}
			</fieldset>

			@if(isset($with_advanced) && $with_advanced === true)
			<fieldset name="advanced-filter" class="collapse">
				<hr />

				{{ $advanced_fields or '' }}

				<div class="form-group">
					<label class="col-sm-2 control-label" for="created_by">Created by</label>
					<div class="col-sm-10">
						@if( CalfHelper::authUserTypeIsAuthorized('contributor', '=') )
						<p class="form-control-static">Me</p>
						@else
						<select name="created_by" class="form-control" >
							<option value="" {{ isset($data['created_by']) && $data['created_by'] == '' ? 'selected="selected"' : '' }}>- select user -</option>
								<optgroup label="Created by">
									<option value="{{ CalfHelper::authUserId() }}" {{ isset($data['created_by']) && $data['created_by'] == CalfHelper::authUserId() ? 'selected="selected"' : '' }}>Me</option>
								</optgroup>
								<optgroup label="Other users">
									@foreach(CalfUser::get() as $user)
									<option value="{{ $user->id }}" {{ isset($data['created_by']) && $data['created_by'] == $user->id ? 'selected="selected"' : '' }}>{{ $user->firstname.' '.$user->surname }}</option>
									@endforeach
								</optgroup>
						</select>
						@endif
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="created_at">Created on</label>
					<div class="col-sm-10">
						<input class="form-control" placeholder="YYYY-MM-DD HH:mm:ss" type="text" data-type="date" name="created_at" id="created_at" value="{{ $data['created_at'] or '' }}" />
						<label class="radio-inline">
							<input type="radio" name="created_at_scope" id="created_at_scope" value="on" {{ isset($data['created_at_scope']) && $data['created_at_scope'] == 'on' ? 'checked="checked"' : '' }}/>
							<span>On</span>
						</label>
						<label class="radio-inline">
							<input type="radio" name="created_at_scope" id="created_at_scope" value="before" {{ isset($data['created_at_scope']) && $data['created_at_scope'] == 'before' ? 'checked="checked"' : '' }}/>
							<span>Before</span>
						</label>
						<label class="radio-inline">
							<input type="radio" name="created_at_scope" id="created_at_scope" value="after" {{ isset($data['created_at_scope']) && $data['created_at_scope'] == 'after' ? 'checked="checked"' : '' }}/>
							<span>After</span>
						</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="updated_by">Updated by</label>
					<div class="col-sm-10">
						<select name="updated_by" class="form-control" >
							<option value="" {{ isset($data['updated_by']) && $data['updated_by'] == '' ? 'selected="selected"' : '' }}>- select user -</option>
								<optgroup label="Updated by">
									<option value="{{ CalfHelper::authUserId() }}" {{ isset($data['updated_by']) && $data['updated_by'] == CalfHelper::authUserId() ? 'selected="selected"' : '' }}>Me</option>
								</optgroup>
								<optgroup label="Other users">
									@foreach(CalfUser::get() as $user)
									<option value="{{ $user->id }}" {{ isset($data['updated_by']) && $data['updated_by'] == $user->id ? 'selected="selected"' : '' }}>{{ $user->firstname.' '.$user->surname }}</option>
									@endforeach
								</optgroup>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="updated_at">Last updated</label>
					<div class="col-sm-10">
						<input class="form-control" placeholder="YYYY-MM-DD HH:mm:ss" type="text" data-type="date" name="updated_at" id="updated_at" value="{{ $data['updated_at'] or '' }}" />
						<label class="radio-inline">
							<input type="radio" name="updated_at_scope" id="updated_at_scope" value="on" {{ isset($data['updated_at_scope']) && $data['updated_at_scope'] == 'on' ? 'checked="checked"' : '' }}/>
							<span>On</span>
						</label>
						<label class="radio-inline">
							<input type="radio" name="updated_at_scope" id="updated_at_scope" value="before" {{ isset($data['updated_at_scope']) && $data['updated_at_scope'] == 'before' ? 'checked="checked"' : '' }}/>
							<span>Before</span>
						</label>
						<label class="radio-inline">
							<input type="radio" name="updated_at_scope" id="updated_at_scope" value="after" {{ isset($data['updated_at_scope']) && $data['updated_at_scope'] == 'after' ? 'checked="checked"' : '' }}/>
							<span>After</span>
						</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="is_published">Published state</label>
					<div class="col-sm-10">
						<select name="is_published" id="is_published" class="form-control" >
							<option value="" {{ isset($data['is_published']) && $data['is_published'] == '' ? 'selected="selected"' : '' }}>All</option>
							<option value="1" {{ isset($data['is_published']) && $data['is_published'] == '1' ? 'selected="selected"' : '' }}>Published only</option>
							<option value="0" {{ isset($data['is_published']) && $data['is_published'] == '0' ? 'selected="selected"' : '' }}>Unpublished only</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="published_by">Published by</label>
					<div class="col-sm-10">
						<select name="published_by" class="form-control" >
							<option value="" {{ isset($data['published_by']) && $data['published_by'] == '' ? 'selected="selected"' : '' }}>- select user -</option>
								<optgroup label="Published by">
									<option value="{{ CalfHelper::authUserId() }}" {{ isset($data['published_by']) && $data['published_by'] == CalfHelper::authUserId() ? 'selected="selected"' : '' }}>Me</option>
								</optgroup>
								<optgroup label="Other users">
									@foreach(CalfUser::get() as $user)
									<option value="{{ $user->id }}" {{ isset($data['published_by']) && $data['published_by'] == $user->id ? 'selected="selected"' : '' }}>{{ $user->firstname.' '.$user->surname }}</option>
									@endforeach
								</optgroup>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="published_at">Published on</label>
					<div class="col-sm-10">
						<input class="form-control" placeholder="YYYY-MM-DD HH:mm:ss" type="text" data-type="date" name="published_at" id="published_at" value="{{ $data['published_at'] or '' }}" />
						<label class="radio-inline">
							<input type="radio" name="published_at_scope" id="published_at_scope" value="on" {{ isset($data['published_at_scope']) && $data['published_at_scope'] == 'on' ? 'checked="checked"' : '' }}/>
							<span>On</span>
						</label>
						<label class="radio-inline">
							<input type="radio" name="published_at_scope" id="published_at_scope" value="before" {{ isset($data['published_at_scope']) && $data['published_at_scope'] == 'before' ? 'checked="checked"' : '' }}/>
							<span>Before</span>
						</label>
						<label class="radio-inline">
							<input type="radio" name="published_at_scope" id="published_at_scope" value="after" {{ isset($data['published_at_scope']) && $data['published_at_scope'] == 'after' ? 'checked="checked"' : '' }}/>
							<span>After</span>
						</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label" for="is_deleted">Soft deleted</label>
					<div class="col-sm-10">
						<select name="is_deleted" id="is_deleted" class="form-control" >
							<option value="" {{ isset($data['is_deleted']) && $data['is_deleted'] == '' ? 'selected="selected"' : '' }}>All</option>
							<option value="1" {{ isset($data['is_deleted']) && $data['is_deleted'] == '1' ? 'selected="selected"' : '' }}>Deleted only</option>
							<option value="0" {{ isset($data['is_deleted']) && $data['is_deleted'] == '0' ? 'selected="selected"' : '' }}>Soft deleted only</option>
						</select>
					</div>
				</div>

			</fieldset>
			@endif

			<hr />

			<fieldset name="form-actions">
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10" >
						<input class="btn btn-primary" type="submit" name="form-submit" id="form-submit" value="{{ isset($data['form-submit']) ? 'Refine' : 'Apply' }} filter" />

						<p class="form-control-static">
							@if(isset($with_advanced) && $with_advanced === true)
							<a href="#advanced-filter" data-toggle="collapse" data-target="fieldset[name=advanced-filter]">Advanced search &nbsp;|&nbsp;</a>
							@endif

							<a href="{{ Request::url() }}">Reset</a>
						</p>
					</div>
				</div>
			</fieldset>

		</form>
	</div>

</div>
@endsection