<div class="form-group">
    <label for="{{ $name or 'revision' }}" class="control-label">{{ $label or 'Revision' }}</label>
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <input type="number" name="{{ $name or 'revision' }}" id="{{ $name or 'revision' }}" class="form-control" value="{{ $value or 0 }}" />
        </div>
    </div>
</div>