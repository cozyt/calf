<?php
echo View::make('calf::regions.forms.fields.textarea', array(
    'name'          => isset($name) ? $name : 'meta',
    'label'         => isset($label) ? $label : 'Description',
    'value'         => isset($value) ? $value : '',
    'rowHeight'     => isset($rowHeight) ? $rowHeight : 5,
    'group_class'   => isset($group_class) ? $group_class : '',
    'group_attr'    => isset($group_attr) ? $group_attr : '',
    'label_class'   => isset($label_class) ? $label_class : '',
    'label_attr'    => isset($label_attr) ? $label_attr : '',
    'control_class' => isset($control_class) ? $control_class : '',
    'control_attr'  => isset($control_attr) ? $control_attr : '',
));
?>