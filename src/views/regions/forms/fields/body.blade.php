<?php
echo View::make('calf::regions.forms.fields.textarea', array(
    'name'          => isset($name) ? $name : 'body',
    'label'         => isset($label) ? $label : 'Body',
    'value'         => isset($value) ? $value : '',
    'row_height'    => isset($row_height) ? $row_height : (isset($rowHeight) ? $rowHeight : 15),
    'group_class'   => isset($group_class) ? $group_class : '',
    'group_attr'    => isset($group_attr) ? $group_attr : '',
    'label_class'   => isset($label_class) ? $label_class : '',
    'label_attr'    => isset($label_attr) ? $label_attr : '',
    'control_class' => isset($control_class) ? $control_class : '',
    'control_attr'  => isset($control_attr) ? $control_attr : '',
));
