<?php
echo View::make('calf::regions.forms.fields.text', array(
    'name'          => isset($name) ? $name : 'lead',
    'type'          => 'text',
    'label'         => isset($label) ? $label : 'Lead',
    'value'         => isset($value) ? $value : '',
    'group_class'   => isset($group_class) ? $group_class : '',
    'group_attr'    => isset($group_attr) ? $group_attr : '',
    'label_class'   => isset($label_class) ? $label_class : '',
    'label_attr'    => isset($label_attr) ? $label_attr : '',
    'control_class' => isset($control_class) ? $control_class : '',
    'control_attr'  => isset($control_attr) ? $control_attr : '',
));
?>