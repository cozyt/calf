<div class="form-group">
    <label for="type" class="control-label">{{ $label or 'Type' }}</label>

    <select
     name="{{ $name or 'type' }}"
     id="type"
     class="form-control"
     data-change="showOne"
     data-target=".help-block[data-for=type]"
    >
        <option value=""              {{ $value == ''              ? 'selected="selected"' : '' }}>- select layout -</option>
        <option value="contributor"   {{ $value == 'contributor'   ? 'selected="selected"' : '' }}>Contributor</option>
        <option value="editor"        {{ $value == 'editor'        ? 'selected="selected"' : '' }}>Editor</option>
        <option value="administrator" {{ $value == 'administrator' ? 'selected="selected"' : '' }}>Administrator</option>
        <option value="developer"     {{ $value == 'developer'     ? 'selected="selected"' : '' }}>Developer</option>
    </select>

    @if( isset($with_helper_text) && $with_helper_text )
        <span class="help-block {{ $action == 'create' || $value == 'contributor' ? '' : 'hidden' }}"
        data-for="type" data-value="contributor">
            <strong>Contributors</strong> may only add content for review by an editor or administrator and view/edit their own records.
            Contributors can not publish content to the site, delete existing records or perform any administrative tasks within&nbsp;{{ Config::get('calf::call_me') }}.
            Further, any updates to a record will only be stored in a revision and not in the main record.
        </span>
        <span class="help-block {{ $action == 'create' || $value == 'editor' ? '' : 'hidden' }}"
        data-for="type" data-value="editor">
            <strong>Editors</strong> may add, edit and publish content to the site as well as remove existing records.
            Like contributors, they are not capable of performing administrative tasks within {{ Config::get('calf::call_me') }}, such as creating new&nbsp;users.
        </span>
        <span class="help-block {{ $action == 'create' || $value == 'administrator' ? '' : 'hidden' }}"
        data-for="type" data-value="administrator">
            <strong>Administrators</strong> may add and publish content to the site as well as remove existing records.
            Being a higher level account, they may also perform administrative tasks, such as create new users and view administrative content, such as record meta&nbsp;data.
        </span>
        <span class="help-block {{ $action == 'create' || $value == 'developer' ? '' : 'hidden' }}"
        data-for="type" data-value="developer">
            <strong>Developer</strong> accounts should only be created when absolutely necessary.
            They have all the same permissions as an&nbsp;administrator.
        </span>
    @endif

</div>
