{{-- New link modal --}}

<div class="modal fade" id="wysihtml__toolbar__link--{{ $id }}" data-wysihtml5-dialog="createLink" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Insert link</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Link URL</label>
                    <input type="text" value="http://" class="form-control" data-wysihtml5-dialog-field="href" />
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="_blank" data-wysihtml5-dialog-field="target"> Open in new window
                    </label>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-primary" data-dismiss="modal" data-wysihtml5-dialog-action="save">Insert</a>
                <a class="btn btn-sm btn-default" data-dismiss="modal" data-wysihtml5-dialog-action="cancel">Cancel</a>
            </div>
        </div>
    </div>
</div>


{{-- New image modal --}}

<div class="modal fade" id="wysihtml__toolbar__image--{{ $id }}" data-wysihtml5-dialog="insertImage" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Insert image</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" value="http://" class="form-control" data-wysihtml5-dialog-field="src" />
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-primary" data-dismiss="modal" data-wysihtml5-dialog-action="save">Insert</a>
                <a class="btn btn-sm btn-default" data-dismiss="modal" data-wysihtml5-dialog-action="cancel">Cancel</a>
            </div>
        </div>
    </div>
</div>


<ul class="wysihtml__toolbar" id="wysihtml__toolbar--{{ $id }}">

    {{-- Text size --}}

    <li>
        <div class="dropdown">
            <button class="btn btn-sm btn-default" id="wysihtml__toolbar__text-size--{{ $id }}" type="button" title="Text size" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="glyphicon glyphicon-font"></i>
                <span class="hidden-sm hidden-md">&nbsp;Text size&nbsp;</span>
                <span class="caret"></span>
            </button>

            <ul class="dropdown-menu" aria-labelledby="wysihtml__toolbar__text-size--{{ $id }}">
                <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="p">Normal text</a></li>
                <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1">Header 1</a></li>
                <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2">Header 2</a></li>
                <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h3">Header 3</a></li>
                <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h4">Header 4</a></li>
                <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h5">Header 5</a></li>
                <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h6">Header 6</a></li>
            </ul>
        </div>
    </li>


    {{-- Text formatting --}}

    <li>
        <div class="btn-group">
            <a class="btn btn-sm btn-default" title="Bold" data-wysihtml5-command="bold"><i class="glyphicon glyphicon-bold"></i></a>
            <a class="btn btn-sm btn-default" title="Italic" data-wysihtml5-command="italic"><i class="glyphicon glyphicon-italic"></i></a>
            <a class="btn btn-sm btn-default" title="Underline" data-wysihtml5-command="underline"><i class="glyphicon glyphicon-underline"></i></a>
        </div>
    </li>


    {{-- Text block types --}}

    <li>
        <div class="btn-group">
            <a class="btn btn-sm btn-default" title="Unordered list" data-wysihtml5-command="insertUnorderedList"><i class="glyphicon glyphicon-list"></i></a>
            <a class="btn btn-sm btn-default" title="Ordered list" data-wysihtml5-command="insertOrderedList"><i class="glyphicon glyphicon-th-list"></i></a>
        </div>
    </li>


    {{-- Insert elements --}}

    <li>
        <div class="btn-group">
            <a class="btn btn-sm btn-default" title="Create link" data-toggle="modal" data-target="#wysihtml__toolbar__link--{{ $id }}" data-wysihtml5-command="createLink"><i class="glyphicon glyphicon-link"></i></a>

            <a class="btn btn-sm btn-default" title="Insert image" data-toggle="modal" data-target="#wysihtml__toolbar__image--{{ $id }}" data-wysihtml5-command="insertImage"><i class="glyphicon glyphicon-picture"></i></a>
        </div>
    </li>


    {{-- Misc... --}}

    <li>
        <div class="btn-group">
            <a class="btn btn-sm btn-default" title="Undo" data-wysihtml5-command="undo"><i class="glyphicon glyphicon-undo"></i></a>
            <a class="btn btn-sm btn-default" title="Redo" data-wysihtml5-command="redo"><i class="glyphicon glyphicon-redo"></i></a>
        </div>
    </li>


    {{-- View source --}}

    <li>
        <a class="btn btn-xs btn-default" title="View source" data-wysihtml5-action="change_view"><i class="glyphicon glyphicon-console"></i></a>
    </li>
</ul>
