<div class="form-group {{ $group_class or '' }}" {{ $group_attr or '' }}>
    <label for="{{ $name }}" class="control-label {{ $label_class or '' }}" {{ $label_attr or '' }}>{{ $label }}</label>
    <input type="{{ $type or 'text' }}" name="{{ $name }}" id="{{ $name }}" class="form-control {{ $control_class or '' }}" value="{{ $value or '' }}" {{ $control_attr or '' }}/>
</div>
