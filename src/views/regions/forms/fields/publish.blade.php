@if( CalfHelper::authUserTypeIsAuthorized('editor', '>=') )
<div class="form-group">
    @if(! isset($label) || $label !== '')
        <label for="is_published" class="control-label">{{ $label or 'Publish' }}</label>
    @endif

    <div class="input-group">
        <span class="input-group-addon">
            <input type="checkbox" name="is_published" id="is_published" value="1" {{ isset($value) && $value == 1 ? 'checked="checked"' : '' }}/>
        </span>
        <input class="form-control" placeholder="{{ $format or 'YYYY-MM-DD HH:mm:ss' }}"  type="text" data-type="date" name="published_at" id="published_at" value="{{ isset($date_value) && $date_value != '0000-00-00 00:00:00' ? $date_value : '' }}" />
    </div>
</div>
@endif