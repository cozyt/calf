<?php $row_height = isset($row_height) ? $row_height : (isset($rowHeight) ? $rowHeight : 5) ?>

<div class="form-group  {{ isset($control_attr) && strstr($control_attr, 'data-type="html"') ? 'wysihtml' : '' }}  {{ $group_class or '' }}" {{ $group_attr or '' }}>

    @if( isset($label) )
        <label for="{{ $name }}" class="control-label  {{ $label_class or '' }}" {{ $label_attr or '' }}>
            {{ $label }}
        </label>
    @endif


    @if( isset($control_attr) && strstr($control_attr, 'data-type="html"') )
        @include('calf::regions.forms.fields.wysihtml-toolbar', array('id' => $name))    
    @endif

    <textarea name="{{ $name }}" id="{{ $name }}" class="form-control  {{ $control_class or '' }}" rows="{{ $row_height }}" {{ $control_attr or '' }}>{{ $value or '' }}</textarea>

</div>
