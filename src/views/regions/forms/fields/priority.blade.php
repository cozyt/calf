<div class="form-group">
    <label for="{{ $name or 'priority' }}" class="control-label">{{ $label or 'Priority' }}</label>
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <input type="number" name="{{ $name or 'priority' }}" id="{{ $name or 'priority' }}" class="form-control" value="{{ $value or 0 }}" />
        </div>
    </div>
</div>