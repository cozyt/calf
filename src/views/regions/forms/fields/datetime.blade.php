<div class="form-group">
    @if(! isset($label) || $label !== '')
        <label for="{{ $name or 'Date' }}" class="control-label">{{ $label or 'Date' }}</label>
    @endif

    <input class="form-control" placeholder="{{ $format or 'YYYY-MM-DD HH:mm:ss' }}"  type="text" data-type="date" name="{{ $name or 'Date' }}" id="{{ $name or 'Date' }}" value="{{ isset($value) && $value != '0000-00-00 00:00:00' ? $value : '' }}" />
</div>