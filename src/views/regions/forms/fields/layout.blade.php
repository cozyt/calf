<?php
$multiple = isset($multiple) && $multiple;
?>

<div class="form-group">
    <label for="layout" class="control-label">{{ $label or 'Layout' }}{{ $multiple ? 's' : '' }}</label>

    <select
     name="{{ $name or 'layout' }}{{ $multiple ? '[]' : '' }}"
     class="form-control"
     {{ $multiple ? 'multiple="multiple" title="- select layout -"' : '' }}>

        @if(! $multiple)
            <option value="" {{ $value == '' ? 'selected="selected"' : '' }}>
                - select layout -
            </option>
        @endif

        @foreach(CalfLayout::get() as $layout)
            @if($type == NULL || $layout->type == $type)
                <option
                 value="{{ $layout->id }}"
                {{ $value == $layout->id ? 'selected="selected"' : '' }}>
                    {{ $layout->name }}
                </option>
            @endif
        @endforeach
    </select>

</div>
