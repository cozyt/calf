<form action="{{ route('calf.action', array('type' => $permission->type, 'permission' => $permission->id)) }}" method="get" class="form-horizontal" role="form" name="batch">
    {{ $records_list or '' }}

    @if(isset($actions) && $actions)
        <div class="pull-right  records-listing--option">
            @include('calf::regions.btn.action', array('actions' => $actions, 'btn_style' => 'default', 'show_text' => true))
        </div>
    @endif
</form>
