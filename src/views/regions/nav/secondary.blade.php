<nav role="navigation" name="secondary">
	<ul class="nav">
		@foreach($items as $item)
			@if( ! isset($item->auth_type) || CalfHelper::authUserTypeIsAuthorized($item->auth_type, '>=') )
			<li name="{{ $item->id }}" class="{{ $item->id == $active ? 'active' : '' }}">
				<a href="{{ URL::route('calf.listing', array('type' => $parent, 'item' => $item->id)) }}">
					<span class="glyphicon glyphicon-{{ $item->icon }}"></span>
					<span class="hidden-sm">{{ $item->alt_title or $item->title }}</span>
				</a>
			</li>
			@endif
		@endforeach
	</ul>
</nav>
