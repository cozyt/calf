<nav role="navigation" name="tertiary">
    <ul class="nav navbar-nav">
        <li name="logout">
            <a href="{{ URL::route('calf.logout') }}">
            	<span class="glyphicon glyphicon-log-out"></span>
            	<span class="hidden-sm">Logout</span>
            </a>
        </li>
        <li name="account">
            <a href="{{ URL::route('calf.account') }}">
                <span class="glyphicon glyphicon-user"></span>
                <span class="hidden-sm">{{ isset($user) ? $user->firstname.' '.$user->surname : 'Account' }}</span>
            </a>
        </li>
        {{-- <li name="notifications">
            <a href="{{ URL::route('calf.notifications') }}">
                <span class="glyphicon glyphicon-bell"></span>
                <span class="hidden-sm">Notifications</span>
                <span class="badge"></span>
            </a>
        </li> --}}
    </ul>
</nav>
