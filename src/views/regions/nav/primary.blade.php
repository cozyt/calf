<nav role="navigation" name="primary">
    <ul class="nav navbar-nav">
        @if( count($items) )
            @foreach($items as $item)
            	@if( ! isset($item->auth_type) || CalfHelper::authUserTypeIsAuthorized($item->auth_type, '>=') )
                <li name="{{ $item->id }}" class="{{ $active == $item->id ? 'active' : '' }}">
                    <a href="{{ URL::route('calf', array('type' => $item->id)) }}"><span class="glyphicon glyphicon-{{ $item->icon }}"></span> {{ $item->title }}</a>
                </li>
                @endif
            @endforeach
        @endif
    </ul>
</nav>
