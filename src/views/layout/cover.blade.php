@extends('calf::layout.master')
<?php $body_class .= ' cover'; ?>

@section('main')
<div class="main">
	<div class="messages">
		{{ $messages or '' }}
	</div>
	<div class="clearfix main--content">
		<article>
			@yield('article')
		</article>
	</div>
</div>
@endsection