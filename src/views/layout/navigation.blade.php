@if( Auth::check() )
    <nav role="navigation" name="{{ $name or 'navigation' }}">
        <ul>
            @if( isset($urls) )
                @foreach($urls as $route => $data)
                    <li class="{{ Request::is($data['url']) || Request::is($data['url'].'/*') ? 'active' : '' }}">
                        <a href="{{ URL::route($route) }}">{{ ucfirst($data['title']) }}</a>
                    </li>
                @endforeach
            @endif
        </ul>
    </nav>
@endif