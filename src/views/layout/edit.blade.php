@extends('calf::layout.master')

@section('main')
<div class="col-sm-11 col-sm-offset-1 main">

	@if( isset($messages) )
		<div class="messages">
			@if( is_array($messages) )
				@foreach($messages as $m)
					{{ $m }}
				@endforeach
			@else
				{{ $messages }}
			@endif
		</div>
	@endif

	<div class="clearfix main--content">
		<article>
			<div class="col-sm-12">
				<div class="page-header">
					<h1>@yield('title', ucfirst($action).' '.strtolower($page_header))</h1>
				</div>
			</div>

			<div class="col-sm-12">
				<p class="lead">@yield('lead')</p>
			</div>

			@include('calf::regions.forms.edit', array('action' => $action, 'row' => $row, 'permission' => $permission))
		</article>

		<aside class="col-sm-12">
			@yield('aside')
		</aside>
	</div>
</div>
@endsection