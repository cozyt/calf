@extends('calf::layout.master')
<?php $body_class .= ' double'; ?>

@section('main')
<div class="col-sm-11 col-sm-offset-1 main">
	<div class="messages">
		{{ $messages or '' }}
	</div>

	<div class="clearfix main--content">
		<article class="col-sm-8 col-md-6">
			@yield('article')
		</article>

		<aside class="col-sm-4 col-md-6">
			@yield('aside')
		</aside>
	</div>

</div>
@endsection