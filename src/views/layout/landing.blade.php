@extends('calf::layout.master')
<?php $body_class .= ' landing'; ?>

@section('main')
<div class="col-sm-11 col-sm-offset-1 main">
	<div class="messages">
		{{ $messages or '' }}
	</div>

	<div class="clearfix">
		<article>
			<div class="jumbotron">
				@yield('jumbotron')
			</div>

			<div class="main--content">
				<div class="col-sm-12">
					@yield('article')
				</div>
			</div>
		</article>
	</div>
</div>
@endsection