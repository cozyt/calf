@extends('calf::layout.single')
<?php $body_class .= ' records-listing'; ?>

@section('article')

@if(isset($page_header))
	<div class="page-header">
		@if(isset($permission->buttons['create']))
		<div class="pull-right records-listing--option">
			<a href="{{ URL::route('calf.create', array('type' => $permission->type, 'permission' => $permission->id)) }}" name="create" class="btn btn-primary">
				{{ $permission->buttons['create'] or 'Create new record' }}
			</a>
		</div>
		@endif

		<div class="pull-right records-listing--option">
			{{ $forms_filter or '' }}
		</div>

		<h1>{{ $page_header }}</h1>
	</div>
@endif

<div class="records-listing--option">
	{{ $pagination or '' }}
</div>

{{ $records_count or '' }}

{{ $records_list or '' }}

@endsection