<!DOCTYPE html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $calf }}</title>

    @foreach(Config::get('calf::assets.css') as $css)
    <link href="{{ Str::startsWith($css, 'http') ? $css : Config::get('calf::assets_url') . $css }}" rel="stylesheet">
    @endforeach

    <script src="{{ Config::get('calf::assets_url') }}/scripts/modernizr-2.6.2.min.js"></script>
  </head>

  <body class="{{ $body_class or '' }}">
    <!--[if lt IE 9]><p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->

    <header class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="row">
          <div class="col-sm-1 navbar-header">
            @if( Auth::check() )
            <a href="#" class="pull-right navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            @endif
            <a class="navbar-brand" href="{{ URL::route('calf.root') }}">
              <span class="glyphicon glyphicon-home visible-sm"></span>
              <span class="hidden-sm"> {{ $calf }}</span>
            </a>
          </div>

          @if( Auth::check() )
          <div class="navbar-collapse collapse">
            <div class="col-sm-6">
                @include('calf::regions.nav.primary', array('items' => $permission_types, 'active' => $permission_type_slug))
            </div>
            <div class="col-sm-5">
              @include('calf::regions.nav.tertiary')
            </div>
          </div>
          @endif
        </div>
    </header>

    <div class="container-fluid">
      <div class="row">
        @if( Auth::check() )
        <div class="col-sm-1 sidebar">
          {{ $secondary_nav or '' }}
        </div>
        @endif

        @yield('main')
      </div>

      <div class="row">
        <footer class="col-sm-12">
        </footer>
      </div>
    </div>

    @foreach(Config::get('calf::assets.js') as $js)
    <script src="{{ Str::startsWith($js, 'http') ? $js : Config::get('calf::assets_url') . $js }}"></script>
    @endforeach

    @yield('foot')
  </body>
</html>