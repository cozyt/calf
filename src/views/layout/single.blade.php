@extends('calf::layout.master')
<?php $body_class .= ' single'; ?>

@section('main')
<div class="col-sm-11 col-sm-offset-1 main">
	<div class="messages">
		{{ $messages or '' }}
	</div>

	<div class="clearfix main--content">
		<article class="col-sm-12">
			@yield('article')
		</article>

		<aside class="col-sm-12">
			@yield('aside')
		</aside>
	</div>

</div>
@endsection