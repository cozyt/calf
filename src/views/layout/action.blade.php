@extends('calf::layout.single')
<?php $body_class .= ' action'; ?>

@section('article')

@if(isset($page_header))
    <div class="page-header">

        <div class="pull-right records-listing--option">
            <a href="{{ URL::route('calf.listing', array('type' => $permission->type, 'permission' => $permission->id)) }}" name="show" class="btn btn-default">
                {{ glyphicon('back') }}
                Return to {{ Str::plural(strtolower($permission->title)) }}
            </a>
        </div>

    	<div class="pull-right records-listing--option">
    		{{ $forms_filter or '' }}
    	</div>

    	<h1>{{ $page_header }}</h1>
    </div>
@endif

{{ $records_count or '' }}

{{ $records_list or '' }}

@endsection