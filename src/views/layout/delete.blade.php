@extends('calf::layout.single')

@section('article')

	<div class="page-header">
		<h1>Delete {{ $permission->id }}</h1>
	</div>

	@if($is_deleted)

		<p>
			<a href="{{ URL::route('calf.listing', array('type' => $permission->type, 'permission' => $permission->id)) }}" class="btn btn-primary">Return to listing</a>
		</p>

	@else

		<p class="lead">Are you sure you want to delete this {{ $permission->id }}?</p>

		<form action="" method="post">
			<input type="hidden" name="id" value="{{ $row['id'] }}" />

			<div class="form-group">
				<input type="submit" name="form-submit" id="form-submit" class="btn btn-primary" value="Yes, delete {{ $permission->id }}" />
				<a href="{{ URL::route('calf.listing', array('type' => $permission->type, 'permission' => $permission->id)) }}" class="btn btn-default">No, return to listing</a>
			</div>
		</form>

	@endif

@endsection