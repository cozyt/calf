<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<h1>{{ Config::get('app.url') }}</h1>

		<p>A record has been created and requires your attention.</p>

		<h2>#{{ $record->id }}: {{ $record->title or '' }}</h2>
		<p>Created by user ID: {{ $record->created_by or '' }}</p>

		<p>{{ $record->body or '' }}</p>

		<h3>Options</h3>

		<ul>
			<li><a href="{{ URL::route('calf.edit', array($permission->type, $permission->id, $record->id)) }}" target="_blank">View</a></li>
			<li><a href="{{ URL::route('calf.edit', array($permission->type, $permission->id, $record->id)) }}" target="_blank">Publish</a></li>
			<li><a href="{{ URL::route('calf.delete', array($permission->type, $permission->id, $record->id)) }}" target="_blank">Delete</a></li>
		</ul>

		<hr>

		<p>Powered by Calf</p>
	</body>
</html>