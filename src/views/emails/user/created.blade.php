<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<h1>User account created</h1>

		<p>You have been registered to use {{ Config::get('calf::call_me') }} on {{ Config::get('app.url') }}.</p>
		<p>Please visit {{ URL::route('calf.login') }} and login to {{ Config::get('calf::call_me') }} using the credentials below.</p>

		<p>
			<strong>Username:</strong>&nbsp;{{ $record['email'] }}<br>
			<strong>Password:</strong>&nbsp;{{ $record['password'] }}
		</p>

		<hr>

		<p>Powered by Calf</p>
	</body>
</html>