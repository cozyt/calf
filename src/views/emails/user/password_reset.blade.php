<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password reset</h2>

		<div>
			To reset your password, complete this form: {{ URL::route('calf.reset.token', array($token)) }}
		</div>
	</body>
</html>