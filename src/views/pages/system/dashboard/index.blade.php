@extends('calf::layout.landing')
<?php $body_class .= ' dashboard'; ?>

@section('jumbotron')
	<h1>
		<span class="glyphicon glyphicon-{{ $permission_type->icon }}"></span> Welcome to {{ $calf == 'Calf' ? $calf : 'your CMS' }}, {{ Auth::user()->firstname }}.
	</h1>
	<p>Your dashboard is the central hub for all activity on your site.</p>
@endsection

@section('article')
	<div class="content-review">
		@foreach($content_review as $key => $content)

			{{-----------------------------------------------------------------
			 * Content status
			-----------------------------------------------------------------}}
			<div class="col-sm-5 col-md-3">
				<div class="panel panel-default content-review--stats">
					<div class="panel-heading"  data-toggle="collapse" data-target=".content-review--stats .panel-body">
						<h3 class="panel-title">Content status</h3>
					</div>
					<div class="content-review--chart" id="content-review--{{ Str::slug($key) }}-stats-chart"></div>
					<div class="panel-body">
						<h1>{{ $content['count'] or 0 }} records</h1>
						@if($key == 'user')
						<small class="text-muted">Of {{ $total_records }} total records / {{ round($content['count'] / $total_records * 100, 1) }}% of all site content</small>
						@endif
					</div>

					@if( $content['count'] )
					<table class="table">
						@foreach($content['status'] as $status => $count)
						<tr>
							<th>@include('calf::regions.records.published-status-label', array('status' => $status))</th>
							<td align="right">{{ $count }}</td>
							<td align="right">{{ round($count / $content['count'] * 100, 1) }}%</td>
						</tr>
						@endforeach
					</table>
					<div class="panel-footer">
						{{ $create_btn or '' }}
					</div>
					@else
					<div class="panel-body">
						<p>
							{{ $key == 'user' ? 'You have not created any content&nbsp;yet.' : 'No content has been created&nbsp;yet...' }}
						</p>
						{{ $create_btn or '' }}
					</div>
					@endif

				</div>
			</div>


			{{-----------------------------------------------------------------
			 * Latest activity
			-----------------------------------------------------------------}}
			<div class="col-sm-7 col-md-5">
				<div class="panel panel-default content-review--last-post">
					<div class="panel-heading"  data-toggle="collapse" data-target=".content-review--last-post .panel-body">
						<h3 class="panel-title">Latest activity</h3>
					</div>
					<div class="content-review--chart" id="content-review--{{ Str::slug($key) }}-activity-chart"></div>
					@if( isset($content['records']) && count($content['records']) )
					<table class="table">
						@foreach($content['records'] as $k => $row)
						<tr>
							@if($k == 0)
							<td colspan="4">
								<div class="page-header">
									<div class="pull-right">
										<div>@include('calf::regions.records.published-status-label', array('status' => $row->is_published, 'publishDate' => $row->published_at))</div>
										<nav role="navigation" class="btn-group">
											@include('calf::regions.btn.record-option', array('btn_style' => 'sm', 'option' => 'edit', 'type' => 'content', 'permission' => $row->type, 'row' => $row))
											@include('calf::regions.btn.record-option', array('btn_style' => 'sm', 'option' => 'delete', 'type' => 'content', 'permission' => $row->type, 'row' => $row))
										</nav>
									</div>
									<h1>{{ $row->title }}</h1>
								</div>
								<table class="table">
									<tr>
										<th>Content type</th>
										<td>{{ CalfPermission::find($row->type)->alt_title }}</td>
									</tr>
									<tr>
										<th>Date created</th>
										<td>{{ $row->created_at }}</td>
									</tr>

									@if( published_status($row->is_published, $row->published_at) == 'queued' )
									<tr>
										<th>Publish at</th>
										<td>{{ $row->published_at }}</td>
									</tr>
									@endif

									@if( published_status($row->is_published, $row->published_at) == 'published' )
									<tr>
										<th>Date published</th>
										<td>{{ $row->published_at }}</td>
									</tr>
									@endif

								</table>
							</td>
							@else
							<td>{{ CalfPermission::find($row->type)->alt_title }}</td>
							<td>{{ $row->title }}</td>
							<td>@include('calf::regions.records.published-status-label', array('status' => $row->is_published, 'publishDate' => $row->published_at))</td>
							<td style="width: 90px">
								<nav role="navigation" class="btn-group pull-right">
									@include('calf::regions.btn.record-option', array('btn_style' => 'sm', 'option' => 'edit', 'type' => 'content', 'permission' => $row->type, 'row' => $row))
									@include('calf::regions.btn.record-option', array('btn_style' => 'sm', 'option' => 'delete', 'type' => 'content', 'permission' => $row->type, 'row' => $row))
								</nav>
							</td>
							@endif
						</tr>
						@endforeach
					</table>
					@else
					<div class="panel-body">
						<?php /*<p>
							{{ $key == 'user' ? 'You\'ve not created anything recently.' : 'No content has been created&nbsp;recently...' }}
						</p>
						*/ ?>
						{{ $create_btn or '' }}
					</div>
					@endif
				</div>
			</div>
		@endforeach


		{{-----------------------------------------------------------------
		 * Popular tags
		-----------------------------------------------------------------}}
		<div class="col-sm-5 col-md-4">
			<div class="panel panel-default content-review--tags">
				<div class="panel-heading"  data-toggle="collapse" data-target=".content-review--tags .panel-body">
					<h3 class="panel-title">Popular keyword tags</h3>
				</div>
				<div class="content-review--chart" id="content-review--tags-chart"></div>
				@if( isset($tags['popular']) && count($tags['popular']) )
				<div class="panel-body">
					<h1>{{ $tags['distinct'] or 0 }} / {{ $tags['count'] or 0 }}</h1>
					<small class="text-muted">Unique / Total</small>
				</div>
				<table class="table table-striped">
					<tr>
						<th>Tag</th>
						<th>Count</th>
						<th>% Tags</th>
						<th>% Records</th>
					</tr>
					@foreach($tags['popular'] as $tag => $count)
					<tr>
						<td>{{ $tag }}</td>
						<td>{{ $count }}</td>
						<td>{{ round($count / $tags['count'] * 100, 1) }}%</td>
						<td>{{ round($count / $total_records * 100, 1) }}%</td>
					</tr>
					@endforeach
				</table>
				@endif
			</div>
		</div>
	</div>
@endsection

@section('foot')
	<script type="text/javascript">
	calf.apis.google.charts.init(function(){

		@foreach($content_review as $key => $content)
			@if( $content['count'] )
			calf.apis.google.charts
			.draw('pie', 'content-review--{{ Str::slug($key) }}-stats-chart', [
					['Status', 'Records']
					@foreach($content['status'] as $type => $count)
					,['{{ ucfirst($type) }}', {{ $count }}]
					@endforeach
				], {
					width: '100%',
					height: 250,
					chartArea : { top: 20, height: '100%'},
					legend: { position: 'none' },
					colors : [calf.ui.color.success, calf.ui.color.warning, calf.ui.color.danger, calf.ui.color.default],
					pieSliceText: 'none'
			})
				@if( isset($content['activity']) )
				.draw('line', 'content-review--{{ Str::slug($key) }}-activity-chart', [
					['Date', 'Created', 'Updated', 'Published']
					@foreach($content['activity'] as $date => $type)
					,['{{ $date }}'
						@foreach($type as $type_count)
						, {{ $type_count }}
						@endforeach
					]
					@endforeach
				], {
					width: '100%',
					height: 250,
					chartArea: { left: 40, width: '100%' },
					legend: { position: 'top', alignment: 'center' },
					colors : [calf.ui.color.primary, calf.ui.color.danger, calf.ui.color.success, calf.ui.color.warning],
					vAxis: { gridlines: { count: 3} }
				});
				@endif
			@endif
		@endforeach

		@if( isset($content['tags']) )
		calf.apis.google.charts.draw('bar', 'content-review--tags-chart', [
			['Tags', 'Count', { role: 'style'}]
			@foreach($tags['popular'] as $tag => $count)
			,['{{ $tag }}', {{ $count }}, 'color: '+calf.ui.color.primary]
			@endforeach
		], {
			width: '100%',
			height: 250,
			chartArea: { top: 20, height: '100%' },
			legend: { position: 'none' },
			bar: { groupWidth: '90%' },
			hAxis: { baselineColor: '#cccccc', gridlines: {color: '#cccccc' }}
		});
		@endif
	});
	</script>
@endsection