@extends('calf::layout.landing')

@section('jumbotron')
	<h1>
		<span class="glyphicon glyphicon-{{ $permission->icon }}"></span> What's it all about?
	</h1>
	<p>Find out more about Calf and how to use it in your application.</p>
@endsection

@section('article')
	<div class="col-sm-3">
		<nav role="navigation">
			<div class="nav list-group">
				@foreach($readme_headers as $slug => $header)
					<a href="#{{ $header }}" class="list-group-item">{{ $header }}</a>
				@endforeach
			</div>
		</nav>
	</div>
	<div class="col-sm-8">
		{{ $readme or '' }}
		<br>
		<p class="text-muted">Last modified: {{ $readme_date_modified }}.</p>
	</div>
@endsection