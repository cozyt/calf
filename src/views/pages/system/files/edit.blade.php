<?php
$fileName = isset($row->filename) ? $row->filename : '';
$hasSubmittedFiles = Input::has('form-submit') && isset($row->files);
$fileName = $hasSubmittedFiles ? $row->files[1] : $fileName;
$fileType = isset($row->type) ? $row->type : '';
?>

@extends('calf::layout.edit')

@section('lead')
	{{ $action == 'edit' ? 'Edit existing file in the file library for use in '.$calf : 'Upload a new file to the file libray for later use in '.$calf }}
@endsection

@section('form')

    {{--
    Display the main file and its data where no file exists, display just an upload field
    --}}

    @if($fileName)
        <fieldset name="asset">
            <legend>Main file</legend>

            <div class="form-group">

                <?php
                // Could use blades @include but this just makes it easier to read
                echo View::make('calf::regions.records.file-data', array(
                    'fieldname'     => 'files[]',
                    'file'          => array(
                        'id'        => isset($row->id) ? $row->id : 0,
                        'type'      => $fileType,
                        'filename'  => $fileName,
                        'title'     => isset($row->title) ? $row->title : '',
                    ),
                    'options'       => array('replace'),
                    'version'       => 'original',
                    'replace_type'  => raw_asset_types(),
                ));
                ?>

            </div>
        </fieldset>

    @else

        @include('calf::regions.forms.fieldsets.upload', array('type' => raw_asset_types(), 'fieldname' => 'files[]', 'legend' => 'Upload file', 'label' => 'Select new file...', 'version' => 'original', 'btn_class' => 'btn-primary btn-sm'))

    @endif





    {{--
    Display various image versions with its data
    --}}

    @if( $fileName && $fileType == 'image' && count(CalfFile::imageVersions()) )
        <fieldset name="file-versions">
            <legend>Alternate versions</legend>

            @foreach(CalfFile::imageVersions() as $version)

                <!-- {{ $version }} version -->
                <?php
                $versionFieldName = 'files_' . $version;

                // Could use blades @include but this just makes it easier to read
                echo View::make('calf::regions.records.file-data', array(
                    'fieldname'     => $versionFieldName . '[]',
                    'file'          => array(
                        'id'        => $row->id,
                        'type'      => 'image',
                        'filename'  => $hasSubmittedFiles ? $row->{$versionFieldName}[1] : $fileName,
                        'title'     => ucfirst($version) . ' version'
                    ),
                    'options'       => array('replace'),
                    'version'       => $version,
                    'replace_type'  => raw_asset_types(),
                ));
                ?>

            @endforeach

        </fieldset>
    @endif

@endsection

@section('meta_fields')
    @include('calf::regions.forms.fields.title', array('value' => isset($row->title) ? $row->title : ''))
    @include('calf::regions.forms.fields.text', array('name' => 'type', 'label' => 'File type', 'value' => $fileType, 'control_attr' => 'readonly'))
    @include('calf::regions.forms.fields.title', array('name' => 'alt', 'label' => 'Alt. Text', 'value' => isset($row->alt) ? $row->alt : ''))
    @include('calf::regions.forms.fields.textarea', array('name' => 'caption', 'label' => 'Caption', 'value' => isset($row->caption) ? $row->caption : '', 'rowHeight' => 2))
@endsection
