@extends('calf::layout.edit')

@section('title')
Account management
@endsection

@section('lead')
	@if( $action == 'account' )
		Edit your {{ $calf }} account details
	@elseif( $action == 'edit' )
		Edit existing {{ $calf }} user
	@else
		Register a new {{ $calf }} user
	@endif
@endsection

@section('form')
	<input type="hidden" name="id" value="{{ $row->id or 0 }}" />

	<fieldset>
		<legend>Account details</legend>

		@if($action != 'account' && CalfHelper::authUserTypeIsAuthorized($permission->auth_type, '>=') )
			@include('calf::regions.forms.fields.user_type', array('value' => isset($row->type) ? $row->type : '', 'with_helper_text' => true))
		@else
			<input type="hidden" name="type" value="{{ $row->type or 'contributor' }}" />
		@endif

		<div class="form-group">
			<label for="username" class="control-label">Email</label>
			<input type="email" name="username" id="username" class="form-control input-lg" value="{{ $row->username or '' }}" autocomplete="off" />
			<input type="password" name="prevent_autofill" id="prevent_autofill" value="" style="display:none"/>
		</div>

		<div class="form-group">
			<label for="new_password" class="control-label">Password</label>
			<input type="password" name="new_password" id="new_password" class="form-control" value="" autocomplete="off" />
		</div>

		<div class="form-group">
			<label for="new_password_confirmation" class="control-label">Confirm password</label>
			<input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control" value="" autocomplete="off" />
		</div>
	</fieldset>

	<fieldset>
		<legend>Personal details</legend>

		<div class="form-group">
			<label for="firstname" class="control-label">First name</label>
			<input type="text" name="firstname" id="firstname" class="form-control" value="{{ $row->firstname or '' }}" />
		</div>

		<div class="form-group">
			<label for="surname" class="control-label">Surname</label>
			<input type="text" name="surname" id="surname" class="form-control" value="{{ $row->surname or '' }}" />
		</div>
	</fieldset>

@endsection