@extends('calf::layout.single')
<?php $body_class = 'system layouts create'; ?>

@section('article')

<div class="page-header">
	<h1>Create a new layout</h1>
</div>

<p class="lead">How to generate a new layouts for use in Calf</p>

<h2>Step-by-step</h2>

<ol>
	<li>Create a new view file located in app/views</li>
	<li>Register the layout in the calf.php config file under the 'layouts' key with a unique name.</li>
	<li>You should define at least 2 values for your layout:
		<ol>
			<li>name - A human readable name for the layout file</li>
			<li>path - The path to the view file as created in step 1 (should be written as called using the Laravel View::make() method).</li>
		</ol>
	</li>
</ol>

<h2>Typical example</h2>
<code>'default' => array('name' => 'Default', 'path' => 'layouts.default'),</code>


@endsection