@extends('calf::layout.landing')

@section('jumbotron')
	<h1>
		<span class="glyphicon glyphicon-{{ $permission_type->icon }}"></span> Keep it running...
	</h1>
	<p>Manage system based features of {{ $calf == 'Calf' ? $calf : 'your CMS' }}.</p>
@endsection

@section('article')
	<p class="lead">
		Maintenance and admin are never fun but are necessary parts of {{ $calf == 'Calf' ? $calf : 'all content management systems' }}. <br>
		Tasks, such as creating new users and uploading files exclusively to the file library, can be performed here.
	</p>

	{{ $create_btn or '' }}

@endsection