@extends('calf::layout.cover')

@section('main')
<div class="main">
	<div class="jumbotron">
		@if(Config::get('calf::tone_of_voice') == 'friendly')
			<h1>Uh-Oh!</h1>
			<p>You can't use that bit...</p>
		@else
			<h1><span class="glyphicon glyphicon-ban-circle"></span>  Unauthorized</h1>
			<p>You don't have permission to access that part of {{ $calf == 'Calf' ? $calf : 'the CMS' }}.</p>
		@endif
	</div>
</div>
@endsection