@extends('calf::layout.cover')

@section('main')
<div class="main">
	<div class="jumbotron">
		@if(Config::get('calf::tone_of_voice') == 'friendly')
			<h1>Erm,</h1>
			<p>I couldn't find that one...</p>
		@else
			<h1><span class="glyphicon glyphicon-ban-circle"></span>  404: Record not found</h1>
			<p>We could not find the requested record. <br>Either the record does not exist or you don't have permission to access it.</p>
		@endif
	</div>
</div>
@endsection