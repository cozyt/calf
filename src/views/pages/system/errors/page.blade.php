@extends('calf::layout.cover')

@section('main')
<div class="main">
	<div class="jumbotron">
		@if(Config::get('calf::tone_of_voice') == 'friendly')
			<h1>I'm lost...</h1>
			<p>That's not the page you're looking for.</p>
		@else
			<h1><span class="glyphicon glyphicon-ban-circle"></span>  404: Page not found</h1>
			<p>We could not find the requested page. <br>Either the page does not exist or you don't have permission to access it.</p>
		@endif
	</div>
</div>
@endsection