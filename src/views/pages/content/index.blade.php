@extends('calf::layout.landing')

@section('jumbotron')
	<h1>
		<span class="glyphicon glyphicon-{{ $permission_type->icon }}"></span> Get started!
	</h1>
	<p>Create and edit content for use in your application.</p>
@endsection

@section('article')
	<p class="lead">
		What good is {{ $calf == 'Calf' ? $calf : 'a CMS' }} if you can't manage the content in it? <br>
		Use this section to generate new content and manage existing records for your application.
	</p>

	{{ $create_btn or '' }}

@endsection