@extends('calf::layout.cover')

@section('main')
@parent
<div class="jumbotron">
	@if(Config::get('calf::tone_of_voice') == 'friendly')
	<h1>Don't panic! we got this...</h1>
	@else
	<h1><span class="glyphicon glyphicon-lock"></span> Reset password</h1>
	@endif
</div>

<div class="col-sm-6 col-sm-offset-3">
	<center>
		<p class="lead">Enter your email to reset your {{ $calf == 'Calf' ? $calf : 'CMS' }} password.</p>

		<div class="clearfix">
			<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-xl-4 col-xl-offset-4">
				<form action="" method="post" class="form-signin login--form" role="form" name="reset-stage-1">
					<input type="email" name="email" class="form-control" value="{{ Input::get('email') }}" placeholder="E.g. name@domain.com" autofocus>
					<input type="submit" name="form-submit" class="btn btn-primary btn-block" value="Reset password">
				</form>
			</div>
		</div>
	</center>
</div>
@endsection