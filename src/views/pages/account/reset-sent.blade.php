@extends('calf::layout.cover')

@section('main')
@parent
<div class="jumbotron">
	@if(Config::get('calf::tone_of_voice') == 'friendly')
	<h1>...see, we told you.</h1>
	@else
	<h1><span class="glyphicon glyphicon-lock"></span> Reset password</h1>
	@endif
</div>

<div class="col-sm-6 col-sm-offset-3">
	<p class="lead">
		An email has been sent to {{ $email != '' ? $email : 'the email address provided' }},
		containing instructions on how to reset your {{ $calf == 'Calf' ? $calf : 'CMS' }} password.
	</p>
	<p>
		Please follow the instructions sent in the email to reset your password. If you do not receive an email,
		please check your junk/spam folder or <a href="{{ URL::route('calf.reset', array('email' => $email)) }}">resend the email</a>.
	</p>
</div>
@endsection