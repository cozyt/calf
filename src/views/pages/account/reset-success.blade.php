@extends('calf::layout.cover')

@section('main')
@parent
<div class="jumbotron">
	@if(Config::get('calf::tone_of_voice') == 'friendly')
	<h1>Saweeet!</h1>
	@else
	<h1><span class="glyphicon glyphicon-lock"></span> Password reset</h1>
	@endif
</div>

<div class="col-sm-6 col-sm-offset-3">
	<center>
		<p class="lead">
			You have successfully reset the password for your account.
		</p>
		<p>
			<a href="{{ URL::route('calf') }}" class="btn btn-primary">Log in to {{ $calf == 'Calf' ? $calf : ' your CMS' }}</a>
		</p>
	</center>
</div>
@endsection