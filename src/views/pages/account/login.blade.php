@extends('calf::layout.cover')

@section('main')
@parent
<div class="jumbotron">
	<h1><span class="glyphicon glyphicon-log-in"></span> Log in</h1>
</div>

<div class="col-sm-6 col-sm-offset-3">
	<center>
		<p class="hidden-xs hidden-sm lead">Please enter your email and password to access {{ $calf == 'Calf' ? $calf : 'your CMS' }}.</p>

		<div class="clearfix">
			<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-xl-4 col-xl-offset-4">
				<form action="" method="post" class="form-signin login--form" role="form" name="login">
					<input type="email" name="username" class="form-control" value="{{ Input::get('username') }}" placeholder="E.g. name@domain.com" autofocus>
					<input type="password" name="password" class="form-control" value="{{ Input::get('password') }}" placeholder="Password">
					<input type="submit" name="form-submit" class="btn btn-primary btn-block" value="Log in">
				</form>
			</div>
		</div>

		@if($allow_reset)
		<ul class="list-unstyled">
			<li><a href="{{ URL::route('calf.reset') }}">Forgot password?</a></li>
		</ul>
		@endif

	</center>
</div>
@endsection