@extends('calf::layout.cover')

@section('main')
@parent
<div class="jumbotron">
	@if(Config::get('calf::tone_of_voice') == 'friendly')
		@if( isset($messages) )
		<h1>Did we get our wires crossed?</h1>
		@else
		<h1>You're nearly done!</h1>
		@endif
	@else
	<h1><span class="glyphicon glyphicon-lock"></span> Reset password</h1>
	@endif
</div>

<div class="col-sm-6 col-sm-offset-3">
	<center>
		<p class="lead">
			Enter your email address and new password to complete the reset process.
		</p>

		<div class="clearfix">
			<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-xl-4 col-xl-offset-4">
				<form action="" method="post" class="form-signin login--form" role="form" name="reset-stage-2">
					<input type="email" name="email" class="form-control" value="{{ Input::get('email') }}" placeholder="E.g. name@domain.com" autocomplete="off" autofocus>
					<input type="hidden" name="token" value="{{ $token }}" readonly>

					<input type="password" name="prevent_autofill" id="prevent_autofill" value="" style="display:none" readonly/>
					<input type="password" name="password" class="form-control" value="" placeholder="Password" autocomplete="off">
					<input type="password" name="password_confirmation" class="form-control" value="" placeholder="Confirm password" autocomplete="off">

					<input type="submit" name="form-submit" class="btn btn-primary btn-block" value="Reset password">
				</form>
			</div>
		</div>
	</center>
</div>
@endsection