var calf = {

    ready : function() {
        // Set default states
        // $('[data-show-hide].hidden').each(function(){
            // calf.show($(this));
            // calf.hide($(this).data('show-hide'));
        // });

        // $('[data-show-once].hidden').each(function(){
            // calf.show($(this));
            // calf.hide($(this).data('show-once'));
        // });

        // Render UI enhancements
        calf.ui.init($('input[data-type=date]'), 'datepicker');
        calf.ui.init($('input[data-type=upload]'), 'fileupload');
        calf.ui.init($('input[data-type=slider]'), 'slider');
        calf.ui.init($('[data-type=sortable]'), 'sortable');
        calf.ui.init($('input[data-type=tags]'), 'tags');
        calf.ui.init($('textarea[data-type=html]'), 'wysiwyg');
        calf.ui.init($('select'), 'select');
        calf.ui.init($('input, textarea'), 'placeholder');
        calf.ui.init($('.btn-toggle'), 'buttonToggle');
        calf.ui.init($('.js-action-button-toggle'), 'actionButtonToggle');
        calf.ui.init($('[data-type=asset-library]'), 'assetLibrary');

        // Bind events
        // calf.click($('[data-click]'));
        calf.change($('[data-change]'));
        calf.ajax.bind($('[data-ajax]'));

        return calf;
    },

    click : function(obj) {
        $(obj).on('touchstart click', function(event){
            event.preventDefault();

            var action = $(this).data('click'),
                target = $(this).data('target');

            switch(action) {
                case 'show' :
                    calf.show(target);
                    break;

                case 'hide' :
                    calf.hide(target);
                    break;

                case 'toggle' :
                    calf.toggle(target);
                    break;
            }
        });
    },

    change : function(obj) {
        obj.on('change', function(event) {
            var action = $(this).data('change'),
                target = $(this).data('target'),
                value = $(this).val(),
                t = target;

            if(value) {
                t = t + '[data-value="'+value+'"]';
            }

            switch(action) {
                case 'show' :
                    calf.show(t);
                    break;

                case 'showOne' :
                    calf.hide(target);
                    calf.show(t);
                    break;

                case 'hide' :
                    calf.hide(t);
                    break;

                case 'toggle' :
                    calf.toggle(t);
                    break;
            }
        });
    },

    show   : function(obj) { $(obj).removeClass('hidden').show(); return calf; },
    hide   : function(obj) { $(obj).addClass('hidden').hide(); return calf; },
    toggle : function(obj) { $(obj).toggleClass('hidden').toggle(); return calf; },

    alert : function(type, message, appendTo, hide) {
        var selector = '.messages';

        if(typeof appendTo !== 'undefined') {
            selector = appendTo;
        }

        $(selector).append('<div class="alert alert-'+type+'">'+message+'</div>');
        $(selector + ' .alert:last-child')
            .hide()
            .slideDown('slow', function(){
                if(hide === true) {
                    $(this).delay(10000).slideUp('slow', function(){ $(this).remove(); });
                }
            });

        return calf;
    },

    ajax : {
        bind : function(obj) {
            obj.on('click touchstart', function(event) {
                event.preventDefault();

                switch( $(this).data('response-action') ) {
                    case 'prepend' :
                        calf.ajax.prepend($(this), $(this).data('response-target'), $(this).data('response-url'), $(this).data('response-type'), {});
                        break;

                    case 'append' :
                        calf.ajax.append($(this), $(this).data('response-target'), $(this).data('response-url'), $(this).data('response-type'), {});
                        break;

                    case 'replace' :
                        calf.ajax.replace($(this), $(this).data('response-target'), $(this).data('response-url'), $(this).data('response-type'), {});
                        break;

                    default :
                        calf.ajax.insert($(this), $(this).data('response-target'), $(this).data('response-url'), $(this).data('response-type'), {});
                        break;
                }
            });
        },

        send : function(obj, url, type, supportData, func) {
            $.ajax({
                url: url,
                data: supportData,
                success: function(data){
                    func(obj, data);
                },
                dataType: type
            });
        },

        insert : function(obj, target, url, type, supportData) {
            calf.ajax.send(obj, url, type, supportData, function(obj, data){
                $(target).html(data);
            });
        },

        replace : function(obj, target, url, type, supportData) {
            calf.ajax.send(obj, url, type, supportData, function(obj, data) {
                $(target).replaceWith(data);
            });
        },

        append : function(obj, target, url, type, supportData) {
            calf.ajax.send(obj, url, type, supportData, function(obj, data){
                $(target).append(data);
            });
        },

        prepend : function(obj, target, url, type, supportData) {
            calf.ajax.send(obj, url, type, supportData, function(obj, data){
                $(target).prepend(data);
            });
        }
    },

    ui : {
        color : {
            primary : '#428bca',
            success : '#5cb85c',
            info    : '#5bc0de',
            warning : '#f0ad4e',
            danger  : '#d9534f',
            default : '#777777'
        },

        init : function(obj, type) {
            if(obj.length > 0) {
                switch(type) {
                    case 'buttonToggle' :
                        calf.ui.buttonToggle(obj);
                        break;

                    case 'actionButtonToggle':
                        calf.ui.actionButtonToggle(obj);
                        break;

                    case 'datepicker' :
                        calf.ui.datepicker(obj);
                        break;

                    case 'fileupload' :
                        calf.ui.fileUpload(obj);
                        break;

                    case 'wysiwyg' :
                        calf.ui.wysiwyg(obj);
                        break;

                    case 'select' :
                        calf.ui.select(obj);
                        break;

                    case 'slider':
                        calf.ui.slider(obj);
                        break;

                    case 'sortable' :
                        calf.ui.sortable(obj);
                        break;

                    case 'tags' :
                        calf.ui.tags(obj);
                        break;

                    case 'placeholder' :
                        calf.ui.placeholder(obj);
                        break;

                    case 'assetLibrary' :
                        calf.ui.assetLibrary(obj);
                        break;
                }
            }

            return calf;
        },

        buttonToggle : function(obj) {
            obj.on('click touchstart', function(e) {
                e.preventDefault();

                if($(this).hasClass('active')) {
                    $(this).removeClass('active');

                    $(this).children('input[type="checkbox"]').removeAttr('checked');
                } else {
                    $(this).addClass('active');

                    $(this).children('input[type="checkbox"]').attr('checked','checked');
                }
            });
        },

        actionButtonToggle : function(obj) {
            var target = $(obj.data('target'));

            target.hide();

            obj.on('click touchend', function(e) {
                if($('.js-action-button-toggle').hasClass('active')) {
                    target.show();
                } else {
                    target.hide();
                }
            });
        },

        datepicker : function(obj) {
            obj.attr('data-date-format', obj.attr('placeholder'))
                .wrap('<div class="input-group date"></div>')
                .after('<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>');

            obj.datetimepicker({
                useSeconds: true
            });

            obj.next('span').on('touchstart click', function(){
                obj.data('DateTimePicker').show();
            });

            return calf;
        },

        select : function(obj) {
            obj.selectpicker();
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
                $('.selectpicker').selectpicker('mobile');
            }

            return calf;
        },

        slider : function(obj) {
            obj.slider({});
        },

        sortable: function(obj) {
            var type = $('.records-list table')[0].nodeName.toLowerCase(),
                args = {};

            if(type == 'table') {
                args.containerSelector = 'table';
                args.itemPath = '> tbody';
                args.itemSelector = 'tr';
                args.placeholder = '<tr class="placeholder"><td colspan="20"></td></tr>';
            }

            obj.sortable(args);
        },

        tags : function(obj) {
            var args = {
                tagClass : function(item) {
                    return 'label label-info';
                },
                typeahead: {}
            };

            if(typeof obj.data('suggestions') !== 'undefined') {
                args.typeahead.source = obj.data('suggestions');
            }

            if(typeof obj.data('suggestions-target') !== 'undefined') {
                args.typeahead.source = function(query) {
                    return $.get(obj.data('suggestions-target'));
                };
            }

            obj.tagsinput(args);

            return calf;
        },

        wysiwyg : function(obj) {
            obj.each(function() {
                var id      = this.attributes.id.nodeValue,
                    toolbar = 'wysihtml__toolbar--'+id,
                    editor  = new wysihtml5.Editor(id, {
                                name        : 'wysihtml__editor',
                                toolbar     : toolbar,
                                parserRules : wysihtml5ParserRules
                            });
            });

            return calf;
        },

        fileUpload : function(obj) {
            obj.each(function() {
                calf.fileUpload.init($(this));
            });
            return calf;
        },

        placeholder : function(obj) {
            obj.placeholder();
            return calf;
        },

        assetLibrary : function(obj) {
            calf.assetLibrary.load(obj);

            return calf;
        }
    },

    assetLibrary : {
        load : function(obj) {
            $(obj.data('response-target')).prev().show();
            $(obj.data('response-target')).html();

            calf.ajax.send(obj,
                obj.data('url'),
                'html',
                {},
                function(obj, data) {
                    $(obj.data('response-target')).prev().hide();
                    $(obj.data('response-target')).html(data);

                    calf.assetLibrary.bind.pagination(obj);
                    calf.assetLibrary.bind.checkboxes(obj);
                }
            );
        },
        bind : {
            pagination : function(obj) {
                $(obj.data('response-target') + ' .pagination a')
                    .each(function() {
                        $(this).attr('data-url', $(this).attr('href'));
                        $(this).attr('data-response-target', obj.data('response-target'));
                    })
                    .on('click touchstart', function(event){
                        event.preventDefault();

                        calf.assetLibrary.load($(this));
                    });
            },
            checkboxes : function(obj) {
                var targetStr = obj.data('response-target'),
                    parent = $(targetStr).parent(),
                    items = $(targetStr + ' .asset-library__item label');

                items.on('click touchstart', function() {
                    var checkbox = $(this).children('input[type="checkbox"]');

                    if(checkbox.is(':checked')) {
                        parent.append('<input type="hidden" name="assets[]" value="'+checkbox.val()+'" readonly/>');
                    } else {
                        parent.children('input[name="assets[]"][value="'+checkbox.val()+'"]').remove();
                    }
                });

                parent.children('input[name="assets[]"]').each(function() {
                    items.children('input[type="checkbox"][value="' + $(this).val() + '"]').attr('checked','checked');
                });
            }
        }
    },

    fileUpload : {
        init : function(obj) {
            var fieldname       = obj.attr('name'),
                progressTarget  = obj.data('progress-target');

            if(fieldname.substr(fieldname.length-2,fieldname.length) == '[]') {
                fieldname = fieldname.substr(0,fieldname.length-2);
            }

            $('[data-show-done]').each(function(){ calf.hide(obj.data('show-done')); });
            $('[data-show-fail]').each(function(){ calf.hide(obj.data('show-fail')); });

            obj.fileupload({
                dataType : 'json',
                maxChunkSize: parseInt(obj.data('chunk-size'), 10),
                limitMultiFileUploadSize: parseInt(obj.data('chunk-size'), 10),
                // limitMultiFileUploads: 1,
                maxFileSize: parseInt(obj.data('max-size'), 10),
                acceptFileTypes: new RegExp("(\.|\/)(" + obj.data('allowed-extensions') + ")$")
            })
                .on('fileuploadprocessalways', function(e, data) {
                    if(data.files.length > 0 && data.files.error) {
                        calf.fileUpload.fail(obj, data.files[0]);
                    }
                })
                .on('fileuploadstart', function(e, data) {
                    $(progressTarget).removeClass('hidden');
                    $(progressTarget + ' .progress-bar').removeClass('progress-bar-danger');
                    $(progressTarget + ' .progress-bar').css('width', '0%');
                })
                .on('fileuploadprogressall', function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $(progressTarget + ' .progress-bar').css('width', progress + '%');
                })
                .on('fileuploaddone', function(e, data) {
                    $.each(data.result[fieldname], function (index, file) {
                        if(file.error){
                            calf.fileUpload.fail(obj, file);
                            return;
                        }else{
                            calf.fileUpload.success(obj, file);
                        }
                    });
                })
                .on('fileuploadfail', function(e, data) {
                    calf.fileUpload.fail(obj, data);
                });

            return calf;
        },

        success : function(obj, data) {
            calf.ajax.send(obj,
                obj.data('response-url'),
                obj.data('response-type'), {
                    name: data.name
                    // fieldname: obj.attr('name'),
                    // version: obj.data('version'),
                }, function(obj, data) {
                    $(obj.data('response-target')).append(data);
            });

            if(obj.data('show-done')){
                calf.show(obj.data('show-done'));
            }

            return calf;
        },

        fail : function(obj, data) {
            var error;

            if(typeof data.error == 'undefined') {
                $.each(data.messages, function(index, message){
                    calf.alert('danger',
                        '<span class="glyphicon glyphicon-remove-circle"></span> Failed to upload file/s - ' + data.errorThrown + ': ' + message,
                        obj.data('response-target')
                    );
                });
            } else {
                calf.alert('danger',
                    '<span class="glyphicon glyphicon-remove-circle"></span> Failed to upload ' + data.name + ' - ' + data.error,
                    obj.data('response-target')
                );
            }

            $('.fileupload__progress .progress-bar')
                .addClass('progress-bar-danger');

            if(obj.data('show-fail')){
                calf.show(obj.data('show-fail'));
            }

            return calf;
        }
    },

    apis: {
        google: {
            charts: {
                init : function (func) {
                    google.load("visualization", "1", {packages:["corechart"]});
                    google.setOnLoadCallback(func);

                    return calf;
                },

                type : function(type, target) {
                    switch(type) {
                        case 'pie' :
                            return new google.visualization.PieChart(document.getElementById(target));
                        case 'bar' :
                            return new google.visualization.BarChart(document.getElementById(target));
                        case 'line' :
                            return new google.visualization.LineChart(document.getElementById(target));
                    }
                },

                draw : function(type, target, data, options) {
                    var chart = calf.apis.google.charts.type(type, target);
                    chart.draw(google.visualization.arrayToDataTable(data), options);

                    return calf.apis.google.charts;
                }
            }
        }
    }
};

$(document).ready(function(){
    calf.ready();
});
